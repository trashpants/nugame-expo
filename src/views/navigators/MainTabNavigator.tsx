import React, { useEffect } from "react";
import { useDispatch } from "react-redux";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { SimpleLineIcons } from "@expo/vector-icons";
import { DiscoverScreen, SearchScreen, CalendarScreen } from "@screens/index";
import {
  getGames,
  appLoaded,
  POPULAR_GAMES,
  UPDATED_GAMES,
  RELEASED_GAMES,
  getPromoGames,
} from "@state/actions";
import { useTheme } from "@react-navigation/native";

import colors from "utilities/colors";

//import DealsNavigator from "./DealsNavigator";

const Tab = createBottomTabNavigator();

export type MainTabParams = {
  Detail: { screen: "GameDetail" | ""; params: { id: number } };
  UserDetails: undefined;

  Calendar: undefined;
};

export default function MainTabNavigator() {
  const dispatch = useDispatch();
  const theme = useTheme();

  useEffect(() => {
    const getGamesByType = (type: string) => dispatch(getGames(type));
    const getHeaderGames = () => dispatch(getPromoGames());
    const initialiseApp = () => dispatch(appLoaded());

    initialiseApp();
    getGamesByType(POPULAR_GAMES);
    getGamesByType(UPDATED_GAMES);
    getGamesByType(RELEASED_GAMES);
    getHeaderGames();
  }, [dispatch]);

  return (
    <Tab.Navigator
      screenOptions={({ route }) => ({
        tabBarIcon: ({ color }) => {
          let iconName;

          switch (route.name) {
            case "Discover":
              iconName = "game-controller";
              break;
            case "Releases":
              iconName = "calendar";
              break;
            case "Deals":
              iconName = "bag";
              break;
            case "Search":
              iconName = "magnifier";
              break;
            default:
              iconName = "target";
              break;
          }

          // You can return any component that you like here!
          return <SimpleLineIcons name={iconName} size={24} color={color} />;
        },
      })}
      tabBarOptions={{
        activeTintColor: theme.colors.primary,
        inactiveTintColor: theme.dark ? colors.grey400 : colors.grey400,
        keyboardHidesTabBar: true,
        //tabStyle: { backgroundColor: theme.colors.background },
      }}
    >
      <Tab.Screen name="Discover" component={DiscoverScreen} />
      <Tab.Screen name="Releases" component={CalendarScreen} />
      {/*<Tab.Screen name="Deals" component={DealsNavigator} />*/}
      <Tab.Screen name="Search" component={SearchScreen} />
    </Tab.Navigator>
  );
}

export { MainTabNavigator };
