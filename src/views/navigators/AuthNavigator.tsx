import React from "react";
import {
  createStackNavigator,
  TransitionPresets,
} from "@react-navigation/stack";

import {
  UserScreen,
  UpdateProfileScreen,
  SocialLoginScreen,
} from "views/screens";

export type AuthStackParams = {
  Detail: { screen: "GameDetail" | ""; params: { id: number } };

  UserDetails: undefined;
  UpdateUserDetails: undefined;
  SocialLogin: undefined;
};

const Stack = createStackNavigator<AuthStackParams>();

export default function AuthNavigator() {
  return (
    <Stack.Navigator initialRouteName="UserDetails" headerMode="none">
      <Stack.Screen
        name="UserDetails"
        component={UserScreen}
        options={{
          ...TransitionPresets.ModalPresentationIOS,
          cardOverlayEnabled: true,
        }}
      />
      <Stack.Screen name="UpdateUserDetails" component={UpdateProfileScreen} />
      <Stack.Screen name="SocialLogin" component={SocialLoginScreen} />
    </Stack.Navigator>
  );
}

export { AuthNavigator };
