import React from "react";
import {
  createStackNavigator,
  TransitionPresets,
} from "@react-navigation/stack";

import {
  GameDetailScreen,
  NewsScreen,
  ArticleScreen,
  SimilarGamesScreen,
  DetailedReviewScreen,
  ReviewsScreen,
  ReleasesScreen,
  DeveloperGamesScreen,
} from "views/screens";
import { Article, Review } from "utilities/types";

export type GameStackParams = {
  Detail: { screen: "GameDetail" | ""; params: { id: number } };
  GameDeals: { id: number };

  GameDetail: { id: number };
  News: { id: number };
  Article: { article: Article };
  SimilarGames: { id: number };
  DetailedReview: { review: Review };
  Reviews: { id: number };
  Releases: { id: number };
  DeveloperGames: { id: number; name: string };
};

const Stack = createStackNavigator<GameStackParams>();

export default function GameNavigator() {
  return (
    <Stack.Navigator initialRouteName="GameDetail" headerMode="none">
      <Stack.Screen
        name="GameDetail"
        component={GameDetailScreen}
        options={{
          ...TransitionPresets.ModalPresentationIOS,
          gestureEnabled: true,
          cardOverlayEnabled: true,
        }}
      />
      <Stack.Screen name="News" component={NewsScreen} />
      <Stack.Screen name="Article" component={ArticleScreen} />
      <Stack.Screen name="SimilarGames" component={SimilarGamesScreen} />
      <Stack.Screen name="DetailedReview" component={DetailedReviewScreen} />
      <Stack.Screen name="Reviews" component={ReviewsScreen} />
      <Stack.Screen name="Releases" component={ReleasesScreen} />
      <Stack.Screen name="DeveloperGames" component={DeveloperGamesScreen} />
    </Stack.Navigator>
  );
}

export { GameNavigator };
