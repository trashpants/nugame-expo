import React, { useEffect } from "react";
import { NavigationContainer } from "@react-navigation/native";
import { AppearanceProvider, useColorScheme } from "react-native-appearance";
import {
  createStackNavigator,
  TransitionPresets,
} from "@react-navigation/stack";
import { useSelector, useDispatch } from "react-redux";
import { StatusBar } from "react-native";

import MainTabNavigator from "./MainTabNavigator";
import GameNavigator from "./GameNavigator";
import AuthNavigator from "./AuthNavigator";

import {
  UserReviewScreen,
  PromotedGamesScreen,
  GameDealsScreen,
  CreateProfileScreen,
} from "views/screens";
import { RootState } from "state/store";
import { appLoaded } from "state/actions";
import { DarkTheme, LightTheme } from "utilities/colors";

export type MainStackParams = {
  UserSetup: undefined;

  Main: undefined;
  Detail: { screen: "GameDetail" | ""; params: { id: number } };
  PromotedGames: undefined;
  Deals: undefined;
  UserReview: { id: number };
  GameDeals: { id: number };
  UserDetails: undefined;
};

const Stack = createStackNavigator<MainStackParams>();

export default function RootNavigator() {
  const scheme = useColorScheme();
  const userAuth = useSelector((state: RootState) => state.auth.apiKey);
  const dispatch = useDispatch();

  useEffect(() => {
    const initialiseApp = () => dispatch(appLoaded());
    initialiseApp();
  }, [dispatch]);

  return (
    <AppearanceProvider>
      <StatusBar
        barStyle={scheme === "dark" ? "light-content" : "dark-content"}
        backgroundColor="#00000033"
        translucent
      />
      <NavigationContainer theme={scheme === "dark" ? DarkTheme : LightTheme}>
        {userAuth === null ? (
          <Stack.Navigator
            headerMode="none"
            screenOptions={{
              cardOverlayEnabled: true,
            }}
          >
            <Stack.Screen name="UserSetup" component={CreateProfileScreen} />
          </Stack.Navigator>
        ) : (
          <Stack.Navigator
            initialRouteName="Main"
            headerMode="none"
            screenOptions={{
              cardOverlayEnabled: true,
            }}
          >
            <Stack.Screen name="Main" component={MainTabNavigator} />
            <Stack.Screen
              name="Detail"
              component={GameNavigator}
              options={{
                ...TransitionPresets.ModalPresentationIOS,
                cardOverlayEnabled: true,
              }}
            />
            <Stack.Screen
              name="PromotedGames"
              component={PromotedGamesScreen}
              options={{
                ...TransitionPresets.ModalPresentationIOS,
                cardOverlayEnabled: true,
              }}
            />
            <Stack.Screen
              name="UserReview"
              component={UserReviewScreen}
              options={{
                ...TransitionPresets.ModalPresentationIOS,
                cardStyle: { backgroundColor: "transparent" },
                gestureResponseDistance: { vertical: 1000 },
                gestureEnabled: true,
                cardOverlayEnabled: true,
              }}
            />
            <Stack.Screen
              name="GameDeals"
              component={GameDealsScreen}
              options={{
                ...TransitionPresets.ModalPresentationIOS,
                cardStyle: { backgroundColor: "transparent" },
                gestureResponseDistance: { vertical: 1000 },
                gestureEnabled: true,
                cardOverlayEnabled: true,
              }}
            />
            <Stack.Screen
              name="UserDetails"
              component={AuthNavigator}
              options={{
                ...TransitionPresets.ModalPresentationIOS,
                cardOverlayEnabled: true,
              }}
            />
          </Stack.Navigator>
        )}
      </NavigationContainer>
    </AppearanceProvider>
  );
}

export { RootNavigator };
