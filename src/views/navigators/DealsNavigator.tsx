import React from "react";
import { createStackNavigator } from "@react-navigation/stack";

import { DealsMainScreen, DealDetailScreen } from "views/screens";
import { Deal } from "utilities/types";

export type DealStackParams = {
  Main: undefined;
  DealDetail: { deal: Deal };
  Deals: undefined;
};

const Stack = createStackNavigator<DealStackParams>();

export default function DealsNavigator() {
  return (
    <Stack.Navigator initialRouteName="Main" headerMode="none">
      <Stack.Screen name="Main" component={DealsMainScreen} />
      <Stack.Screen name="DealDetail" component={DealDetailScreen} />
    </Stack.Navigator>
  );
}

export { DealsNavigator };
