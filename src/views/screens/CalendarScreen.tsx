import React from "react";
import { View, Text, StyleSheet, Image, TouchableOpacity } from "react-native";
import { useSelector, useDispatch } from "react-redux";
import { StackNavigationProp } from "@react-navigation/stack";
import { RouteProp, useTheme } from "@react-navigation/native";
import { SimpleLineIcons } from "@expo/vector-icons";
import Constants from "expo-constants";
import { Agenda } from "react-native-calendars";

import { MainTabParams } from "views/navigators";
import { Game } from "utilities/types";
import { RootState } from "state/store";
import { byDateAsc } from "utilities/helpers";
import { GameCard, UserIcon } from "components";
import { getIconFromPlatform } from "utilities/icons";
import { getMoreReleases } from "state/actions";
import colors from "utilities/colors";

interface CalendarScreenProps {
  navigation: StackNavigationProp<MainTabParams, "Calendar">;
  route: RouteProp<MainTabParams, "Calendar">;
}

export default function CalendarScreen({ navigation }: CalendarScreenProps) {
  const theme = useTheme();
  const dispatch = useDispatch();

  const games = useSelector((state: RootState) => state.games.games).sort(
    byDateAsc
  );

  //const [totalGameList, setTotalGameList] = useState<Game[]>(games);

  const styles = StyleSheet.create({
    container: {
      flex: 1,
    },
    mainHeader: {
      fontSize: 36,
      fontWeight: "bold",
      color: theme.colors.text,
    },
    headerContainer: {
      paddingTop: Constants.statusBarHeight + 16,
      flexDirection: "row",
      justifyContent: "space-between",
      alignItems: "flex-end",
      paddingHorizontal: 16,
      paddingBottom: 16,
    },
    calendarContainer: {
      flex: 1,
    },
    itemContainer: {
      paddingLeft: 16,
      paddingVertical: 8,
      flexDirection: "row",
    },
    gameCoverContainer: { width: 125, marginRight: 12 },
    gameDetailsContainer: {
      flex: 1,
    },
    gameTitle: {
      fontSize: 24,
      fontWeight: "bold",
      color: theme.colors.text,
    },
    gameSummary: {
      fontSize: 16,
      color: theme.colors.text,
    },
    iconContainer: {
      flexDirection: "row",
    },
    icon: {
      height: 24,
      width: 24,
      resizeMode: "contain",
      margin: 6,
    },
    emptyDate: {
      height: 1,
      flex: 1,
      paddingVertical: 24,
      marginRight: 16,
      borderBottomWidth: 1,
      borderColor: theme.colors.border,
    },
    handle: {
      height: 8,
      borderRadius: 6,
      width: 64,
      backgroundColor: colors.grey050,
      borderWidth: 1,
      borderColor: theme.colors.border,
    },
  });

  const calendarTheme = {
    backgroundColor: theme.colors.background,
    calendarBackground: theme.colors.card,
    textSectionTitleColor: "#b6c1cd",
    selectedDayBackgroundColor: "#00adf5",
    selectedDayTextColor: colors.grey050,
    todayTextColor: "#00adf5",
    dayTextColor: theme.colors.text,
    textDisabledColor: theme.colors.background,
    dotColor: theme.colors.primary,
    selectedDotColor: "#ffffff",
    monthTextColor: theme.colors.primary,
    indicatorColor: theme.colors.primary,
    textDayFontWeight: "300",
    textMonthFontWeight: "bold",
    textDayHeaderFontWeight: "300",
    textDayFontSize: 16,
    textMonthFontSize: 16,
    textDayHeaderFontSize: 16,
  };

  return (
    <View style={styles.container}>
      <View style={styles.calendarContainer}>
        <View style={styles.headerContainer}>
          <Text style={styles.mainHeader}>Releases</Text>
          <UserIcon
            size={48}
            onPress={() => {
              navigation.navigate("UserDetails");
            }}
          />
        </View>
        <Agenda
          theme={calendarTheme}
          items={groupByDate(games)}
          loadItemsForMonth={(month) => {
            console.log("trigger items loading");

            dispatch(getMoreReleases(new Date(month.dateString), "PREVIOUS"));
          }}
          selected={new Date()}
          renderKnob={() => {
            return <View style={styles.handle} />;
          }}
          renderItem={(item) => {
            return (
              <TouchableOpacity
                style={styles.itemContainer}
                onPress={() => {
                  navigation.navigate("Detail", {
                    screen: "GameDetail",
                    params: { id: item.id },
                  });
                }}
              >
                <View style={styles.gameCoverContainer}>
                  <GameCard
                    cardWidth={125}
                    game={item}
                    onPress={(g) => {
                      navigation.navigate("Detail", {
                        screen: "GameDetail",
                        params: { id: g.id },
                      });
                    }}
                  />
                </View>

                <View style={styles.gameDetailsContainer}>
                  <Text
                    style={styles.gameTitle}
                    numberOfLines={2}
                    ellipsizeMode="tail"
                  >
                    {item.name}
                  </Text>
                  <Text
                    style={styles.gameSummary}
                    numberOfLines={3}
                    ellipsizeMode="tail"
                  >
                    {item.summary}
                  </Text>
                  <View style={styles.iconContainer}>
                    {item.platforms.map((p: string, i: number) => {
                      return (
                        <Image
                          key={i}
                          source={getIconFromPlatform(p, theme.dark)}
                          style={styles.icon}
                        />
                      );
                    })}
                  </View>
                </View>
                <View style={{ justifyContent: "center", padding: 8 }}>
                  <SimpleLineIcons
                    name="arrow-right"
                    size={24}
                    color={theme.colors.border}
                  />
                </View>
              </TouchableOpacity>
            );
          }}
          renderEmptyDate={() => {
            return (
              <View>
                <View style={styles.emptyDate} />
              </View>
            );
          }}
          rowHasChanged={(r1: Game, r2: Game) => {
            return r1.id !== r2.id;
          }}
          markedDates={getMarkedDates(games)}
        />
      </View>
    </View>
  );
}

export { CalendarScreen };

function getMarkedDates(games: Game[]) {
  const dates = [...new Set(games.map((g) => g.initial_release))];
  const groups: Record<string, unknown> = {};

  dates.forEach((d) => {
    if (d === null) {
      return;
    }
    let day;
    const year = String(new Date(d).getFullYear());
    let month = String(new Date(d).getMonth() + 1);
    if (month.length === 1) {
      month = "0" + month;
    }
    day = String(new Date(d).getDate());
    if (day.length === 1) {
      day = "0" + day;
    }
    const date = year + "-" + month + "-" + day;

    groups[date] = { marked: true };
  });
  return groups;
}

function getAllDates(startDate: Date, stopDate: Date) {
  const days = {};
  let currentDate = new Date(startDate);
  const stopD = new Date(stopDate);

  while (currentDate <= stopD) {
    const d = new Date(currentDate);

    let month, day;
    const year = String(new Date(d).getFullYear());
    month = String(new Date(d).getMonth() + 1);
    if (month.length === 1) {
      month = "0" + month;
    }
    day = String(new Date(d).getDate());
    if (day.length === 1) {
      day = "0" + day;
    }
    const date = year + "-" + month + "-" + day;
    days[date] = [];
    const newDate = currentDate;
    newDate.setDate(currentDate.getDate() + 1);

    currentDate = newDate;
  }

  console.log("all days ", days);
  return days;
}

function groupByDate(games: Game[]) {
  // get unique dates out of this array
  const dates = [...new Set(games.map((g) => g.initial_release))].filter(
    (i) => {
      return i !== null;
    }
  );
  let groups: {} = {};

  //loop over each day from start to end
  const [currentDate] = dates;
  const lastDate = dates[dates.length - 1];
  // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
  groups = getAllDates(currentDate!, lastDate!);

  dates.forEach((d) => {
    if (d === null) {
      return;
    }
    let month, day;
    const year = String(new Date(d).getFullYear());
    month = String(new Date(d).getMonth() + 1);
    if (month.length === 1) {
      month = "0" + month;
    }
    day = String(new Date(d).getDate());
    if (day.length === 1) {
      day = "0" + day;
    }
    const date = year + "-" + month + "-" + day;

    const dateGroup: Game[] = [];
    games.forEach((g) => {
      if (g.initial_release === d) {
        dateGroup.push(g);
      }
    });

    groups[date] = dateGroup;
  });
  return groups;
}
