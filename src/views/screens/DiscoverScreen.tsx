import React, { useState } from "react";
import { View, Dimensions, Text, StyleSheet } from "react-native";
import { useSelector } from "react-redux";
import Constants from "expo-constants";
import { StackNavigationProp } from "@react-navigation/stack";
import { FlatGrid } from "react-native-super-grid";
import { useTheme } from "@react-navigation/native";

import {
  GameCard,
  GamesCarousel,
  HeaderCard,
  UserIcon,
  TabControl,
} from "components";
import { MainStackParams } from "views/navigators";
import { RootState } from "state/store";
import { Game, placeholders } from "utilities/types";
import { POPULAR_GAMES, UPDATED_GAMES } from "state/actions";
import { byDateAsc } from "utilities/helpers";

interface DiscoverScreenProps {
  navigation: StackNavigationProp<MainStackParams, "Main">;
}

const cols = 3;
const cardWidth = Math.round(Dimensions.get("window").width) / cols - 20;

export default function DiscoverScreen({ navigation }: DiscoverScreenProps) {
  const [filter, setFilter] = useState(POPULAR_GAMES);
  const theme = useTheme();

  const promotedGames = useSelector((state: RootState) => state.games.hot);

  const updatedgames = useSelector((state: RootState) =>
    state.games.games.filter((g: Game) => {
      return state.games.updated.includes(g.id);
    })
  );

  const popularGames = useSelector((state: RootState) =>
    state.games.games.filter((g: Game) => {
      return state.games.popular.includes(g.id);
    })
  );

  const recent = useSelector((state: RootState) =>
    state.games.games
      .filter((g: Game) => {
        return (
          state.games.comingSoon.includes(g.id) &&
          new Date(g.initial_release!).setHours(0, 0, 0, 0) >=
            new Date().setHours(0, 0, 0, 0)
        );
      })
      .slice(0, 20)
      .sort(byDateAsc)
  );

  const styles = StyleSheet.create({
    mainHeader: {
      fontSize: 36,
      fontWeight: "bold",
      color: theme.colors.text,
    },
    subHeader: {
      fontSize: 24,
      fontWeight: "bold",
      padding: 8,
      paddingTop: 36,
      marginHorizontal: 8,
      color: theme.colors.text,
    },
    headerContainer: {
      paddingTop: Constants.statusBarHeight + 6,
      flexDirection: "row",
      justifyContent: "space-between",
      alignItems: "flex-end",
      marginHorizontal: 16,
    },
  });

  return (
    <FlatGrid
      itemDimension={cardWidth}
      data={
        updatedgames.length === 0
          ? placeholders
          : filter === UPDATED_GAMES
          ? updatedgames
          : popularGames
      }
      renderItem={({ item }) => (
        <GameCard
          cardWidth={cardWidth}
          game={item}
          onPress={(game) => {
            if (game.id > 0) {
              navigation.navigate("Detail", {
                screen: "GameDetail",
                params: { id: game.id },
              });
            }
          }}
        />
      )}
      ListHeaderComponent={
        <>
          <View style={styles.headerContainer}>
            <Text style={styles.mainHeader}>Hot Games</Text>
            <UserIcon
              size={48}
              onPress={() => {
                navigation.navigate("UserDetails");
              }}
            />
          </View>
          <HeaderCard games={promotedGames} />
          <Text style={styles.subHeader}>Coming soon</Text>
          <GamesCarousel games={recent} />
          <Text style={styles.subHeader}>Discover</Text>
          <TabControl
            values={[POPULAR_GAMES, UPDATED_GAMES]}
            onChange={(value) => {
              setFilter(value);
            }}
          />
        </>
      }
    />
  );
}

export { DiscoverScreen };
