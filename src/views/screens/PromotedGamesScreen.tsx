import React from "react";
import {
  View,
  Image,
  StyleSheet,
  Dimensions,
  Text,
  TouchableOpacity,
} from "react-native";
import Dash from "react-native-dash";
import { useSelector } from "react-redux";
import { StackNavigationProp } from "@react-navigation/stack";
import { RouteProp, useNavigation, useTheme } from "@react-navigation/native";
import { SimpleLineIcons } from "@expo/vector-icons";
import { LinearGradient } from "expo-linear-gradient";

import { MainStackParams } from "views/navigators";
import { PromotedGames } from "utilities/types";
import { RootState } from "state/store";
import {
  GameCard,
  CloseButton,
  ParallaxScrollView,
  BannerBackground,
} from "components";

interface PromotedGamesProps {
  navigation: StackNavigationProp<MainStackParams, "PromotedGames">;
  route: RouteProp<MainStackParams, "PromotedGames">;
}

const gameCardWidth = Dimensions.get("screen").width / 4;
const screenWidth = Dimensions.get("screen").width;
const screenHalfHeight = Dimensions.get("screen").height / 2;

export default function PromotedGamesScreen({}: PromotedGamesProps) {
  const theme = useTheme();

  const promotedGames: PromotedGames[] = useSelector(
    (state: RootState) => state.games.hot
  );
  const navigation = useNavigation();

  const styles = StyleSheet.create({
    headerComponent: {
      height: screenHalfHeight,
      width: screenWidth,
    },
    gradient: {
      height: screenHalfHeight,
      width: screenWidth,
      position: "absolute",
    },
    pageTitle: {
      fontSize: 36,
      fontWeight: "bold",
      padding: 16,
      position: "absolute",
      top: screenHalfHeight - 64,
      color: theme.colors.text,
    },
    container: {
      paddingHorizontal: 16,
      paddingVertical: 8,
    },
    headerContainer: {
      flexDirection: "row",
      alignItems: "flex-end",
    },
    gameCardContainer: {
      width: gameCardWidth,
    },
    headerText: {
      flex: 1,
      justifyContent: "flex-end",
      padding: 6,
      color: theme.colors.text,
    },
    gameTitle: {
      fontSize: 18,
      fontWeight: "bold",
      color: theme.colors.text,
    },
    gameRelease: {
      fontSize: 16,
      color: theme.colors.text,
    },
    titleText: {
      fontWeight: "bold",
      paddingTop: 16,
      fontSize: 24,
      color: theme.colors.text,
    },
    bodyText: {
      fontSize: 18,
      color: theme.colors.text,
    },
    dash: {
      height: 1,
      marginTop: 32,
      marginBottom: 64,
      marginHorizontal: 32,
    },
    screenshot: {
      height: 200,
      width: screenWidth - 32,
      borderRadius: 6,
      marginVertical: 8,
    },
  });

  return (
    <>
      <ParallaxScrollView
        headerHeight={screenHalfHeight}
        backgroundColor={theme.colors.background}
        renderHeader={() => {
          return (
            <BannerBackground
              image={promotedGames[0].details.banner_img}
              video={null}
              onVideoStarted={() => {}}
              onVideoEnded={() => {}}
            />
          );
        }}
      >
        <>
          <LinearGradient
            colors={["transparent", "transparent", theme.colors.background]}
            style={styles.gradient}
          />
          <Text style={styles.pageTitle}>Hot Games</Text>
          <View style={styles.headerComponent} />
          {promotedGames.map((item, i) => {
            return (
              <View key={i}>
                <TouchableOpacity
                  style={styles.container}
                  onPress={() => {
                    navigation.navigate("Detail", {
                      screen: "GameDetail",
                      params: { id: item.igdb_index },
                    });
                  }}
                >
                  <View style={styles.headerContainer}>
                    <View style={styles.gameCardContainer}>
                      <GameCard
                        game={item.details}
                        cardWidth={Dimensions.get("screen").width / 4}
                        onPress={() => {}}
                      />
                    </View>
                    <View style={styles.headerText}>
                      <Text style={styles.gameTitle} numberOfLines={3}>
                        {item.details.name}
                      </Text>
                      <Text style={styles.gameRelease}>
                        {"Out: " +
                          new Date(
                            item.details.initial_release!
                          ).toLocaleDateString(undefined, {
                            year: "numeric",
                            month: "short",
                            day: "numeric",
                          })}
                      </Text>
                    </View>
                    <SimpleLineIcons
                      name="arrow-right"
                      size={24}
                      color={theme.colors.border}
                      style={{ paddingBottom: 12 }}
                    />
                  </View>
                  <Text style={styles.titleText}>{item.title}</Text>
                  <Image
                    style={styles.screenshot}
                    source={{ uri: item.details.screenshots[0] }}
                  />
                  <Text style={styles.bodyText}>{item.about}</Text>
                </TouchableOpacity>
                {i !== promotedGames.length - 1 && (
                  <Dash
                    style={styles.dash}
                    dashColor={theme.colors.border}
                    dashLength={6}
                    dashGap={6}
                    dashThickness={6}
                    dashStyle={{ borderRadius: 6 }}
                  />
                )}
              </View>
            );
          })}
        </>
      </ParallaxScrollView>

      <CloseButton />
    </>
  );
}

export { PromotedGamesScreen };
