import React, { useState, useEffect } from "react";
import {
  Platform,
  View,
  StyleSheet,
  Text,
  TextInput,
  Dimensions,
  KeyboardAvoidingView,
  ScrollView,
} from "react-native";
import Constants from "expo-constants";
import { useSelector, useDispatch } from "react-redux";
import { StackNavigationProp } from "@react-navigation/stack";
import { RouteProp, useTheme } from "@react-navigation/native";

import { GameStackParams } from "views/navigators";
import { Game } from "utilities/types";
import { RootState } from "state/store";
import colors from "utilities/colors";
import { StarRating, TextButton } from "components";
import { reviewGame } from "state/actions";

interface UserReviewProps {
  navigation: StackNavigationProp<GameStackParams, "SimilarGames">;
  route: RouteProp<GameStackParams, "SimilarGames">;
}

export default function UserReviewScreen({
  navigation,
  route,
}: UserReviewProps) {
  const theme = useTheme();
  const { id } = route.params;
  const [game] = useSelector((state: RootState) => state.games.games).filter(
    (g: Game) => {
      return g.id === id;
    }
  );

  const [score, setScore] = useState(0);
  const [title, setTitle] = useState("");
  const [body, setBody] = useState("");

  const dispatch = useDispatch();
  const postReview = () => dispatch(reviewGame(id, score, title, body));

  useEffect(() => {
    if (game.user_review !== null) {
      setScore(game.user_review.score);
      setTitle(game.user_review.title);
      setBody(game.user_review.body);
    }
  }, [game]);

  const styles = StyleSheet.create({
    handle: {
      alignSelf: "center",
      marginTop: Dimensions.get("screen").height / 2 - 24,
      backgroundColor: colors.grey050,
      height: 6,
      width: 72,
      borderRadius: 3,
    },
    container: {
      flex: 1,
      marginTop: 6,
      marginHorizontal: 3,
      backgroundColor: theme.colors.background,
      borderRadius: 12,
      paddingBottom: Constants.statusBarHeight,
    },
    headerBlock: {
      flex: 1,
      flexDirection: "row",
      justifyContent: "space-between",
      borderBottomWidth: 1,
      borderBottomColor: theme.colors.border,
      padding: 12,
    },
    title: {
      position: "absolute",
      top: 12,
      left: 0,
      right: 0,
      textAlign: "center",
      color: theme.colors.text,
    },
    gameName: {
      paddingTop: 12,
      textAlign: "center",
      fontSize: 24,
      fontWeight: "bold",
      color: theme.colors.text,
    },
    ratingContainer: {
      flexDirection: "row",
      alignSelf: "center",
      padding: 12,
    },
    reviewTitle: {
      color: theme.colors.text,
      borderRadius: 6,
      padding: 12,
      fontSize: 16,
      marginHorizontal: 12,
      backgroundColor: theme.colors.card,
    },
    reviewBody: {
      color: theme.colors.text,
      textAlignVertical: "top",
      borderRadius: 6,
      padding: 12,
      fontSize: 16,
      margin: 12,
      backgroundColor: theme.colors.card,
      height: 150,
    },
  });

  return (
    <ScrollView style={{ flex: 1 }} keyboardShouldPersistTaps="handled">
      <KeyboardAvoidingView
        style={{ flex: 1 }}
        behavior={Platform.OS === "ios" ? "position" : "padding"}
      >
        <View style={styles.handle} />
        <View style={styles.container}>
          <View style={styles.headerBlock}>
            <Text style={styles.title}>Leave a review</Text>
            <TextButton
              label="Cancel"
              onPress={() => {
                if (navigation.canGoBack()) {
                  navigation.goBack();
                }
              }}
            />
            <TextButton
              label="Save"
              onPress={() => {
                postReview();
                if (navigation.canGoBack()) {
                  navigation.goBack();
                }
              }}
            />
          </View>

          <Text style={styles.gameName}>{game.name}</Text>

          <View style={styles.ratingContainer}>
            <StarRating
              score={score}
              editable
              onScore={(val) => {
                setScore(val);
              }}
            />
          </View>

          <TextInput
            style={styles.reviewTitle}
            placeholder="Title of your review"
            onChangeText={(text) => {
              setTitle(text);
            }}
            value={title}
            placeholderTextColor={theme.dark ? colors.grey500 : colors.grey300}
          />
          <TextInput
            style={styles.reviewBody}
            multiline
            placeholder="Write a review"
            onChangeText={(text) => {
              setBody(text);
            }}
            value={body}
            placeholderTextColor={theme.dark ? colors.grey500 : colors.grey300}
          />
        </View>
      </KeyboardAvoidingView>
    </ScrollView>
  );
}

export { UserReviewScreen };
