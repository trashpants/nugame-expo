import React from "react";
import { ScrollView } from "react-native";
import { StackNavigationProp } from "@react-navigation/stack";
import { RouteProp } from "@react-navigation/native";

import { GameStackParams } from "views/navigators";
import { Review } from "utilities/types";
import { ReviewCard, TapHeader } from "components";

interface DetailedReviewScreenProps {
  navigation: StackNavigationProp<GameStackParams, "DetailedReview">;
  route: RouteProp<GameStackParams, "DetailedReview">;
}

export default function DetailedReviewScreen({
  route,
}: DetailedReviewScreenProps) {
  const { review }: { review: Review } = route.params;

  return (
    <ScrollView>
      <TapHeader label="Reviews & Ratings" />
      <ReviewCard review={review} largeMode fullWidth />
    </ScrollView>
  );
}

export { DetailedReviewScreen };
