/* eslint-disable no-shadow */
import React, { useEffect } from "react";
import {
  Image,
  Text,
  View,
  StyleSheet,
  Dimensions,
  TouchableOpacity,
  StatusBar,
} from "react-native";
import { useSelector, useDispatch } from "react-redux";
import { LinearGradient } from "expo-linear-gradient";
import { StackNavigationProp } from "@react-navigation/stack";
import { RouteProp, useTheme } from "@react-navigation/native";
import { SimpleLineIcons } from "@expo/vector-icons";
import {
  getDetail,
  getRelatedGames,
  createGameStubIfNeeded,
  dealsForGame,
} from "@state/actions";

import { GameStackParams } from "views/navigators";
import {
  ActionBar,
  BannerBackground,
  CloseButton,
  Gallery,
  GenreList,
  NewsList,
  ParallaxScrollView,
  PlatformIcons,
  ReviewCarousel,
  SimilarGames,
  TextButton,
  VideoGallery,
} from "components";
import colors from "utilities/colors";
import { Game, GameDeals } from "utilities/types";
import { RootState } from "state/store";

interface GameDetailProps {
  navigation: StackNavigationProp<GameStackParams, "GameDetail">;
  route: RouteProp<GameStackParams, "GameDetail">;
}

const screenHeight = Dimensions.get("screen").height / 2;
const screenWidth = Dimensions.get("screen").width;

export default function GameDetailScreen({
  navigation,
  route,
}: GameDetailProps) {
  const theme = useTheme();
  const { id } = route.params;
  const [game] = useSelector((state: RootState) =>
    state.games.games.filter((g: Game) => {
      return g.id === id;
    })
  );

  const pricesLoading: boolean = useSelector(
    (state: RootState) => state.deals.dealsLoading
  );

  const [gamePrices] = useSelector(
    (state: RootState) =>
      state.deals.dealsForGames.filter((deals: GameDeals) => {
        return deals.igdb_index === game.id;
      }),
    (left, right) => {
      return left !== right && !pricesLoading;
    }
  );

  const dispatch = useDispatch();
  const createGameStub = (id: number, name: string, coverUrl: string) =>
    dispatch(createGameStubIfNeeded(id, name, coverUrl));

  const initialRelease =
    game.initial_release === null
      ? "TBD"
      : new Date(game.initial_release).toLocaleDateString(undefined, {
          year: "numeric",
          month: "short",
        });

  useEffect(() => {
    const fetchDetailsForGame = (id: number) => dispatch(getDetail(id));
    const fetchRelatedGames = (id: number) => dispatch(getRelatedGames(id));
    const fetchDealsForGame = (id: number) => dispatch(dealsForGame(id));
    fetchDetailsForGame(id);
    fetchRelatedGames(id);
    fetchDealsForGame(id);
  }, [dispatch, id]);

  const styles = StyleSheet.create({
    coverImage: {
      height: (screenHeight / 2.5) * 1.33,
      width: screenHeight / 2.5,
      resizeMode: "cover",
      borderRadius: 6,
      position: "absolute",
      top: screenHeight / 4,
      alignSelf: "center",
    },
    gradient: {
      height: screenHeight,
      width: screenWidth,
    },
    detailBody: {
      backgroundColor: theme.colors.background,
    },
    gameTitle: {
      fontSize: 36,
      fontWeight: "bold",
      textAlign: "center",
      marginTop: -64,
      paddingHorizontal: 16,
      color: theme.colors.text,
    },
    gameDeveloper: {
      fontSize: 18,
      textAlign: "center",
      color: theme.colors.text,
    },
    subTitle: {
      fontSize: 28,
      fontWeight: "bold",
      color: theme.colors.text,
    },
    summary: {
      fontSize: 16,
      marginTop: 16,
      color: theme.colors.text,
    },
    contentContainer: {
      marginTop: 24,
      paddingHorizontal: 16,
    },
    headerWithButton: {
      flex: 1,
      flexDirection: "row",
      alignItems: "center",
      justifyContent: "space-between",
      color: theme.colors.text,
    },
    platformContainer: {
      flexDirection: "row",
      justifyContent: "center",
      alignItems: "center",
      paddingTop: 12,
    },
    pricesTitle: {
      fontWeight: "bold",
      color: theme.colors.text,
    },
    purchaseContainer: {
      paddingVertical: 0,
      flex: 1,
      flexDirection: "row",
      justifyContent: "space-between",
      alignItems: "center",
    },
    checkOffersButton: {
      borderRadius: 8,
      backgroundColor: colors.blue400,
      padding: 12,
      shadowColor: "#000",
      shadowOpacity: 0.3,
      shadowRadius: 10,

      elevation: 10,
    },
    checkOffersButtonText: {
      color: colors.grey050,
      fontWeight: "bold",
    },
    disabledOfferButton: {
      backgroundColor: colors.grey400,
    },
    displayedPrice: {
      fontWeight: "bold",
      fontSize: 36,
      color: theme.colors.text,
    },
  });

  return (
    <>
      <StatusBar
        barStyle="light-content"
        backgroundColor="#00000033"
        translucent
      />
      <ParallaxScrollView
        headerHeight={screenHeight}
        backgroundColor={theme.colors.background}
        renderHeader={() => {
          return (
            <BannerBackground
              image={game.banner_img}
              video={game.banner_clip}
              onVideoStarted={() => {}}
              onVideoEnded={() => {}}
            />
          );
        }}
      >
        <>
          <LinearGradient
            colors={["transparent", "transparent", theme.colors.background]}
            style={styles.gradient}
          />
          <Image source={{ uri: game.cover }} style={styles.coverImage} />
          <Text style={styles.gameTitle}>{game.name}</Text>
          {game.developer === null ? (
            <Text style={styles.gameDeveloper}>Unknown developer</Text>
          ) : (
            <TouchableOpacity
              style={{
                flexDirection: "row",
                justifyContent: "center",
                alignItems: "center",
              }}
              onPress={() => {
                navigation.push("DeveloperGames", {
                  id: game.developer!.id,
                  name: game.developer!.name,
                });
              }}
            >
              <Text style={styles.gameDeveloper}>{game.developer!.name}</Text>
              <SimpleLineIcons
                name="arrow-right"
                size={12}
                color={theme.colors.border}
                style={{ paddingLeft: 6 }}
              />
            </TouchableOpacity>
          )}
          <View style={styles.platformContainer}>
            <Text style={styles.gameDeveloper}>{initialRelease + "  •  "}</Text>
            <PlatformIcons platforms={game.platforms} gameId={game.id} />
          </View>
          <View style={styles.detailBody}>
            <ActionBar id={game.id} />

            <View style={styles.contentContainer}>
              <Text style={styles.pricesTitle}>
                {pricesLoading || gamePrices === undefined
                  ? "Fetching prices"
                  : gamePrices.deals.length === 0
                  ? "No Prices availale"
                  : "Prices around"}
              </Text>
              <View style={styles.purchaseContainer}>
                <View>
                  <Text style={styles.displayedPrice}>
                    {!pricesLoading &&
                    gamePrices !== undefined &&
                    gamePrices.deals.length > 0
                      ? gamePrices.deals[0].sale_price
                      : "£--.--"}
                  </Text>
                </View>

                <TouchableOpacity
                  style={[
                    styles.checkOffersButton,
                    (gamePrices === undefined ||
                      gamePrices.deals.length === 0) &&
                      styles.disabledOfferButton,
                  ]}
                  onPress={() => {
                    if (
                      gamePrices !== undefined &&
                      gamePrices.deals.length > 0
                    ) {
                      navigation.navigate("GameDeals", { id: game.id });
                    }
                  }}
                >
                  <Text style={styles.checkOffersButtonText}>Check offers</Text>
                </TouchableOpacity>
              </View>
            </View>

            <View style={styles.contentContainer}>
              <Text style={styles.subTitle}>About</Text>
              <Text style={styles.summary}>{game.summary}</Text>
            </View>

            {game.genres.length > 0 && (
              <>
                <View style={styles.contentContainer}>
                  <Text style={styles.subTitle}>Genres</Text>
                </View>
                <GenreList genres={game.genres} />
              </>
            )}

            {game.screenshots.length > 0 && (
              <>
                <View style={styles.contentContainer}>
                  <Text style={styles.subTitle}>Screenshots</Text>
                </View>
                <Gallery images={game.screenshots} />
              </>
            )}

            {game.videos.length > 0 && (
              <>
                <View style={styles.contentContainer}>
                  <Text style={styles.subTitle}>Videos</Text>
                </View>
                <VideoGallery videos={game.videos} />
              </>
            )}

            {game.news !== null && game.news.length > 0 && (
              <>
                <View
                  style={[styles.contentContainer, styles.headerWithButton]}
                >
                  <Text style={styles.subTitle}>News</Text>
                  {game.news.length > 3 ? (
                    <TextButton
                      label="Show more"
                      onPress={() => {
                        navigation.navigate("News", { id: id });
                      }}
                    />
                  ) : null}
                </View>
                <NewsList
                  articles={game.news}
                  limited
                  onPress={(article) => {
                    navigation.navigate("Article", { article: article });
                  }}
                />
              </>
            )}

            {game.reviews !== null && game.reviews.length > 0 && (
              <>
                <View
                  style={[styles.contentContainer, styles.headerWithButton]}
                >
                  <Text style={styles.subTitle}>Reviews</Text>
                  {game.reviews.length > 3 ? (
                    <TextButton
                      label="Show more"
                      onPress={() => {
                        navigation.navigate("Reviews", { id: game.id });
                      }}
                    />
                  ) : null}
                </View>
                <ReviewCarousel reviews={game.reviews} />
              </>
            )}

            {game.similar_games !== null && game.similar_games.length > 0 && (
              <>
                <View
                  style={[styles.contentContainer, styles.headerWithButton]}
                >
                  <Text style={styles.subTitle}>Similar Games</Text>
                  {game.similar_games.length > 5 ? (
                    <TextButton
                      label="Show more"
                      onPress={() => {
                        navigation.push("SimilarGames", { id: game.id });
                      }}
                    />
                  ) : null}
                </View>
                <SimilarGames
                  games={game.similar_games}
                  limited
                  onPress={(g) => {
                    createGameStub(g.id, g.name, g.cover);

                    navigation.push("Detail", {
                      screen: "GameDetail",
                      params: { id: g.id },
                    });
                  }}
                />
              </>
            )}
          </View>
        </>
      </ParallaxScrollView>
      <CloseButton />
    </>
  );
}

export { GameDetailScreen };
