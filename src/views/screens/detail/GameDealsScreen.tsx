import React from "react";
import {
  View,
  StyleSheet,
  Text,
  Dimensions,
  Image,
  FlatList,
  TouchableOpacity,
} from "react-native";
import Constants from "expo-constants";
import { useSelector } from "react-redux";
import { StackNavigationProp } from "@react-navigation/stack";
import { RouteProp, useTheme } from "@react-navigation/native";
import { SimpleLineIcons } from "@expo/vector-icons";
import * as WebBrowser from "expo-web-browser";

import { GameStackParams } from "views/navigators";
import colors from "utilities/colors";
import { TextButton } from "components";
import { RootState } from "state/store";
import { GameDeals } from "utilities/types";
import { getAffiliateLogo } from "utilities/icons";

interface GameDealsProps {
  navigation: StackNavigationProp<GameStackParams, "SimilarGames">;
  route: RouteProp<GameStackParams, "SimilarGames">;
}

export default function GameDealsScreen({ navigation, route }: GameDealsProps) {
  const theme = useTheme();
  const { id } = route.params;

  const [gameDeals] = useSelector(
    (state: RootState) => state.deals.dealsForGames
  ).filter((deals: GameDeals) => {
    return deals.igdb_index === id;
  });

  const styles = StyleSheet.create({
    handle: {
      alignSelf: "center",
      marginTop: Dimensions.get("screen").height / 2 - 24,
      backgroundColor: colors.grey050,
      height: 6,
      width: 72,
      borderRadius: 3,
    },
    container: {
      flex: 1,
      marginTop: 6,
      marginHorizontal: 3,
      backgroundColor: theme.colors.background,
      borderRadius: 12,
    },
    headerBlock: {
      flexDirection: "row",
      justifyContent: "space-between",
      borderBottomWidth: 1,
      borderBottomColor: theme.colors.border,
      padding: 12,
    },
    title: {
      position: "absolute",
      top: 12,
      left: 0,
      right: 0,
      textAlign: "center",
      color: theme.colors.text,
    },
    rowContainer: {
      flex: 1,
      flexDirection: "row",
      padding: 8,
      alignItems: "center",
    },
    separator: {
      borderColor: theme.colors.border,
      borderBottomWidth: 1,
      marginHorizontal: 16,
      marginVertical: 8,
    },
    image: {
      height: 110,
      width: 75,
      borderRadius: 6,
    },
    textBody: {
      paddingHorizontal: 8,
      flex: 1,
    },
    gameTitle: {
      fontWeight: "bold",
      fontSize: 16,
      paddingTop: 2,
      paddingBottom: 4,
      color: theme.colors.text,
    },
    pricesContainer: {
      flexDirection: "row",
      alignItems: "center",
    },
    originalPrice: {
      textDecorationLine: "line-through",
      color: colors.red400,
      fontSize: 12,
      fontWeight: "bold",
    },
    price: {
      fontSize: 24,
      fontWeight: "bold",
      paddingTop: 4,
      color: theme.colors.text,
    },
    affiliateLogo: {
      backgroundColor: colors.grey900,
      position: "absolute",
      height: 24,
      width: 75,
      borderRadius: 6,
      left: 8,
      bottom: 0,
    },
  });

  return (
    <View style={{ flex: 1 }}>
      <View style={styles.handle} />
      <View style={styles.container}>
        <View style={styles.headerBlock}>
          <Text style={styles.title}>Current Deals</Text>
          <TextButton
            label="Close"
            onPress={() => {
              if (navigation.canGoBack()) {
                navigation.goBack();
              }
            }}
          />
        </View>
        <FlatList
          data={gameDeals.deals}
          renderItem={({ item }) => (
            <TouchableOpacity
              style={styles.rowContainer}
              onPress={async () => {
                await WebBrowser.openBrowserAsync(item.url);
              }}
            >
              <Image source={{ uri: item.image_url }} style={styles.image} />
              <Image
                source={getAffiliateLogo(item.source)}
                style={styles.affiliateLogo}
                resizeMode="contain"
              />
              <View style={styles.textBody}>
                <Text style={styles.gameTitle}>{item.title}</Text>

                <View style={styles.pricesContainer}>
                  <View>
                    <Text style={styles.originalPrice}>{item.price}</Text>
                    <Text style={styles.price}>{item.sale_price}</Text>
                  </View>
                </View>
              </View>
              <SimpleLineIcons
                name="arrow-right"
                size={24}
                color={theme.colors.border}
              />
            </TouchableOpacity>
          )}
          keyExtractor={(item, i) => item.url + i}
          ListFooterComponent={
            <View style={{ paddingBottom: Constants.statusBarHeight }} />
          }
          ItemSeparatorComponent={() => <View style={styles.separator} />}
        />
      </View>
    </View>
  );
}

export { GameDealsScreen };
