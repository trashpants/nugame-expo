import React from "react";
import { View, Dimensions, StyleSheet } from "react-native";
import { useSelector, useDispatch } from "react-redux";
import { StackNavigationProp } from "@react-navigation/stack";
import { RouteProp } from "@react-navigation/native";
import { FlatGrid } from "react-native-super-grid";
import { createGameStubIfNeeded } from "@state/actions";

import { GameStackParams } from "views/navigators";
import { Game } from "utilities/types";
import { GameCard, TapHeader } from "components";
import { RootState } from "state/store";

interface SimilarScreenProps {
  navigation: StackNavigationProp<GameStackParams, "SimilarGames">;
  route: RouteProp<GameStackParams, "SimilarGames">;
}

const cols = 2;
const cardWidth = Math.round(Dimensions.get("window").width) / cols - 20;

const styles = StyleSheet.create({
  header: {
    paddingBottom: 16,
  },
});

export default function SimilarGamesScreen({
  navigation,
  route,
}: SimilarScreenProps) {
  const { id } = route.params;
  const [game] = useSelector((state: RootState) => state.games.games).filter(
    (g: Game) => {
      return g.id === id;
    }
  );
  const dispatch = useDispatch();
  const createGameStub = (gameId: number, gameName: string, coverUrl: string) =>
    dispatch(createGameStubIfNeeded(gameId, gameName, coverUrl));

  return (
    <View>
      <FlatGrid
        itemDimension={cardWidth}
        data={game.similar_games!}
        renderItem={({ item }) => (
          <GameCard
            cardWidth={cardWidth}
            game={item}
            onPress={(g) => {
              createGameStub(g.id, g.name, g.cover);
              navigation.push("Detail", {
                screen: "GameDetail",
                params: { id: g.id },
              });
            }}
          />
        )}
        ListHeaderComponent={() => {
          return (
            <View style={styles.header}>
              <TapHeader label={"Similar to " + game.name} />
            </View>
          );
        }}
      />
    </View>
  );
}

export { SimilarGamesScreen };
