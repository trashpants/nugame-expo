import React from "react";
import {
  View,
  StyleSheet,
  ScrollView,
  Text,
  Image,
  ImageRequireSource,
} from "react-native";
import { useSelector } from "react-redux";
import { StackNavigationProp } from "@react-navigation/stack";
import { RouteProp, useTheme } from "@react-navigation/native";

import { GameStackParams } from "views/navigators";
import { Game, Release } from "utilities/types";
import { TapHeader } from "components";
import { RootState } from "state/store";
import { getIconFromPlatform } from "utilities/icons";
import colors from "utilities/colors";

interface SimilarScreenProps {
  navigation: StackNavigationProp<GameStackParams, "Releases">;
  route: RouteProp<GameStackParams, "Releases">;
}

export default function ReleasesScreen({ route }: SimilarScreenProps) {
  const theme = useTheme();
  const { id } = route.params;
  const [game] = useSelector((state: RootState) => state.games.games).filter(
    (g: Game) => {
      return g.id === id;
    }
  );

  const sortedReleases = splitReleases(game.releases, theme.dark);

  const styles = StyleSheet.create({
    header: {
      paddingBottom: 24,
    },
    dateGroup: {
      borderLeftWidth: 2,
      borderColor: theme.colors.border,
      paddingLeft: 24,
      marginLeft: 36,
      marginTop: 24,
    },
    dateStyle: {
      marginTop: -6,
      fontSize: 24,
      fontWeight: "bold",
      color: theme.colors.text,
    },
    timelineDot: {
      position: "absolute",
      left: -12,
      height: 24,
      width: 24,
      borderRadius: 12,
      backgroundColor: colors.grey300,
    },
    timelineDotInner: {
      position: "absolute",
      left: -10,
      top: 2,
      height: 20,
      width: 20,
      borderRadius: 10,
      backgroundColor: colors.grey050,
    },
    icon: {
      height: 32,
      width: 32,
      marginRight: 12,
      resizeMode: "contain",
    },
    platformTitle: {
      fontSize: 18,
      fontWeight: "bold",
      color: theme.colors.text,
    },
    regionStyle: {
      fontSize: 18,
      color: theme.colors.text,
    },
    releaseContainer: {
      flex: 1,
      flexDirection: "row",
      paddingVertical: 12,
      paddingHorizontal: 16,
    },
  });

  return (
    <ScrollView>
      <View style={styles.header}>
        <TapHeader label={game.name + " releases"} />
      </View>
      {sortedReleases.map((item: customRelease, i) => {
        const date =
          new Date(item.date).toISOString() === "1970-01-01T00:00:00.000Z" // no games were ever released on this date
            ? "TBD"
            : new Date(item.date).toLocaleDateString(undefined, {
                year: "numeric",
                month: "short",
                day: "numeric",
              });
        return (
          <View key={i} style={styles.dateGroup}>
            <View style={styles.timelineDot} />
            <View style={styles.timelineDotInner} />
            <Text style={styles.dateStyle}>{date}</Text>
            {item.releases.map((rel, j) => {
              return (
                <View key={j} style={styles.releaseContainer}>
                  <Image source={rel.icon} style={styles.icon} />
                  <View>
                    <Text style={styles.platformTitle}>{rel.platform}</Text>
                    <Text style={styles.regionStyle}>{rel.region}</Text>
                  </View>
                </View>
              );
            })}
          </View>
        );
      })}
    </ScrollView>
  );
}

type customRelease = {
  date: Date;
  releases: {
    platform: string;
    region: string;
    icon: ImageRequireSource;
  }[];
};

function splitReleases(releases: Release[], darkMode: boolean) {
  //create a sublist of all available dates
  const unique = [...new Set(releases.map((release) => release.date))];
  const dates: customRelease[] = [];

  unique.forEach((date) => {
    dates.push({
      date,
      releases: [],
    });
  });

  //for each release drop it into a sub key
  releases.forEach((release) => {
    //this is probably really bad for performance but were doing it for like no more than 10 items
    dates
      .filter((item) => item.date === release.date)[0]
      .releases.push({
        platform: release.platform,
        region: release.region,
        icon: getIconFromPlatform(release.platform, darkMode),
      });
  });

  return dates.sort(byDate);
}

function byDate(a: customRelease, b: customRelease) {
  const releaseA = a.date;
  const releaseB = b.date;

  let comparison = 0;
  if (releaseA > releaseB) {
    comparison = 1;
  } else if (releaseA < releaseB) {
    comparison = -1;
  }
  return comparison;
}

export { ReleasesScreen };
