import React from "react";
import { View, Dimensions, StyleSheet } from "react-native";
import { useSelector } from "react-redux";
import { StackNavigationProp } from "@react-navigation/stack";
import { RouteProp } from "@react-navigation/native";
import { FlatGrid } from "react-native-super-grid";

import { GameStackParams } from "views/navigators";
import { Game, Company } from "utilities/types";
import { GameCard, TapHeader } from "components";
import { RootState } from "state/store";

interface DeveloperGamesProps {
  navigation: StackNavigationProp<GameStackParams, "DeveloperGames">;
  route: RouteProp<GameStackParams, "DeveloperGames">;
}

const cols = 2;
const cardWidth = Math.round(Dimensions.get("window").width) / cols - 20;

const styles = StyleSheet.create({
  header: {
    paddingBottom: 16,
  },
});

export default function DeveloperGamesScreen({
  navigation,
  route,
}: DeveloperGamesProps) {
  const { id, name } = route.params;
  const games: Game[] = useSelector(
    (state: RootState) => state.games.games
  ).filter((g: Game) => {
    return (
      g.developer?.id === id ||
      (g.other_companies !== null &&
        g.other_companies !== undefined &&
        g.other_companies.filter((c: Company) => {
          return c.id === id;
        }).length > 0)
    );
  });

  return (
    <View>
      <FlatGrid
        data={games}
        itemDimension={cardWidth}
        renderItem={({ item }) => (
          <GameCard
            cardWidth={cardWidth}
            game={item}
            onPress={(game) => {
              navigation.push("Detail", {
                screen: "GameDetail",
                params: { id: game.id },
              });
            }}
          />
        )}
        ListHeaderComponent={() => {
          return (
            <View style={styles.header}>
              <TapHeader label={"Games by " + name} />
            </View>
          );
        }}
      />
    </View>
  );
}

export { DeveloperGamesScreen };
