import React from "react";
import { View, FlatList, StyleSheet } from "react-native";
import { useSelector } from "react-redux";
import { StackNavigationProp } from "@react-navigation/stack";
import { RouteProp } from "@react-navigation/native";

import { GameStackParams } from "views/navigators";
import { Game, Review } from "utilities/types";
import { ReviewCard, TapHeader } from "components";
import { RootState } from "state/store";

interface ReviewsScreenProps {
  navigation: StackNavigationProp<GameStackParams, "Reviews">;
  route: RouteProp<GameStackParams, "Reviews">;
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});
export default function ReviewsScreen({ route }: ReviewsScreenProps) {
  const { id } = route.params;
  const [game] = useSelector((state: RootState) =>
    state.games.games.filter((g: Game) => {
      return g.id === id;
    })
  );

  return (
    <View style={styles.container}>
      <FlatList
        data={game.reviews}
        keyExtractor={(review: Review) => review.id.toString()}
        ListHeaderComponent={() => {
          return <TapHeader label={game.name + " Ratings and reviews"} />;
        }}
        renderItem={({ item }: { item: Review }) => (
          <ReviewCard review={item} largeMode={false} fullWidth />
        )}
      />
    </View>
  );
}

export { ReviewsScreen };
