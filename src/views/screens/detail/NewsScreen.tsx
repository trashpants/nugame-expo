import React from "react";
import { View, ScrollView } from "react-native";
import { useSelector } from "react-redux";
import { StackNavigationProp } from "@react-navigation/stack";
import { RouteProp } from "@react-navigation/native";

import { GameStackParams } from "views/navigators";
import { Game } from "utilities/types";
import { NewsList, TapHeader } from "components";
import { RootState } from "state/store";

interface NewsScreenProps {
  navigation: StackNavigationProp<GameStackParams, "News">;
  route: RouteProp<GameStackParams, "News">;
}

export default function NewsScreen({ navigation, route }: NewsScreenProps) {
  const { id } = route.params;
  const [game] = useSelector((state: RootState) => state.games.games).filter(
    (g: Game) => {
      return g.id === id;
    }
  );

  return (
    <View>
      <ScrollView>
        <TapHeader label={game.name + " news"} />
        <NewsList
          articles={game.news}
          limited={false}
          onPress={(article) => {
            navigation.navigate("Article", { article: article });
          }}
        />
      </ScrollView>
    </View>
  );
}

export { NewsScreen };
