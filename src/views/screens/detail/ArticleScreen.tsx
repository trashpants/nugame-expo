import React from "react";
import { View, Text, StyleSheet, Image, Dimensions } from "react-native";
import { StackNavigationProp } from "@react-navigation/stack";
import { RouteProp, useTheme } from "@react-navigation/native";

import { GameStackParams } from "views/navigators";
import { Game } from "utilities/types";
import { HTMLRender, CloseButton, ParallaxScrollView } from "components";

interface ArticleScreenProps {
  navigation: StackNavigationProp<GameStackParams, "Article">;
  route: RouteProp<GameStackParams, "Article">;
  game: Game;
}

export default function ArticleScreen({ route }: ArticleScreenProps) {
  const theme = useTheme();
  const { article } = route.params;
  const date = new Date(article.publish_date).toLocaleDateString(undefined, {
    year: "numeric",
    month: "short",
    day: "numeric",
  });

  const styles = StyleSheet.create({
    container: {
      padding: 16,
      paddingTop: Dimensions.get("screen").width * 0.75 + 16,
    },
    mainImage: {
      height: Dimensions.get("screen").width * 0.75,
      width: Dimensions.get("screen").width,
    },
    sourceLogo: {
      height: 36,
      width: 36,
      position: "absolute",
      top: Dimensions.get("screen").width * 0.75 - 52,
      right: 16,
    },
    headerTitle: {
      fontSize: 36,
      fontWeight: "bold",
      color: theme.colors.text,
    },
    subTitle: {
      fontSize: 24,
      fontStyle: "italic",
      paddingVertical: 12,
      color: theme.colors.text,
    },
    authorText: {
      color: theme.colors.text,
    },
  });

  return (
    <>
      <ParallaxScrollView
        headerHeight={Dimensions.get("screen").width * 0.75}
        backgroundColor={theme.colors.background}
        renderHeader={() => {
          return (
            <>
              <Image
                style={styles.mainImage}
                source={{ uri: article.image.original }}
              />
              <Image
                style={styles.sourceLogo}
                source={require("@assets/sources/gamespot.png")}
              />
            </>
          );
        }}
      >
        <View style={styles.container}>
          <Text style={styles.headerTitle}>{article.title}</Text>
          <Text style={styles.authorText}>
            by {article.authors} - {date}
          </Text>
          <Text style={styles.subTitle}>{article.deck}</Text>

          <HTMLRender html={article.body} imagePadding={32} />
        </View>
      </ParallaxScrollView>
      <CloseButton icon="back" />
    </>
  );
}
export { ArticleScreen };
