import React, { useState, useEffect, useRef } from "react";
import {
  View,
  Dimensions,
  Text,
  TextInput,
  StyleSheet,
  ViewStyle,
  ActivityIndicator,
} from "react-native";
import { useSelector, useDispatch } from "react-redux";
import Constants from "expo-constants";
import { StackNavigationProp } from "@react-navigation/stack";
import { FlatGrid } from "react-native-super-grid";
import {
  Transition,
  Transitioning,
  TransitioningView,
} from "react-native-reanimated";
import { TouchableOpacity } from "react-native-gesture-handler";
import { AntDesign } from "@expo/vector-icons";
import { useTheme } from "@react-navigation/native";

import { GameCard, TextButton } from "components";
import { MainStackParams } from "views/navigators";
import { searchForGame, clearSearch } from "state/actions";
import { RootState } from "state/store";
import { Game } from "utilities/types";
import colors from "utilities/colors";
import { bySimilarity, useDebounce } from "utilities/helpers";

interface SearchScreenProps {
  navigation: StackNavigationProp<MainStackParams, "Main">;
}

interface Layout {
  layout: {
    container: ViewStyle;
  };
}

const cols = 3;
const cardWidth = Math.round(Dimensions.get("window").width) / cols - 20;

const cancelLayouts: Layout[] = [
  {
    layout: {
      container: {
        width: 0,
        right: -100,
      },
    },
  },
  {
    layout: {
      container: {
        right: 0,
        width: undefined,
        height: undefined,
      },
    },
  },
];

const titleLayouts: Layout[] = [
  {
    layout: {
      container: {
        paddingTop: Constants.statusBarHeight + 16,
      },
    },
  },
  {
    layout: {
      container: {
        paddingTop: Constants.statusBarHeight + 8,
      },
    },
  },
];

const clearButtonLayout: Layout[] = [
  {
    layout: {
      container: {
        opacity: 0,
      },
    },
  },
  {
    layout: {
      container: {
        opacity: 1,
      },
    },
  },
];

const transition = (
  <Transition.Change interpolation="easeInOut" durationMs={400} />
);

export default function SearchScreen({ navigation }: SearchScreenProps) {
  const theme = useTheme();
  const dispatch = useDispatch();
  const clear = () => dispatch(clearSearch());
  const gameSearch = (s: string) => dispatch(searchForGame(s));
  const searchIds = useSelector((state: RootState) => state.games.search);
  const resultsLoading = useSelector(
    (state: RootState) => state.games.searchLoading
  );
  const games = useSelector((state: RootState) => state.games.games).filter(
    (g: Game) => {
      return searchIds.includes(g.id);
    }
  );

  const [searchTerm, setSearchTerm] = useState("");
  const debouncedSearchTerm = useDebounce(searchTerm, 1000);
  const [focused, setFocused] = useState(false);

  const searchInput = useRef<TextInput>(null);
  const cancelRef = useRef<TransitioningView>(null);
  const titleRef = useRef<TransitioningView>(null);
  const searchBarRef = useRef<TransitioningView>(null);
  const clearButtonRef = useRef<TransitioningView>(null);

  const [selectedCancelLayout, setCancelLayout] = useState(
    cancelLayouts[0].layout
  );
  const [selectedTitleLayout, setTitleLayout] = useState(
    titleLayouts[0].layout
  );
  const [selectedClearButtonLayout, setClearButtonLayout] = useState(
    clearButtonLayout[0].layout
  );

  useEffect(() => {
    if (debouncedSearchTerm) {
      dispatch(searchForGame(debouncedSearchTerm));
    }
  }, [debouncedSearchTerm, dispatch]);

  const styles = StyleSheet.create({
    titleContainer: {
      flexDirection: "row",
      justifyContent: "space-between",
      paddingTop: Constants.statusBarHeight + 16,
      padding: 8,
      marginHorizontal: 8,
    },
    mainHeader: {
      fontSize: 36,
      fontWeight: "bold",
      color: theme.colors.text,
    },
    searchContainer: {
      flexDirection: "row",
      marginHorizontal: 16,
      marginBottom: 16,
    },
    searchComponent: {
      flex: 1,
      flexDirection: "row",
      justifyContent: "space-between",
      borderRadius: 6,
      paddingHorizontal: 12,
      paddingVertical: 10,
      fontSize: 16,
      backgroundColor: theme.dark ? colors.grey700 : colors.grey100,
      borderColor: theme.colors.border,
    },
    searchInput: {
      fontSize: 16,
      color: theme.colors.text,
      flex: 1,
    },
    searchCancelButtonContainer: {
      width: 0, // 0-60
      paddingLeft: 8,
      justifyContent: "center",
      alignContent: "center",
    },
    clearButton: {
      backgroundColor: colors.grey200,
      height: 22,
      width: 22,
      borderRadius: 11,
      justifyContent: "center",
      alignItems: "center",
    },
  });

  return (
    <View style={{ flex: 1 }}>
      <>
        <Transitioning.View
          style={[styles.titleContainer, selectedTitleLayout.container]}
          {...{ transition, ref: titleRef }}
        >
          <Text
            style={[
              styles.mainHeader,
              focused ? { fontSize: 18 } : { fontSize: 36 },
            ]}
            numberOfLines={1}
            ellipsizeMode="tail"
          >
            {searchTerm === "" ? "Search" : "Results for " + searchTerm}
          </Text>
          <ActivityIndicator animating={resultsLoading} />
        </Transitioning.View>

        <Transitioning.View
          style={styles.searchContainer}
          {...{ transition, ref: searchBarRef }}
        >
          <View style={styles.searchComponent}>
            <TextInput
              ref={searchInput}
              style={styles.searchInput}
              placeholder="Search for a game"
              keyboardType="web-search"
              value={searchTerm}
              onChangeText={(val) => {
                setSearchTerm(val);
                if (val.length > 0) {
                  setClearButtonLayout(clearButtonLayout[1].layout);
                } else {
                  setClearButtonLayout(clearButtonLayout[0].layout);
                }
              }}
              onFocus={() => {
                if (cancelRef.current) {
                  cancelRef.current.animateNextTransition();
                }
                if (titleRef.current) {
                  titleRef.current.animateNextTransition();
                }
                if (searchBarRef.current) {
                  searchBarRef.current.animateNextTransition();
                }
                if (searchTerm.length > 0) {
                  setClearButtonLayout(clearButtonLayout[1].layout);
                } else {
                  setClearButtonLayout(clearButtonLayout[0].layout);
                }
                setCancelLayout(cancelLayouts[1].layout);
                setTitleLayout(titleLayouts[1].layout);
                setFocused(true);
              }}
              onBlur={() => {
                if (cancelRef.current) {
                  cancelRef.current.animateNextTransition();
                }
                if (titleRef.current) {
                  titleRef.current.animateNextTransition();
                }
                if (searchBarRef.current) {
                  searchBarRef.current.animateNextTransition();
                }

                setCancelLayout(cancelLayouts[0].layout);
                setTitleLayout(titleLayouts[0].layout);
                setClearButtonLayout(clearButtonLayout[0].layout);
                setFocused(false);
              }}
              onSubmitEditing={() => {
                gameSearch(searchTerm);
              }}
            />
            <Transitioning.View
              style={[selectedClearButtonLayout.container]}
              {...{ transition, ref: clearButtonRef }}
            >
              <TouchableOpacity
                style={styles.clearButton}
                onPress={() => {
                  setSearchTerm("");
                  setClearButtonLayout(clearButtonLayout[0].layout);
                }}
              >
                <AntDesign name="close" size={16} />
              </TouchableOpacity>
            </Transitioning.View>
          </View>

          <Transitioning.View
            style={[
              styles.searchCancelButtonContainer,
              selectedCancelLayout.container,
            ]}
            {...{ transition, ref: cancelRef }}
          >
            <TextButton
              label="Cancel"
              onPress={() => {
                setSearchTerm("");
                searchInput.current!.blur();
                setClearButtonLayout(clearButtonLayout[0].layout);
                clear();
              }}
            />
          </Transitioning.View>
        </Transitioning.View>
      </>
      <FlatGrid
        itemDimension={cardWidth}
        data={games.sort(bySimilarity)}
        renderItem={({ item }) => (
          <GameCard
            cardWidth={cardWidth}
            game={item}
            onPress={(game) => {
              navigation.navigate("Detail", {
                screen: "GameDetail",
                params: { id: game.id },
              });
            }}
          />
        )}
      />
    </View>
  );
}

export { SearchScreen };
