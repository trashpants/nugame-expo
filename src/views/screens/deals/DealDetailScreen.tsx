import React from "react";
import {
  View,
  StyleSheet,
  Text,
  Image,
  Dimensions,
  TouchableOpacity,
} from "react-native";
import * as WebBrowser from "expo-web-browser";
import { StackNavigationProp } from "@react-navigation/stack";
import { RouteProp } from "@react-navigation/native";
import stringStripHtml from "string-strip-html";

import { DealStackParams } from "views/navigators";
import { CloseButton, ParallaxScrollView } from "components";
import colors from "utilities/colors";
import { getDistributerLogo } from "utilities/icons";

interface DealsDetailScreenProps {
  navigation: StackNavigationProp<DealStackParams, "DealDetail">;
  route: RouteProp<DealStackParams, "DealDetail">;
}

const styles = StyleSheet.create({
  container: {
    paddingTop: Dimensions.get("screen").width * 0.75,
    paddingHorizontal: 16,
  },
  imageContainer: {
    height: Dimensions.get("screen").width * 0.75,
    width: Dimensions.get("screen").width,
    backgroundColor: colors.grey100,
  },
  image: {
    height: Dimensions.get("screen").width * 0.75,
    width: Dimensions.get("screen").width,
  },
  title: {
    paddingTop: 12,
    fontSize: 36,
    fontWeight: "bold",
  },
  headerContainer: {
    flex: 1,
    flexDirection: "row",
  },
  bodyContainer: {
    margin: 16,
    backgroundColor: colors.grey050,
  },
  buttonContainer: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 12,
    backgroundColor: colors.blue400,
    shadowColor: "#000",
    shadowOpacity: 0.4,
    shadowRadius: 4,
    elevation: 10,
    marginVertical: 16,
    paddingHorizontal: 16,
    paddingVertical: 16,
  },
  buyNowText: {
    color: colors.grey050,
    fontWeight: "bold",
    textTransform: "capitalize",
    fontSize: 18,
  },
  descriptionFrom: {
    fontWeight: "bold",
    paddingVertical: 12,
    fontSize: 16,
  },
  descriptionText: {
    fontSize: 16,
    backgroundColor: colors.grey050,
  },
  detailContainer: {
    justifyContent: "center",
    flexDirection: "row",
    backgroundColor: colors.grey200,
    borderRadius: 12,
    padding: 12,
    marginTop: 32,
    marginBottom: 16,
    marginHorizontal: 64,
    alignItems: "center",
  },
  pricesContainer: {
    //alignItems: "center",
  },
  originalPrice: {
    textDecorationLine: "line-through",
    color: colors.red400,
    fontSize: 18,
    fontWeight: "bold",
  },
  price: {
    fontSize: 36,
    fontWeight: "bold",
  },
  distributerLogo: {
    height: 48,
    width: 48,
    marginBottom: 6,
    marginRight: 12,
  },
});

export default function DealDetailScreen({ route }: DealsDetailScreenProps) {
  const { deal } = route.params;

  return (
    <>
      <ParallaxScrollView
        headerHeight={Dimensions.get("screen").width * 0.75}
        backgroundColor={colors.grey050}
        renderHeader={() => {
          return (
            <>
              <View style={styles.imageContainer}>
                <Image
                  style={styles.image}
                  source={{ uri: deal.image_url }}
                  resizeMode="contain"
                />
              </View>
            </>
          );
        }}
      >
        <View style={styles.container}>
          <Text style={styles.title}>{deal.title}</Text>

          <View style={styles.detailContainer}>
            <Image
              source={getDistributerLogo(deal.platform)}
              style={styles.distributerLogo}
            />
            <View style={styles.pricesContainer}>
              {deal.price !== deal.sale_price ? (
                <Text style={styles.originalPrice}>{deal.price}</Text>
              ) : (
                <View />
              )}
              <Text style={styles.price}>{deal.sale_price}</Text>
              <Text>{deal.platform}</Text>
            </View>
          </View>

          <TouchableOpacity
            onPress={async () => {
              await WebBrowser.openBrowserAsync(deal.url);
            }}
          >
            <View style={styles.buttonContainer}>
              <Text style={styles.buyNowText}>
                {"Buy Now @ " + deal.source}
              </Text>
            </View>
          </TouchableOpacity>

          <Text style={styles.descriptionFrom}>
            {"Description from " + deal.source + ":"}
          </Text>
          <Text style={styles.descriptionText}>
            {stringStripHtml(
              deal.description
                .replace(
                  `&nbsp;
Video
&nbsp;`,
                  ""
                )
                .replace(
                  `&nbsp;
Screenshots
&nbsp;`,
                  ""
                )
            )}
          </Text>
        </View>
      </ParallaxScrollView>
      <CloseButton icon="back" />
    </>
  );
}

export { DealDetailScreen };
