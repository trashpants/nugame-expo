import React, { useEffect } from "react";
import { View, Text, StyleSheet, ScrollView } from "react-native";
import Constants from "expo-constants";
import { StackNavigationProp } from "@react-navigation/stack";
import { RouteProp } from "@react-navigation/native";
import { useDispatch, useSelector } from "react-redux";

import { MainStackParams } from "views/navigators";
import { DealsCarousel } from "components";
import { RootState } from "state/store";
import { fetchPlatformDeals } from "state/actions";

interface DealsScreenProps {
  navigation: StackNavigationProp<MainStackParams, "Deals">;
  route: RouteProp<MainStackParams, "Deals">;
}

const styles = StyleSheet.create({
  container: {
    //paddingTop: Constants.statusBarHeight + 16,
  },
  mainHeader: {
    fontSize: 36,
    fontWeight: "bold",
    paddingTop: Constants.statusBarHeight + 16,
    padding: 8,
    marginHorizontal: 8,
  },
  subHeader: {
    fontSize: 24,
    fontWeight: "bold",
    padding: 8,
    paddingTop: 36,
    marginHorizontal: 8,
  },
});

export default function DealsMainScreen({}: DealsScreenProps) {
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(fetchPlatformDeals("Playstation 4"));
    dispatch(fetchPlatformDeals("Xbox one"));
    dispatch(fetchPlatformDeals("Nintendo Switch"));
    dispatch(fetchPlatformDeals("PC"));
  }, [dispatch]);

  const ps4 = useSelector((state: RootState) => state.deals.playstation4);
  const ps5 = useSelector((state: RootState) => state.deals.playstation5);
  const xboxOne = useSelector((state: RootState) => state.deals.xboxOne);
  const xsx = useSelector((state: RootState) => state.deals.xboxSeriesX);
  const pc = useSelector((state: RootState) => state.deals.pc);
  const consoles = useSelector((state: RootState) => state.deals.consoles);
  const pcParts = useSelector((state: RootState) => state.deals.pcComponents);

  const nintendoSwitch = useSelector(
    (state: RootState) => state.deals.nintendoSwitch
  );

  return (
    <View style={styles.container}>
      <ScrollView>
        <Text style={styles.mainHeader}>Gaming Deals</Text>

        {ps5.length > 0 && (
          <>
            <Text style={styles.subHeader}>Playstation 5</Text>
            <DealsCarousel deals={ps5} />
          </>
        )}

        {xsx.length > 0 && (
          <>
            <Text style={styles.subHeader}>Xbox Series X</Text>
            <DealsCarousel deals={xsx} />
          </>
        )}

        {ps4.length > 0 && (
          <>
            <Text style={styles.subHeader}>Playstation 4</Text>
            <DealsCarousel deals={ps4} />
          </>
        )}
        {xboxOne.length > 0 && (
          <>
            <Text style={styles.subHeader}>Xbox One</Text>
            <DealsCarousel deals={xboxOne} />
          </>
        )}
        {nintendoSwitch.length > 0 && (
          <>
            <Text style={styles.subHeader}>Nintendo Switch</Text>
            <DealsCarousel deals={nintendoSwitch} />
          </>
        )}
        {pc.length > 0 && (
          <>
            <Text style={styles.subHeader}>PC Games</Text>
            <DealsCarousel deals={pc} />
          </>
        )}
        {consoles.length > 0 && (
          <>
            <Text style={styles.subHeader}>Consoles and Peripherals</Text>
            <DealsCarousel deals={consoles} />
          </>
        )}
        {pcParts.length > 0 && (
          <>
            <Text style={styles.subHeader}>PC Parts</Text>
            <DealsCarousel deals={pcParts} />
          </>
        )}
      </ScrollView>
    </View>
  );
}

export { DealsMainScreen };
