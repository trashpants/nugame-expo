import React, { useState } from "react";
import {
  View,
  Text,
  StyleSheet,
  Dimensions,
  TouchableOpacity,
} from "react-native";
import { useSelector } from "react-redux";
import { StackNavigationProp } from "@react-navigation/stack";
import { SimpleLineIcons } from "@expo/vector-icons";
import { FlatGrid } from "react-native-super-grid";
import { useTheme } from "@react-navigation/native";

import { UserIcon, CloseButton, TabControl, GameCard } from "components";
import { AuthStackParams } from "views/navigators";
import { RootState } from "state/store";
import colors from "utilities/colors";
import { PLAYED_GAMES, HEARTED_GAMES } from "state/actions";
import { Game } from "utilities/types";

interface UserScreenProps {
  navigation: StackNavigationProp<AuthStackParams, "UserDetails">;
}

export default function UserScreen({ navigation }: UserScreenProps) {
  const theme = useTheme();
  const [filter, setFilter] = useState(HEARTED_GAMES);
  const userDetails = useSelector((state: RootState) => state.auth);
  const socialConnect = useSelector(
    (state: RootState) => state.auth.socialConnect
  );

  const cols = 3;
  const cardWidth = Math.round(Dimensions.get("window").width) / cols - 20;

  const heartedGames = useSelector(
    (state: RootState) =>
      state.games.games.filter((g: Game) => {
        return state.games.wishlist.includes(g.id);
      }),
    (left, right) => {
      return (
        left.map((lg) => {
          return lg.id;
        }) !==
        right.map((rg) => {
          return rg.id;
        })
      );
    }
  );

  const ownedGames = useSelector(
    (state: RootState) =>
      state.games.games.filter((g: Game) => {
        return state.games.owned.includes(g.id);
      }),
    (left, right) => {
      return (
        left.map((lg) => {
          return lg.id;
        }) !==
        right.map((rg) => {
          return rg.id;
        })
      );
    }
  );

  const styles = StyleSheet.create({
    backgroundContainer: {
      padding: 32,
      height: Dimensions.get("screen").height / 2,
      alignItems: "center",
      justifyContent: "center",
    },
    username: {
      fontSize: 36,
      fontWeight: "bold",
      textAlign: "center",
      color: theme.colors.text,
    },
    separatorLine: {
      borderBottomWidth: 1,
      marginHorizontal: 16,
      borderColor: colors.grey200,
    },
    actionPanel: {
      borderTopLeftRadius: 12,
      borderTopRightRadius: 12,
      marginTop: -36,
    },
    action: {
      flexDirection: "row",
      alignItems: "center",
      justifyContent: "space-between",
      paddingVertical: 16,
      marginHorizontal: 16,
    },
    actionText: {
      fontSize: 16,
      color: theme.colors.text,
    },
    subHeader: {
      fontSize: 24,
      fontWeight: "bold",
      padding: 8,
      paddingTop: 36,
      marginHorizontal: 8,
      color: theme.colors.text,
    },
    wishlistContainer: {},
    controlsContainer: {
      width: Dimensions.get("screen").width,
    },
  });

  return (
    <>
      <FlatGrid
        itemDimension={cardWidth}
        data={filter === HEARTED_GAMES ? heartedGames : ownedGames}
        renderItem={({ item }) => (
          <GameCard
            cardWidth={cardWidth}
            game={item}
            onPress={(game) => {
              navigation.navigate("Detail", {
                screen: "GameDetail",
                params: { id: game.id },
              });
            }}
          />
        )}
        ListHeaderComponent={
          <>
            <View style={styles.backgroundContainer}>
              <UserIcon size={185} />
              <Text style={styles.username}>{userDetails.username}</Text>
            </View>
            <View style={styles.controlsContainer} />
            <View style={styles.actionPanel}>
              <TouchableOpacity
                style={styles.action}
                onPress={() => {
                  navigation.navigate("UpdateUserDetails");
                }}
              >
                <Text style={styles.actionText}>Update Profile</Text>
                <SimpleLineIcons
                  name="arrow-right"
                  size={24}
                  color={colors.grey200}
                />
              </TouchableOpacity>
              <View style={styles.separatorLine} />
              <TouchableOpacity
                style={styles.action}
                onPress={() => {
                  navigation.navigate("SocialLogin");
                }}
              >
                <Text style={styles.actionText}>
                  {socialConnect ? "Sign out to stop sync" : "Sign in to Sync"}
                </Text>
                <SimpleLineIcons
                  name="arrow-right"
                  size={24}
                  color={colors.grey200}
                />
              </TouchableOpacity>
            </View>

            <View style={styles.wishlistContainer}>
              <Text style={styles.subHeader}>Your Games</Text>
              <TabControl
                values={[HEARTED_GAMES, PLAYED_GAMES]}
                onChange={(value) => {
                  setFilter(value);
                }}
              />
            </View>
          </>
        }
      />
      <CloseButton />
    </>
  );
}

export { UserScreen };
