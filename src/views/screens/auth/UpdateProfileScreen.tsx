import React, { useState } from "react";
import {
  Alert,
  View,
  Text,
  StyleSheet,
  TextInput,
  KeyboardAvoidingView,
  ScrollView,
  TouchableOpacity,
  Platform,
  ActivityIndicator,
} from "react-native";
import Constants from "expo-constants";
import { useSelector, useDispatch } from "react-redux";
import { StackNavigationProp } from "@react-navigation/stack";
import { RouteProp, useTheme } from "@react-navigation/native";

import { AuthStackParams } from "views/navigators";
import { UserIcon, CloseButton } from "components";
import { RootState } from "state/store";
import colors from "utilities/colors";
import { resetProfile, updateUserProfileDetails } from "state/actions";
import { getCameraRollImage } from "utilities/helpers";

interface UpdateProfileScreenProps {
  navigation: StackNavigationProp<AuthStackParams, "UpdateUserDetails">;
  route: RouteProp<AuthStackParams, "UpdateUserDetails">;
}

export default function UpdateProfileScreen({
  navigation,
}: UpdateProfileScreenProps) {
  const theme = useTheme();
  const userDetails = useSelector((state: RootState) => state.auth);
  const actionLoading = useSelector((state: RootState) => state.auth.loading);

  const dispatch = useDispatch();
  const updateProfile = (image: string | undefined, username: string) =>
    dispatch(updateUserProfileDetails(image, username));
  const [profileImage, setProfileImage] = useState<string | undefined>(
    undefined
  );
  const [username, setUsername] = useState(userDetails.username);

  const styles = StyleSheet.create({
    iconContainer: {
      alignItems: "center",
      padding: 32,
    },
    usernameEdit: {
      borderRadius: 6,
      padding: 12,
      fontSize: 16,
      marginHorizontal: 12,
      backgroundColor: theme.dark ? colors.grey700 : colors.grey100,
      borderColor: theme.colors.border,
      marginBottom: 32,
      color: theme.colors.text,
    },
    actionButton: {
      borderColor: theme.colors.border,
      borderWidth: 1,
      padding: 12,
      marginHorizontal: 12,
      marginTop: 16,
      borderRadius: 6,
      shadowColor: "#000",
      shadowOffset: {
        width: 0,
        height: 1,
      },
      shadowOpacity: 0.2,
      shadowRadius: 6,
    },
    updateButton: {
      backgroundColor: colors.blue400,
    },
    resetButton: {
      backgroundColor: colors.red400,
    },
    inputTitle: {
      fontWeight: "bold",
      marginHorizontal: 12,
      paddingBottom: 6,
      color: colors.grey900,
    },
    actionText: {
      textAlign: "center",
      fontSize: 16,
      fontWeight: "bold",
      color: colors.grey050,
    },
  });

  return (
    <>
      <KeyboardAvoidingView
        style={Platform.OS === "ios" && { flex: 1 }}
        behavior={Platform.OS === "ios" ? "padding" : undefined}
        keyboardVerticalOffset={
          Platform.OS === "ios" ? Constants.statusBarHeight : 0
        }
      >
        <ScrollView>
          <View style={styles.iconContainer}>
            <UserIcon
              size={185}
              onPress={async () => {
                if (actionLoading) {
                  return;
                }
                let image = await getCameraRollImage();
                if (image !== undefined && image !== null) {
                  setProfileImage(image.uri);
                }
              }}
              edit
              tempImage={profileImage}
            />
          </View>
          <Text style={styles.inputTitle}>Username</Text>

          <TextInput
            style={styles.usernameEdit}
            placeholder="Enter a username"
            onChangeText={(text) => {
              setUsername(text);
            }}
            value={username}
          />

          <TouchableOpacity
            style={[styles.actionButton, styles.updateButton]}
            onPress={() => {
              if (actionLoading) {
                return;
              }

              updateProfile(profileImage, username);
            }}
          >
            {actionLoading ? (
              <ActivityIndicator
                color={colors.grey050}
                size={19.5}
                animating={actionLoading}
              />
            ) : (
              <Text style={styles.actionText}>Update profile</Text>
            )}
          </TouchableOpacity>

          <TouchableOpacity
            style={[styles.actionButton, styles.resetButton]}
            onPress={() => {
              if (actionLoading) {
                return;
              }
              Alert.alert(
                "Are you sure?",
                "All preferences on this device will be reset",
                [
                  {
                    text: "Reset Profile",
                    onPress: () => dispatch(resetProfile()),
                    style: "destructive",
                  },
                  {
                    text: "Cancel",
                    onPress: () => console.log("Cancel Pressed"),
                    style: "cancel",
                  },
                ],
                { cancelable: false }
              );
            }}
          >
            <Text style={styles.actionText}>Reset Profile</Text>
          </TouchableOpacity>
        </ScrollView>
      </KeyboardAvoidingView>

      <CloseButton
        icon="back"
        onPress={() => {
          if (actionLoading) {
            return;
          }
          if (navigation.canGoBack()) {
            navigation.goBack();
          }
        }}
      />
    </>
  );
}

export { UpdateProfileScreen };
