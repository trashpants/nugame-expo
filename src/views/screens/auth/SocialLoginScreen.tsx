import React, { useState, useEffect } from "react";
import {
  View,
  StyleSheet,
  Text,
  Image,
  Platform,
  Dimensions,
  Animated,
  Easing,
  Alert,
  TouchableOpacity,
} from "react-native";
import { StackNavigationProp } from "@react-navigation/stack";
import { Ionicons } from "@expo/vector-icons";
import { useDispatch, useSelector } from "react-redux";
import { useTheme } from "@react-navigation/native";

import {
  CloseButton,
  AppleLoginButton,
  FacebookLoginButton,
  GoogleLoginButton,
} from "components";
import { AuthStackParams } from "views/navigators";
import { syncSocialLogin, disconnectSync } from "state/actions";
import { RootState } from "state/store";
import colors from "utilities/colors";

interface SocialLoginScreenProps {
  navigation: StackNavigationProp<AuthStackParams, "SocialLogin">;
}

export default function SocialLoginScreen({
  navigation,
}: SocialLoginScreenProps) {
  const theme = useTheme();
  const [rotation] = useState(new Animated.Value(0));
  const dispatch = useDispatch();
  const socialLoading = useSelector(
    (state: RootState) => state.auth.socialLoading
  );
  const socialConnect = useSelector(
    (state: RootState) => state.auth.socialConnect
  );

  const spin = rotation.interpolate({
    inputRange: [0, 1],
    outputRange: ["0deg", "360deg"],
  });

  const loop = Animated.loop(
    Animated.timing(rotation, {
      toValue: 1,
      duration: 2000,
      easing: Easing.linear,
      useNativeDriver: true,
    })
  );

  useEffect(() => {
    if (socialLoading) {
      loop.start();
    } else if (socialConnect) {
      loop.stop();
      loop.reset();
      //navigation.goBack();
    }
  }, [loop, socialLoading, socialConnect, navigation]);

  const syncDetails = (token: string) => dispatch(syncSocialLogin(token));

  const styles = StyleSheet.create({
    container: {
      flex: 1,
      justifyContent: "space-between",
      alignItems: "center",
    },
    loginText: {
      fontSize: 16,
      textAlign: "center",
      paddingBottom: 12,
      paddingHorizontal: 12,
      color: theme.colors.text,
    },
    buttonContainer: {
      flex: 1,
      paddingBottom: 32,
      justifyContent: "flex-end",
      width: Dimensions.get("screen").width,
    },
    logo: {
      height: Dimensions.get("screen").width / 2,
      width: Dimensions.get("screen").width / 2,
    },
    iconContainer: {
      flex: 1,
      justifyContent: "flex-end",
    },
    syncIcon: {
      position: "absolute",
      bottom: 0,
      right: 0,
    },
    actionButton: {
      borderColor: colors.grey200,
      borderWidth: 1,
      padding: 12,
      marginHorizontal: 12,
      marginTop: 16,
      borderRadius: 6,
      shadowColor: "#000",
      shadowOffset: {
        width: 0,
        height: 1,
      },
      shadowOpacity: 0.2,
      shadowRadius: 6,
    },
    resetButton: {
      backgroundColor: colors.red400,
    },
    inputTitle: {
      fontWeight: "bold",
      marginHorizontal: 12,
      paddingBottom: 6,
      color: colors.grey900,
    },
    actionText: {
      textAlign: "center",
      fontSize: 16,
      fontWeight: "bold",
      color: colors.grey050,
    },
  });

  return (
    <>
      <View style={styles.container}>
        <View style={styles.iconContainer}>
          <Image
            source={require("@assets/nugame_logo.png")}
            style={styles.logo}
            resizeMode="contain"
          />
          <Animated.View
            style={[styles.syncIcon, { transform: [{ rotate: spin }] }]}
          >
            <Ionicons name="ios-sync" size={48} color={theme.colors.text} />
          </Animated.View>
        </View>
        <View style={styles.buttonContainer}>
          {socialConnect ? (
            <>
              <Text style={styles.loginText}>
                Disconnect your account to disable synchronisation
              </Text>
              <TouchableOpacity
                style={[styles.actionButton, styles.resetButton]}
                onPress={() => {
                  if (socialLoading) {
                    return;
                  }
                  Alert.alert(
                    "Are you sure?",
                    "Your profile will remain as is but will no longer by synchronized over all your devices",
                    [
                      {
                        text: "Disconnect device",
                        onPress: () => {
                          dispatch(disconnectSync());
                        },
                        style: "destructive",
                      },
                      {
                        text: "Cancel",
                        style: "cancel",
                      },
                    ],
                    { cancelable: false }
                  );
                }}
              >
                <Text style={styles.actionText}>Logout</Text>
              </TouchableOpacity>
            </>
          ) : (
            <>
              <Text style={styles.loginText}>
                Login to synchronise your data across all your devices
              </Text>
              {Platform.OS === "ios" && (
                <AppleLoginButton
                  disabled={socialLoading}
                  onComplete={(values) => {
                    syncDetails(values.token);
                  }}
                />
              )}
              <FacebookLoginButton
                disabled={socialLoading}
                onComplete={(values) => {
                  syncDetails(values.token);
                }}
              />
              <GoogleLoginButton
                disabled={socialLoading}
                onComplete={(values) => {
                  syncDetails(values.token);
                }}
              />
            </>
          )}
        </View>
      </View>
      <CloseButton icon="back" />
    </>
  );
}

export { SocialLoginScreen };
