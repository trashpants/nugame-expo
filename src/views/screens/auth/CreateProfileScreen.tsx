import React, { useState } from "react";
import {
  View,
  StyleSheet,
  Image,
  Dimensions,
  Text,
  TouchableOpacity,
  ActivityIndicator,
} from "react-native";
import { StackNavigationProp } from "@react-navigation/stack";
import RNPickerSelect from "react-native-picker-select";
import { SimpleLineIcons } from "@expo/vector-icons";
import { ScrollView } from "react-native-gesture-handler";
import { useSelector, useDispatch } from "react-redux";
import { useTheme } from "@react-navigation/native";

import { MainStackParams } from "views/navigators";
import colors from "utilities/colors";
import { getIconFromPlatform } from "utilities/icons";
import { RootState } from "state/store";
import { createUserProfile } from "state/actions";

interface CreateProfileScreenProps {
  navigation: StackNavigationProp<MainStackParams, "UserSetup">;
}

const platforms: string[] = [
  "PC",
  "Xbox One",
  "Xbox Series X",
  "Playstation 4",
  "Playstation 5",
  "Nintendo Switch",
  "Stadia",
];

export default function CreateProfileScreen({}: CreateProfileScreenProps) {
  const theme = useTheme();
  const [region, setRegion] = useState("US");
  const [ownedPlatforms, setOwnedPlatforms] = useState<string[]>([]);
  const dispatch = useDispatch();
  const createProfile = () =>
    dispatch(createUserProfile(region, ownedPlatforms));

  const actionLoading = useSelector(
    (state: RootState) => state.auth.profileLoading
  );

  const styles = StyleSheet.create({
    container: { flex: 1 },
    imageContainer: {
      flex: 1,
      justifyContent: "center",
      alignItems: "center",
    },
    controlsContainer: {
      paddingBottom: 32,
    },
    logo: {
      height: Dimensions.get("screen").width / 2,
      width: Dimensions.get("screen").width / 2,
    },
    questionText: {
      paddingTop: 24,
      paddingBottom: 6,
      paddingHorizontal: 12,
      color: theme.colors.text,
    },
    pickerIcon: {
      top: 14,
      right: 24,
    },
    platformIconContainer: {
      padding: 12,
      marginHorizontal: 12,
      borderRadius: 6,
      backgroundColor: theme.dark ? colors.grey700 : colors.grey100,
    },
    selectedPlatform: {
      backgroundColor: colors.blue400,
    },
    platformIcon: {
      height: 48,
      width: 48,
    },
    updateButton: {
      borderColor: theme.colors.border,
      borderWidth: 1,
      padding: 12,
      marginHorizontal: 12,
      marginTop: 16,
      borderRadius: 6,
      shadowColor: "#000",
      shadowOffset: {
        width: 0,
        height: 1,
      },
      shadowOpacity: 0.2,
      shadowRadius: 6,
      backgroundColor: colors.blue400,
    },
    actionText: {
      textAlign: "center",
      fontSize: 16,
      fontWeight: "bold",
      color: colors.grey050,
    },
    promoText: {
      paddingHorizontal: 12,
      textAlign: "center",
      fontSize: 18,
      paddingBottom: 12,
      fontWeight: "bold",
      color: theme.colors.text,
    },
  });

  const pickerSelectStyles = StyleSheet.create({
    // eslint-disable-next-line react-native/no-unused-styles
    inputIOS: {
      borderRadius: 6,
      padding: 12,
      fontSize: 16,
      marginHorizontal: 12,
      backgroundColor: theme.dark ? colors.grey700 : colors.grey100,
      color: theme.colors.text,
    },
    // eslint-disable-next-line react-native/no-unused-styles
    inputAndroid: {
      borderRadius: 6,
      padding: 12,
      fontSize: 16,
      marginHorizontal: 12,
      backgroundColor: colors.grey100,
      color: colors.grey900,
    },
  });

  return (
    <View style={styles.container}>
      <View style={styles.imageContainer}>
        <Image
          source={require("@assets/nugame_logo.png")}
          style={styles.logo}
          resizeMode="contain"
        />
      </View>

      <View style={styles.controlsContainer}>
        <Text style={styles.promoText}>
          Discover, explore and keep track of releases and current prices of
          everything video games
        </Text>
        <Text style={styles.questionText}>What region are you in?</Text>
        <RNPickerSelect
          style={pickerSelectStyles}
          onValueChange={(value) => setRegion(value)}
          value={region}
          placeholder={{}}
          items={[
            { label: "Australia - AU$", value: "AU" },
            { label: "Canada - CA$", value: "CA" },
            { label: "Europe - €", value: "EU" },
            { label: "United Kingdom - £", value: "GB" },
            { label: "United States - US$", value: "US" },
          ]}
          Icon={() => {
            return (
              <SimpleLineIcons
                style={styles.pickerIcon}
                name="arrow-down"
                size={16}
                color={theme.colors.text}
              />
            );
          }}
        />
        <Text style={styles.questionText}>
          What platforms are you interested in ?
        </Text>
        <ScrollView horizontal showsHorizontalScrollIndicator={false}>
          {platforms.map((item, i) => {
            const isSelected = ownedPlatforms.indexOf(item) !== -1;
            return (
              <TouchableOpacity
                key={i}
                style={[
                  styles.platformIconContainer,
                  isSelected && styles.selectedPlatform,
                ]}
                onPress={() => {
                  //either drop this from the set of keys or push it in
                  if (isSelected) {
                    setOwnedPlatforms(
                      [...ownedPlatforms].filter((p) => {
                        return p !== item;
                      })
                    );
                  } else {
                    setOwnedPlatforms([...ownedPlatforms, item]);
                  }
                }}
              >
                <Image
                  resizeMode="contain"
                  source={getIconFromPlatform(item, theme.dark || isSelected)}
                  style={styles.platformIcon}
                />
              </TouchableOpacity>
            );
          })}
        </ScrollView>

        <TouchableOpacity
          style={styles.updateButton}
          onPress={() => {
            if (actionLoading) {
              return;
            }

            createProfile();
          }}
        >
          {actionLoading ? (
            <ActivityIndicator
              color={colors.grey050}
              size={19.5}
              animating={actionLoading}
            />
          ) : (
            <Text style={styles.actionText}>Save Preferences</Text>
          )}
        </TouchableOpacity>
      </View>
    </View>
  );
}

export { CreateProfileScreen };
