import { Asset } from "expo-asset";
import { Alert } from "react-native";
import Constants from "expo-constants";

import {
  APP_LOADED,
  AUTH_PROFILE_UPDATE_START,
  AUTH_PROFILE_UPDATE_COMPLETE,
  AUTH_PROFILE_UPDATE_FAIL,
  AUTH_SOCIAL_LOGIN_SYNC_START,
  AUTH_SOCIAL_LOGIN_SYNC_COMPLETE,
  AUTH_SOCIAL_LOGIN_SYNC_FAIL,
  AUTH_SOCIAL_DISCONNECT,
  AUTH_CREATE_PROFILE_START,
  AUTH_CREATE_PROFILE_COMPLETE,
  AUTH_CREATE_PROFILE_END,
} from "../types";

import { AppThunk, persistor } from "state/store";
import {
  postUpdateUserProfile,
  postSocialConnect,
  postSocialDisconnect,
  getUserProfile,
  postCreateUser,
} from "utilities/routes";

export const appLoaded = (): AppThunk => async (dispatch) => {
  Asset.loadAsync([
    require("@assets/platforms/android-dark.png"),
    require("@assets/platforms/android-light.png"),
    require("@assets/platforms/linux-dark.png"),
    require("@assets/platforms/linux-light.png"),
    require("@assets/platforms/mac-dark.png"),
    require("@assets/platforms/mac-light.png"),
    require("@assets/platforms/nintendo-dark.png"),
    require("@assets/platforms/nintendo-light.png"),
    require("@assets/platforms/ps4-light.png"),
    require("@assets/platforms/pc-dark.png"),
    require("@assets/platforms/pc-light.png"),
    require("@assets/platforms/playstation-dark.png"),
    require("@assets/platforms/playstation-light.png"),
    require("@assets/platforms/ps-dark.png"),
    require("@assets/platforms/ps-light.png"),
    require("@assets/platforms/ps4-dark.png"),
    require("@assets/platforms/ps5-dark.png"),
    require("@assets/platforms/ps5-light.png"),
    require("@assets/platforms/stadia-dark.png"),
    require("@assets/platforms/stadia-light.png"),
    require("@assets/platforms/switch-dark.png"),
    require("@assets/platforms/switch-light.png"),
    require("@assets/platforms/xbox-dark.png"),
    require("@assets/platforms/xbox-light.png"),
    require("@assets/platforms/xsx-dark.png"),
    require("@assets/platforms/xsx-light.png"),
  ]);

  dispatch(refreshUserProfileFromServer());
  dispatch({
    type: APP_LOADED,
  });
};

export const updateUserProfileDetails = (
  image: string | undefined,
  username: string
): AppThunk => async (dispatch, getState) => {
  const formData = new FormData();
  formData.append("username", username);

  if (image !== undefined) {
    formData.append("image", {
      uri: image,
      type: "image/jpeg",
      name: image,
    });
  }

  dispatch({
    type: AUTH_PROFILE_UPDATE_START,
  });

  fetch(postUpdateUserProfile, {
    method: "POST",
    headers: {
      Accept: "application/json",
      "Content-Type": "multipart/form-data",
      Authorization: getState().auth.apiKey,
    },
    body: formData,
  })
    .then(async (response) => {
      const data = await response.json();
      if (response.ok) {
        dispatch({
          type: AUTH_PROFILE_UPDATE_COMPLETE,
          payload: { avatar: data.avatar, username: data.username },
        });
      }
    })
    .catch((error) => {
      dispatch({
        type: AUTH_PROFILE_UPDATE_FAIL,
        payload: error,
      });
    });
};

export const refreshUserProfileFromServer = (): AppThunk => async (
  dispatch,
  getState
) => {
  fetch(getUserProfile, {
    method: "GET",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
      Authorization: getState().auth.apiKey,
    },
  })
    .then(async (response) => {
      const data = await response.json();
      if (response.ok) {
        dispatch({
          type: AUTH_SOCIAL_LOGIN_SYNC_COMPLETE,
          payload: {
            avatar: data.avatar,
            username: data.username,
            wishlist: data.hearted,
            owned: data.checked,
            socialConnect: getState().auth.socialConnect,
          },
        });
      }
    })
    .catch((error) => {
      console.log(error);
    });
};

export const syncSocialLogin = (socialToken: string): AppThunk => async (
  dispatch,
  getState
) => {
  dispatch({
    type: AUTH_SOCIAL_LOGIN_SYNC_START,
  });

  fetch(postSocialConnect, {
    method: "POST",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
      Authorization: getState().auth.apiKey,
    },
    body: JSON.stringify({
      token: socialToken,
    }),
  })
    .then(async (response) => {
      const data = await response.json();
      if (response.ok) {
        dispatch({
          type: AUTH_SOCIAL_LOGIN_SYNC_COMPLETE,
          payload: {
            avatar: data.avatar,
            username: data.username,
            wishlist: data.hearted,
            owned: data.checked,
            socialConnect: true,
          },
        });
      }
    })
    .catch((error) => {
      dispatch({
        type: AUTH_SOCIAL_LOGIN_SYNC_FAIL,
        payload: error,
      });
    });
};

export const disconnectSync = (): AppThunk => async (dispatch, getState) => {
  dispatch({
    type: AUTH_SOCIAL_DISCONNECT,
  });

  fetch(postSocialDisconnect, {
    method: "POST",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
      Authorization: getState().auth.apiKey,
    },
  });
  // we dont have to actually do anything here
};

export const createUserProfile = (
  region: string,
  platforms: string[]
): AppThunk => async (dispatch) => {
  dispatch({
    type: AUTH_CREATE_PROFILE_START,
  });

  fetch(postCreateUser, {
    method: "POST",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
    },
    body: JSON.stringify({
      region: region,
      platforms: platforms,
      // eslint-disable-next-line camelcase
      unique_id: Constants.deviceId!,
    }),
  })
    .then(async (response) => {
      const data = await response.json();
      if (response.ok) {
        dispatch({
          type: AUTH_CREATE_PROFILE_COMPLETE,
          payload: { apiKey: "Bearer " + data.token },
        });
      } else {
        dispatch({
          type: AUTH_CREATE_PROFILE_END,
        });
        Alert.alert(
          "There was an error",
          "Please ensure you have selected at least one platform you are interested in",
          [{ text: "OK", onPress: () => {} }],
          { cancelable: true }
        );
      }
    })
    .catch((error) => {
      console.log(error);
    });
};

export const reset = async () => async () => {
  return await persistor
    .purge()
    .then(() => persistor.flush())
    .then(() => {
      persistor.persist();
    });
};
