/* eslint-disable camelcase */
import { Image } from "react-native";

import {
  GAME_UPDATED,
  GAME_PROMOTED,
  GAME_COMING_SOON,
  GAME_POPULAR,
  GAME_SEARCH,
  GAME_SEARCH_START,
  GAME_SEARCH_CLEAR,
  GAME_STUB,
  GAME_REVIEW_HELPFUL,
  GAME_REVIEW,
  GAME_DETAIL,
  GAME_RELATED,
  GAME_MORE_RELEASES_LOADING,
  GAME_MORE_RELEASES_FINISHED,
  GAME_HEARTED,
  GAME_OWNED,
  GAME_RESET_PROFILE,
} from "../types";

import { refreshUserProfileFromServer } from "./AuthActions";

import {
  getComingSoon,
  getGameDetail,
  getPopular,
  getUpdated,
  getRelevantGames,
  getPromotedGames,
  postWasReviewHelpful,
  postReviewGame,
  postSearchGames,
  postMoreReleases,
  postListedGames,
} from "utilities/routes";
import { Game, PromotedGames } from "utilities/types";
import { AppThunk } from "state/store";

export const POPULAR_GAMES = "popular";
export const UPDATED_GAMES = "updated";
export const RELEASED_GAMES = "recent";
export const PLAYED_GAMES = "played";
export const HEARTED_GAMES = "hearted";

export const getGames = (type: string): AppThunk => async (
  dispatch,
  getState
) => {
  let url;

  switch (type) {
    case UPDATED_GAMES:
      url = getUpdated;
      break;
    case RELEASED_GAMES:
      url = getComingSoon;
      break;
    case POPULAR_GAMES:
    default:
      url = getPopular;
  }

  fetch(url, {
    method: "GET",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
      Authorization: getState().auth.apiKey,
    },
  })
    .then(async (response) => {
      const data = await response.json();
      if (response.status === 200 || response.status === 201) {
        // @todo way more work in terms of filtering and updating the store NEEDS to go here

        var games = [...getState().games.games];
        var indexes: number[] = [];

        data.forEach((game: Game) => {
          let gameIndex = games.findIndex((obj) => obj.id === game.id);

          if (gameIndex !== -1) {
            games[gameIndex] = game;
          } else {
            games.push(game);
          }

          indexes.push(game.id);
        });

        switch (type) {
          case UPDATED_GAMES:
            dispatch({
              type: GAME_UPDATED,
              payload: { games, updated: indexes },
            });
            break;
          case RELEASED_GAMES:
            dispatch({
              type: GAME_COMING_SOON,
              payload: { games, comingSoon: indexes },
            });
            break;
          case POPULAR_GAMES:
            dispatch({
              type: GAME_POPULAR,
              payload: { games, popular: indexes },
            });
            break;
        }
      }
    })
    .catch((error) => {
      console.log(error);
    });
};

export const getDetail = (id: number): AppThunk => async (
  dispatch,
  getState
) => {
  dispatch(refreshUserProfileFromServer());
  fetch(getGameDetail(id), {
    method: "GET",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
      Authorization: getState().auth.apiKey,
    },
  })
    .then(async (response) => {
      const data = await response.json();
      // find game in array and update it

      var games = [...getState().games.games];
      let gameIndex = games.findIndex((obj) => obj.id === id);

      games[gameIndex] = data;

      dispatch({
        type: GAME_DETAIL,
        payload: games,
      });
    })
    .catch((error) => {
      console.log(error);
    });
};

export const getPromoGames = (): AppThunk => async (dispatch, getState) => {
  fetch(getPromotedGames, {
    method: "GET",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
      Authorization: getState().auth.apiKey,
    },
  })
    .then(async (response) => {
      const data = await response.json();
      if (response.ok) {
        var games = [...getState().games.games];
        data.forEach((game: PromotedGames) => {
          let gameIndex = games.findIndex((obj) => obj.id === game.igdb_index);

          Image.prefetch(game.details.cover);
          if (game.details.banner_img !== null) {
            Image.prefetch(game.details.banner_img);
          }

          // UpdateGames
          if (gameIndex === -1) {
            games.push(game.details);
          } else {
            games[gameIndex] = game.details;
          }
        });

        dispatch({
          type: GAME_PROMOTED,
          payload: { games, hot: data },
        });
      }
    })
    .catch((error) => {
      console.log(error);
    });
};

export const createGameStubIfNeeded = (
  id: number,
  name: string,
  cover: string
): AppThunk => async (dispatch, getState) => {
  //remember that there is a possibility that a user selects a similar game that is not currently stored in the local Instance
  const gameStubExistInStore =
    [
      ...getState().games.games.filter((g) => {
        g.id;
      }),
    ].length > 0;

  if (!gameStubExistInStore) {
    var games = [...getState().games.games];
    let game: Game = {
      id: id,
      banner_clip: "",
      banner_img: "",
      cover: cover,
      deals: [],
      developer: null,
      franchise: null,
      genres: [],
      initial_release: null,
      name: name,
      news: [],
      other_companies: [],
      platforms: [],
      popularity: -1,
      releases: [],
      reviews: [],
      score: -1,
      screenshots: [],
      similar: 0,
      similar_games: [],
      summary: null,
      updated_at: null,
      user_review: null,
      videos: [],
    };
    games.push(game);

    dispatch({
      type: GAME_STUB,
      payload: [...games],
    });
  }
};

export const markReviewHelpful = (
  id: number,
  gameId: number,
  isHelpful: boolean
): AppThunk => async (dispatch, getState) => {
  var games = [...getState().games.games];
  let gameIndex = games.findIndex((obj) => obj.id === gameId);
  let reviewIndex = games[gameIndex].reviews.findIndex((obj) => obj.id === id);

  if (isHelpful) {
    games[gameIndex].reviews[reviewIndex].helpful_count += 1;
    if (games[gameIndex].reviews[reviewIndex].user_found_helpful !== null) {
      games[gameIndex].reviews[reviewIndex].unhelpful_count -= 1;
    }
  } else {
    games[gameIndex].reviews[reviewIndex].unhelpful_count += 1;
    if (games[gameIndex].reviews[reviewIndex].user_found_helpful !== null) {
      games[gameIndex].reviews[reviewIndex].helpful_count -= 1;
    }
  }
  games[gameIndex].reviews[reviewIndex].user_found_helpful = isHelpful;

  dispatch({
    type: GAME_REVIEW_HELPFUL,
    payload: [...games],
  });

  //update the server
  fetch(postWasReviewHelpful, {
    method: "POST",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
      Authorization: getState().auth.apiKey,
    },
    body: JSON.stringify({
      review_id: id,
      helpful: isHelpful,
    }),
  })
    .then(async (response) => {
      //const data = await response.json();
      // find game in array and update it
      if (response.ok) {
      }
    })
    .catch((error) => {
      console.log(error);
    });
};

export const reviewGame = (
  id: number,
  score: number,
  title: string,
  body: string
): AppThunk => async (dispatch, getState) => {
  // again update the local store and then update the server
  var games = [...getState().games.games];
  let gameIndex = games.findIndex((obj) => obj.id === id);
  games[gameIndex].user_review = {
    id: games[gameIndex].user_review?.helpful_count ?? 0,
    igdb_index: id,
    score,
    title,
    user: "", //@todo getState().auth.username,
    user_avatar: "", //@todo getState().auth.avatar,
    source: "user",
    body,
    helpful_count: games[gameIndex].user_review?.helpful_count ?? 0,
    unhelpful_count: games[gameIndex].user_review?.unhelpful_count ?? 0,
    user_found_helpful: false,
  };

  dispatch({
    type: GAME_REVIEW,
    payload: [...games],
  });

  fetch(postReviewGame, {
    method: "POST",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
      Authorization: getState().auth.apiKey,
    },
    body: JSON.stringify({
      igdb_index: id,
      score,
      title,
      body,
    }),
  })
    .then(async (response) => {
      //const data = await response.json();
      if (response.ok) {
      }
    })
    .catch((error) => {
      console.log(error);
    });
};

export const getRelatedGames = (id: number): AppThunk => async (
  dispatch,
  getState
) => {
  fetch(getRelevantGames(id), {
    method: "GET",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
      Authorization: getState().auth.apiKey,
    },
  })
    .then(async (response) => {
      const data = await response.json();

      var games = [...getState().games.games];
      data.forEach((item: Game) => {
        let gameIndex = games.findIndex((obj) => obj.id === item.id);
        if (gameIndex === -1) {
          // add it into games
          games.push(item);
        } else {
          //update inline
          games[gameIndex] = item;
        }
      });

      dispatch({
        type: GAME_RELATED,
        payload: games,
      });
    })
    .catch((error) => {
      console.log(error);
    });
};

export const searchForGame = (searchTerm: string): AppThunk => async (
  dispatch,
  getState
) => {
  dispatch({
    type: GAME_SEARCH_START,
  });

  fetch(postSearchGames, {
    method: "POST",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
      Authorization: getState().auth.apiKey,
    },
    body: JSON.stringify({
      search: searchTerm,
    }),
  })
    .then(async (response) => {
      const data = await response.json();
      if (response.ok) {
        let games = [...getState().games.games];
        let searchIds: number[] = [];

        data.forEach((game: Game) => {
          let gameIndex = games.findIndex((obj) => obj.id === game.id);
          searchIds.push(game.id);

          if (gameIndex === -1) {
            games.push(game);
          }
        });

        dispatch({
          type: GAME_SEARCH,
          payload: { games: games, search: searchIds },
        });
      }
    })
    .catch((error) => {
      console.log("SEARCH HAS BEEN ABORTED ", error);
      dispatch({
        type: GAME_SEARCH_CLEAR,
      });
    });
};

export const clearSearch = (): AppThunk => async (dispatch) => {
  dispatch({
    type: GAME_SEARCH_CLEAR,
  });
};

export const getMoreReleases = (date: Date, type: string): AppThunk => async (
  dispatch,
  getState
) => {
  dispatch({
    type: GAME_MORE_RELEASES_LOADING,
  });

  fetch(postMoreReleases, {
    method: "POST",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
      Authorization: getState().auth.apiKey,
    },
    body: JSON.stringify({
      date: date,
      type: type,
    }),
  })
    .then(async (response) => {
      const data = await response.json();
      if (response.ok) {
        let games = [...getState().games.games];
        let ids = [...getState().games.comingSoon];

        data.forEach((game: Game) => {
          let gameIndex = games.findIndex((obj) => obj.id === game.id);
          if (type === "NEW") {
            ids.push(game.id);
          } else {
            ids.unshift(game.id);
          }

          if (gameIndex === -1) {
            games.push(game);
          }
        });

        dispatch({
          type: GAME_COMING_SOON,
          payload: { games: games, comingSoon: [...new Set(ids)] },
        });

        dispatch(fetchGamesFinished());
      }
    })
    .catch((error) => {
      console.log(error);
    });
};

export const fetchGamesFinished = (): AppThunk => async (dispatch) => {
  dispatch({
    type: GAME_MORE_RELEASES_FINISHED,
  });
};

export const addGameToList = (
  id: number,
  mode: "checked" | "hearted"
): AppThunk => async (dispatch, getState) => {
  let ids =
    mode === "checked"
      ? [...getState().games.owned]
      : [...getState().games.wishlist];
  let indexPos = ids.indexOf(id);
  if (indexPos === -1) {
    //add to array
    ids.push(id);
  } else {
    //remove id from wishlistArray
    ids.splice(indexPos, 1);
  }

  fetch(postListedGames, {
    method: "POST",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
      Authorization: getState().auth.apiKey,
    },
    body: JSON.stringify({
      ids: ids,
      mode: mode,
    }),
  });

  dispatch({
    type: mode === "checked" ? GAME_OWNED : GAME_HEARTED,
    payload: ids,
  });
};

export const resetProfile = (): AppThunk => async (dispatch) => {
  dispatch({
    type: GAME_RESET_PROFILE,
  });
};
