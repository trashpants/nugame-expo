/* eslint-disable camelcase */
import {
  DEALS_PLAYSTATION_4,
  DEALS_XBOX_ONE,
  DEALS_NINTENDO_SWITCH,
  DEALS_PC_GAMES,
  DEALS_FETCH_PRICES,
  DEALS_STORE_GAMES,
  DEALS_COMPLETE_PRICES,
} from "../types";

import { AppThunk } from "state/store";
import { getDeals, getDealsForGame } from "utilities/routes";

export const fetchPlatformDeals = (platform: string): AppThunk => async (
  dispatch,
  getState
) => {
  fetch(getDeals(platform), {
    method: "GET",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
      Authorization: getState().auth.apiKey,
    },
  })
    .then(async (response) => {
      const data = await response.json();
      if (response.ok) {
        switch (platform.toLowerCase()) {
          case "playstation 4":
            dispatch({
              type: DEALS_PLAYSTATION_4,
              payload: data,
            });
            break;
          case "xbox one":
            dispatch({
              type: DEALS_XBOX_ONE,
              payload: data,
            });
            break;
          case "nintendo switch":
            dispatch({
              type: DEALS_NINTENDO_SWITCH,
              payload: data,
            });
            break;
          case "pc":
            dispatch({
              type: DEALS_PC_GAMES,
              payload: data,
            });
            break;

          default:
            break;
        }
      }
    })
    .catch((error) => {
      console.log(error);
    });
};

export const dealsForGame = (id: number): AppThunk => async (
  dispatch,
  getState
) => {
  dispatch({ type: DEALS_FETCH_PRICES });
  fetch(getDealsForGame(id), {
    method: "GET",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
      Authorization: getState().auth.apiKey,
    },
  })
    .then(async (response) => {
      const data = await response.json();
      if (response.ok) {
        var gameDeals = [...getState().deals.dealsForGames];

        let gameIndex = gameDeals.findIndex((obj) => obj.igdb_index === id);
        if (gameIndex === -1) {
          gameDeals.push({ igdb_index: id, deals: data });
        } else {
          gameDeals[gameIndex] = { igdb_index: id, deals: data };
        }

        dispatch({
          type: DEALS_STORE_GAMES,
          payload: gameDeals,
        });

        dispatch({ type: DEALS_COMPLETE_PRICES });
      }
    })
    .catch((error) => {
      console.log(error);
    });
};
