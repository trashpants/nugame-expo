import { PURGE } from "redux-persist";

import {
  APP_LOADED,
  AUTH_PROFILE_UPDATE_START,
  AUTH_PROFILE_UPDATE_COMPLETE,
  AUTH_PROFILE_UPDATE_FAIL,
  AUTH_SOCIAL_LOGIN_SYNC_START,
  AUTH_SOCIAL_LOGIN_SYNC_COMPLETE,
  AUTH_SOCIAL_LOGIN_SYNC_FAIL,
  AUTH_SOCIAL_DISCONNECT,
  AUTH_CREATE_PROFILE_COMPLETE,
  AUTH_CREATE_PROFILE_START,
  AUTH_CREATE_PROFILE_END,
} from "../types";

const INITIAL_STATE = {
  apiKey: null,
  avatar: "https://robohash.org/unknown user",
  username: "Unknown User",
  loading: false,
  socialConnect: false,
  socialLoading: false,
  profileLoading: false,
};

interface AuthReducerPayload {
  apiKey?: string;
  avatar?: string;
  username?: string;
  loading?: boolean;
  socialConnect?: boolean;
  profileLoading?: boolean;
}

const AuthReducer = (
  state = INITIAL_STATE,
  action: { type: string; payload: AuthReducerPayload }
) => {
  switch (action.type) {
    case PURGE:
      return INITIAL_STATE;
    case APP_LOADED:
      return {
        ...state,
        loading: false,
        socialLoading: false,
        profileLoading: false,
      };
    case AUTH_PROFILE_UPDATE_START:
      return {
        ...state,
        loading: true,
      };
    case AUTH_PROFILE_UPDATE_COMPLETE:
      return {
        ...state,
        avatar: action.payload.avatar,
        username: action.payload.username,
        loading: false,
      };
    case AUTH_PROFILE_UPDATE_FAIL:
      return {
        ...state,
        loading: false,
      };
    case AUTH_SOCIAL_LOGIN_SYNC_START:
      return {
        ...state,
        socialLoading: true,
      };
    case AUTH_SOCIAL_LOGIN_SYNC_COMPLETE:
      return {
        ...state,
        avatar: action.payload.avatar,
        username: action.payload.username,
        socialConnect: action.payload.socialConnect,
        socialLoading: false,
      };
    case AUTH_SOCIAL_LOGIN_SYNC_FAIL:
      return {
        ...state,
        socialLoading: false,
      };
    case AUTH_SOCIAL_DISCONNECT:
      return {
        ...state,
        socialConnect: false,
        socialLoading: false,
      };
    case AUTH_CREATE_PROFILE_START:
      return {
        ...state,
        profileLoading: true,
      };
    case AUTH_CREATE_PROFILE_COMPLETE:
      return {
        ...state,
        apiKey: action.payload.apiKey,
        profileLoading: false,
      };
    case AUTH_CREATE_PROFILE_END:
      return {
        ...state,
        profileLoading: false,
      };
    default:
      return state;
  }
};

export default AuthReducer;
