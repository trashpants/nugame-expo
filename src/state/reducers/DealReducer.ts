import { PURGE } from "redux-persist";

import {
  APP_LOADED,
  DEALS_PLAYSTATION_4,
  DEALS_XBOX_ONE,
  DEALS_PC_GAMES,
  DEALS_NINTENDO_SWITCH,
  DEALS_FETCH_PRICES,
  DEALS_STORE_GAMES,
  DEALS_COMPLETE_PRICES,
} from "../types";

const INITIAL_STATE = {
  promoted: [],
  xboxOne: [],
  xboxSeriesX: [],
  playstation4: [],
  playstation5: [],
  nintendoSwitch: [],
  pc: [],
  consoles: [],
  pcComponents: [],

  dealsLoading: false,
  dealsForGames: [],
};

const DealReducer = (
  state = INITIAL_STATE,
  action: { type: string; payload: unknown }
) => {
  switch (action.type) {
    case PURGE:
      return INITIAL_STATE;
    case APP_LOADED:
      return {
        ...state,
        dealsLoading: false,
      };
    case DEALS_PLAYSTATION_4:
      return {
        ...state,
        playstation4: action.payload,
      };
    case DEALS_XBOX_ONE:
      return {
        ...state,
        xboxOne: action.payload,
      };
    case DEALS_PC_GAMES:
      return {
        ...state,
        pc: action.payload,
      };
    case DEALS_NINTENDO_SWITCH:
      return {
        ...state,
        nintendoSwitch: action.payload,
      };
    case DEALS_PC_GAMES:
      return {
        ...state,
        pc: action.payload,
      };

    case DEALS_FETCH_PRICES:
      return {
        ...state,
        dealsLoading: true,
      };
    case DEALS_STORE_GAMES:
      return {
        ...state,
        dealsForGames: action.payload,
      };
    case DEALS_COMPLETE_PRICES:
      return {
        ...state,
        dealsLoading: false,
      };
    default:
      return state;
  }
};

export default DealReducer;
