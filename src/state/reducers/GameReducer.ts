import { PURGE } from "redux-persist";

import {
  APP_LOADED,
  GAME_UPDATED,
  GAME_COMING_SOON,
  GAME_PROMOTED,
  GAME_POPULAR,
  GAME_SEARCH,
  GAME_SEARCH_START,
  GAME_SEARCH_CLEAR,
  GAME_STUB,
  GAME_REVIEW_HELPFUL,
  GAME_REVIEW,
  GAME_DETAIL,
  GAME_RELATED,
  GAME_MORE_RELEASES_LOADING,
  GAME_MORE_RELEASES_FINISHED,
  GAME_HEARTED,
  GAME_OWNED,
  GAME_RESET_PROFILE,
  AUTH_SOCIAL_LOGIN_SYNC_COMPLETE,
} from "../types";

import { Game, PromotedGames } from "utilities/types";

const INITIAL_STATE = {
  games: <Game[]>[],
  hot: <PromotedGames[]>[],
  popular: <number[]>[],
  search: <number[]>[],
  comingSoon: <number[]>[],
  updated: <number[]>[],
  wishlist: <number[]>[],
  owned: <number[]>[],
  searchLoading: false,
  releasesLoading: false,
};

interface GameReducerPayload {
  games?: Game[];
  hot?: PromotedGames[];
  popular?: number[];
  search?: number[];
  comingSoon?: number[];
  updated?: number[];
  wishlist?: number[];
  owned?: number[];
}

const GameReducer = (
  state = INITIAL_STATE,
  action: { type: string; payload: GameReducerPayload }
) => {
  switch (action.type) {
    case PURGE:
      return INITIAL_STATE;
    case APP_LOADED:
      return {
        ...state,
        searchLoading: false,
        releasesLoading: false,
        search: [],
      };
    case GAME_RESET_PROFILE:
      return {
        ...state,
        searchLoading: false,
        releasesLoading: false,
        search: [],
        wishlist: [],
        owned: [],
      };
    case GAME_UPDATED:
      return {
        ...state,
        updated: action.payload.updated,
        games: action.payload.games,
      };
    case GAME_COMING_SOON:
      return {
        ...state,
        comingSoon: action.payload.comingSoon,
        games: action.payload.games,
      };
    case GAME_POPULAR:
      return {
        ...state,
        popular: action.payload.popular,
        games: action.payload.games,
      };
    case GAME_STUB:
    case GAME_REVIEW_HELPFUL:
    case GAME_REVIEW:
    case GAME_DETAIL:
    case GAME_RELATED:
      return {
        ...state,
        games: action.payload,
      };
    case GAME_SEARCH:
      return {
        ...state,
        search: action.payload.search,
        games: action.payload.games,
        searchLoading: false,
      };
    case GAME_SEARCH_START:
      return {
        ...state,
        searchLoading: true,
      };
    case GAME_SEARCH_CLEAR:
      return {
        ...state,
        search: [],
        searchLoading: false,
      };
    case GAME_MORE_RELEASES_LOADING:
      return {
        ...state,
        releasesLoading: true,
      };
    case GAME_MORE_RELEASES_FINISHED:
      return {
        ...state,
        releasesLoading: false,
      };
    case GAME_PROMOTED:
      return {
        ...state,
        games: action.payload.games,
        hot: action.payload.hot,
      };
    case GAME_HEARTED:
      return {
        ...state,
        wishlist: action.payload,
      };
    case GAME_OWNED:
      return {
        ...state,
        owned: action.payload,
      };

    case AUTH_SOCIAL_LOGIN_SYNC_COMPLETE:
      return {
        ...state,
        wishlist: action.payload.wishlist,
        owned: action.payload.owned,
      };
    default:
      return state;
  }
};

export default GameReducer;
