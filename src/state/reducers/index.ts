import { combineReducers } from "redux";

import AuthReducer from "./AuthReducer";
import DealReducer from "./DealReducer";
import GameReducer from "./GameReducer";

export default combineReducers({
  auth: AuthReducer,
  deals: DealReducer,
  games: GameReducer,
});
