export const APP_LOADED = "app_loaded";

export const AUTH_PROFILE_UPDATE_START = "auth_profile_update_start";
export const AUTH_PROFILE_UPDATE_COMPLETE = "auth_profile_update_complete";
export const AUTH_PROFILE_UPDATE_FAIL = "auth_profile_update_fail";
export const AUTH_SOCIAL_LOGIN_SYNC_START = "auth_social_login_sync_start";
export const AUTH_SOCIAL_DISCONNECT = "auth_social_disconnect";
export const AUTH_SOCIAL_LOGIN_SYNC_COMPLETE =
  "auth_social_login_sync_complete";
export const AUTH_SOCIAL_LOGIN_SYNC_FAIL = "auth_social_login_sync_fail";
export const AUTH_CREATE_PROFILE_START = "auth_create_profile_start";
export const AUTH_CREATE_PROFILE_COMPLETE = "auth_create_profile_complete";
export const AUTH_CREATE_PROFILE_END = "auth_create_profile_end";

export const GAME_UPDATED = "game_updated";
export const GAME_COMING_SOON = "game_coming_soon";
export const GAME_POPULAR = "game_popular";
export const GAME_PROMOTED = "game_promoted";
export const GAME_SEARCH = "game_search";
export const GAME_SEARCH_START = "game_search_start";
export const GAME_SEARCH_CLEAR = "game_search_clear";
export const GAME_STUB = "game_stub";
export const GAME_REVIEW_HELPFUL = "game_review_helpful";
export const GAME_REVIEW = "game_review";
export const GAME_DETAIL = "game_detail";
export const GAME_RELATED = "game_related";
export const GAME_MORE_RELEASES_LOADING = "game_more_releases_loading";
export const GAME_MORE_RELEASES_FINISHED = "game_more_releases_finished";
export const GAME_HEARTED = "game_hearted";
export const GAME_OWNED = "game_owned";
export const GAME_RESET_PROFILE = "game_reset_profile";

export const DEALS_PLAYSTATION_4 = "deals_playstation_4";
export const DEALS_PLAYSTATION_5 = "deals_playstation_5";
export const DEALS_PC_GAMES = "deals_pc_games";
export const DEALS_PC_PARTS = "deals_pc_parts";
export const DEALS_XBOX_ONE = "deals_xbox_one";
export const DEALS_XBOX_SERIES_X = "deals_xbox_series_x";
export const DEALS_NINTENDO_SWITCH = "deals_ninendo_switch";
export const DEALS_FETCH_PRICES = "deals_fetch_prices";
export const DEALS_STORE_GAMES = "deals_store_games";
export const DEALS_COMPLETE_PRICES = "deals_complete_prices";
