import { Platform } from "react-native";
import { applyMiddleware, compose, createStore, Action } from "redux";
import { persistReducer, persistStore } from "redux-persist";
import ReduxThunk, { ThunkAction } from "redux-thunk";
import AsyncStorage from "@react-native-community/async-storage";
import ExpoFileSystemStorage from "redux-persist-expo-filesystem";

import reducers from "./reducers";

import { Game, PromotedGames, Deal, GameDeals } from "utilities/types";

const middleware = [ReduxThunk];
if (__DEV__) {
  const { logger } = require("redux-logger");
  middleware.push(logger);
}

const persistConfig = {
  key: "root",
  storage: Platform.OS === "ios" ? AsyncStorage : ExpoFileSystemStorage,
};

const persistedReducer = persistReducer(persistConfig, reducers);

export const store = createStore(
  persistedReducer,
  {},
  compose(applyMiddleware(...middleware))
);

export const persistor = persistStore(store);

export type RootState = {
  auth: {
    apiKey: string;
    avatar: string;
    username: string;
    loading: boolean;
    socialLoading: boolean;
    socialConnect: boolean;
    profileLoading: boolean;
  };
  games: {
    games: Game[];
    hot: PromotedGames[];
    popular: number[];
    search: number[];
    comingSoon: number[];
    updated: number[];
    wishlist: number[];
    owned: number[];
    searchLoading: boolean;
    releasesLoading: boolean;
  };
  deals: {
    promoted: Deal[];
    xboxOne: Deal[];
    xboxSeriesX: Deal[];
    playstation4: Deal[];
    playstation5: Deal[];
    nintendoSwitch: Deal[];
    pc: Deal[];
    consoles: Deal[];
    pcComponents: Deal[];
    dealsLoading: boolean;
    dealsForGames: GameDeals[];
  };
};

export type AppThunk<ReturnType = void> = ThunkAction<
  ReturnType,
  RootState,
  unknown,
  Action<string>
>;
