import * as Permissions from "expo-permissions";

export const cameraRollPermission = async () => {
  let finalStatus = null;
  await new Promise((resolve) => {
    setTimeout(resolve, 500);
  });

  const { status } = await Permissions.getAsync(Permissions.CAMERA_ROLL);
  finalStatus = status;

  if (status !== "granted") {
    const { status: askStatus } = await Permissions.askAsync(
      Permissions.CAMERA_ROLL
    );
    finalStatus = askStatus;
  }

  return finalStatus;
};

export const pushNotificationPermission = async () => {
  let finalStatus = null;

  // Timeout to wait b/w registering
  await new Promise((resolve) => {
    setTimeout(resolve, 500);
  });

  // First getAsync to check if already enabled
  const { status } = await Permissions.getAsync(Permissions.NOTIFICATIONS);
  finalStatus = status;

  // Show prompt to enable
  if (status !== "granted") {
    const { status: askStatus } = await Permissions.askAsync(
      Permissions.NOTIFICATIONS
    );
    finalStatus = askStatus;
  }

  return finalStatus;
};
