import { useState, useEffect } from "react";
import { Linking, Platform, Alert } from "react-native";
import Constants from "expo-constants";
import * as FileSystem from "expo-file-system";
import * as IntentLauncher from "expo-intent-launcher";
import * as ImagePicker from "expo-image-picker";
import * as ImageManipulator from "expo-image-manipulator";

import { Game } from "./types";
import { cameraRollPermission } from "./permissions";

export function uniqueGames(initialGames: Game[], mergeGames: Game[]) {
  mergeGames.forEach((g: Game) => {
    let gameIndex = initialGames.findIndex((obj) => obj.id === g.id);

    if (gameIndex === -1) {
      initialGames.push(g);
    }
  });

  return initialGames;
}
//Array sort function by date
export function byDateAsc(a: Game, b: Game) {
  const gameA = a.initial_release;
  const gameB = b.initial_release;

  let comparison = 0;
  if (gameA === null) {
    return -1;
  } else if (gameB === null) {
    return 1;
  }

  if (gameA > gameB) {
    comparison = 1;
  } else if (gameA < gameB) {
    comparison = -1;
  }
  return comparison;
}

//Array sort function by date
export function byNumberAsc(a: number, b: number) {
  let comparison = 0;
  if (a === null) {
    return -1;
  } else if (b === null) {
    return 1;
  }

  if (a > b) {
    comparison = 1;
  } else if (a < b) {
    comparison = -1;
  }
  return comparison;
}

// Array function sort games by similarity
export function bySimilarity(a: Game, b: Game) {
  const gameA = a.similar;
  const gameB = b.similar;

  let comparison = 0;

  if (gameA === null) {
    return -1;
  } else if (gameB === null) {
    return 1;
  }

  if (gameA > gameB) {
    comparison = -1;
  } else if (gameA < gameB) {
    comparison = 1;
  }
  return comparison;
}

export function useDebounce(value: string, delay: number) {
  // State and setters for debounced value
  const [debouncedValue, setDebouncedValue] = useState(value);

  useEffect(
    () => {
      // Update debounced value after delay
      const handler = setTimeout(() => {
        setDebouncedValue(value);
      }, delay);

      // Cancel the timeout if value changes (also on delay change or unmount)
      // This is how we prevent debounced value from updating if value is changed ...
      // .. within the delay period. Timeout gets cleared and restarted.
      return () => {
        clearTimeout(handler);
      };
    },
    [value, delay] // Only re-call effect if value or delay changes
  );

  return debouncedValue;
}

export const openSettings = () => {
  if (Platform.OS === "ios") {
    Linking.canOpenURL("app-settings:")
      .then((supported) => {
        if (supported) {
          Linking.openURL("app-settings:");
        }
      })
      .catch(() => {});
  } else {
    const pkg = Constants.manifest.releaseChannel
      ? Constants.manifest.android.package // When published, considered as using standalone build
      : "host.exp.exponent";
    IntentLauncher.startActivityAsync(
      IntentLauncher.ACTION_APPLICATION_DETAILS_SETTINGS,
      {
        data: "package:" + pkg,
      }
    );
  }
};

const askOpenSettings = (title: string, message: string) => {
  Alert.alert(title, message, [
    {
      text: "Open Settings",
      onPress: () => {
        openSettings();
      },
    },
    { text: "Cancel" },
  ]);
};

export const getCameraRollImage = async (base64 = false, size = 512) => {
  if ((await cameraRollPermission()) === "granted") {
    let result = await ImagePicker.launchImageLibraryAsync({
      quality: 1,
    });

    if (!result.cancelled) {
      //compress image and then update
      const manipResult = await ImageManipulator.manipulateAsync(
        result.uri,
        [
          result.height > result.width
            ? { resize: { height: size } }
            : { resize: { width: size } },
        ],
        { base64: base64 }
      );

      let fileInfo = await FileSystem.getInfoAsync(manipResult.uri, {
        size: true,
      });

      //if image over 6 meg then shrink it as best we can
      if (fileInfo !== undefined && fileInfo.size! > 6000000) {
        const pcScale = 6000000 / fileInfo.size!;
        const newImg = await ImageManipulator.manipulateAsync(
          result.uri,
          [
            result.height > result.width
              ? { resize: { height: size * pcScale } }
              : { resize: { width: size * pcScale } },
          ],
          { base64: base64 }
        );
        return newImg;
      }
      return manipResult;
    }
  } else {
    askOpenSettings(
      "Photo access required",
      "Photo access is needed to complete this action"
    );
    return null;
  }
};
