/* eslint-disable camelcase */
export type Game = {
  id: number;
  banner_clip: string;
  banner_img: string;
  cover: string;
  deals: Deal[];
  developer: Company | null;
  franchise: string | null;
  genres: string[];
  initial_release: Date | null;
  name: string;
  news: Article[];
  other_companies: Company[];
  platforms: string[];
  popularity: number;
  releases: Release[];
  reviews: Review[];
  score: number;
  screenshots: string[];
  similar: number | null;
  similar_games: SimilarGame[] | null;
  summary: string | null;
  updated_at: Date | null;
  user_review: Review | null;
  videos: string[];
};

export type Deal = {
  id: number;
  title: string;
  source: string;
  description: string;
  url: string;
  image_url: string;
  platform: string;
  similarity: number;
  price: string;
  sale_price: string;
  date_posted: Date;
  stock_available: boolean;
};

export type Company = {
  id: number;
  name: string;
};

export type Article = {
  id: number;
  authors: string;
  publish_date: Date;
  title: string;
  deck: string;
  body: string;
  image: ArticleImages;
  site_detail_url: string;
};

export type ArticleImages = {
  square_tiny: string;
  screen_tiny: string;
  square_small: string;
  original: string;
};

export type Release = {
  platform: string;
  region: string;
  date: Date;
};

export type Review = {
  id: number;
  igdb_index: number;
  user: string;
  user_avatar: string;
  source: string;
  score: number;
  title: string;
  body: string;
  helpful_count: number;
  unhelpful_count: number;
  user_found_helpful: boolean;
};

export type SimilarGame = {
  id: number;
  name: string;
  cover: string;
  initial_release: Date | null;
  user_review?: Review | null;
};

export type PromotedGames = {
  id: number;
  igdb_index: number;
  title: string;
  about: string;
  live_at: Date;
  details: Game;
};

export type GameDeals = {
  igdb_index: number;
  deals: Deal[];
};

export type GamePlaceholder = {
  id: number;
  name: string;
  cover: string;
  initial_release: Date | null;
};

export const placeholders: GamePlaceholder[] = [
  {
    id: -1,
    name: "Loading",
    cover: "https://nugame.uk/images/default_cover.png",
    initial_release: null,
  },
  {
    id: -2,
    name: "Loading",
    cover: "https://nugame.uk/images/default_cover.png",
    initial_release: null,
  },
  {
    id: -3,
    name: "Loading",
    cover: "https://nugame.uk/images/default_cover.png",
    initial_release: null,
  },
  {
    id: -4,
    name: "Loading",
    cover: "https://nugame.uk/images/default_cover.png",
    initial_release: null,
  },
  {
    id: -5,
    name: "Loading",
    cover: "https://nugame.uk/images/default_cover.png",
    initial_release: null,
  },
  {
    id: -6,
    name: "Loading",
    cover: "https://nugame.uk/images/default_cover.png",
    initial_release: null,
  },
];
