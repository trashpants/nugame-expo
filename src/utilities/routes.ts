import Constants from "expo-constants";

import Config from "../../config.json";

const ROUTEBASE = `${
  Constants.manifest.releaseChannel === "live"
    ? Config.SERVER
    : Config.SERVER_TEST
}/api/`;

export const postCreateUser = `${ROUTEBASE}auth`;

export const getPopular = `${ROUTEBASE}games/popular`;
export const getUpdated = `${ROUTEBASE}games/updated`;
export const getPromotedGames = `${ROUTEBASE}promoted`;
export const getComingSoon = `${ROUTEBASE}games/releases`;
export const postMoreReleases = `${ROUTEBASE}games/releases/more`;

export const postSearchGames = `${ROUTEBASE}search`;

export const getGameDetail = (igdbIndex: number) =>
  `${ROUTEBASE}game/${igdbIndex}`;
export const getRelevantGames = (igdbIndex: number) =>
  `${ROUTEBASE}game/relevant/${igdbIndex}`;

export const postWasReviewHelpful = `${ROUTEBASE}review/helpful`;
export const postReviewGame = `${ROUTEBASE}review`;

export const getDeals = (platform: string) =>
  `${ROUTEBASE}deals/games/${platform}`;
export const getDealsForGame = (igdbIndex: number) =>
  `${ROUTEBASE}prices/${igdbIndex}`;

export const postUpdateUserProfile = `${ROUTEBASE}user/profile/update`;
export const getUserProfile = `${ROUTEBASE}user/profile`;
export const postSocialConnect = `${ROUTEBASE}user/social`;
export const postSocialDisconnect = `${ROUTEBASE}user/social/disconnect`;
export const postListedGames = `${ROUTEBASE}games/lists/update`;
