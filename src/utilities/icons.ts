import { createIconSet } from "@expo/vector-icons";

export type NucleoIconNames =
  | "check"
  | "check-fill"
  | "heart"
  | "heart-fill"
  | "write"
  | "write-fill"
  | "share-ios"
  | "share-android"
  | "thumb-up"
  | "thumb-up-fill"
  | "thumb-down"
  | "thumb-down-fill"
  | "gamepad"
  | "gamepad-fill"
  | "calendar"
  | "calendar-fill"
  | "bag"
  | "bag-fill"
  | "search"
  | "search-fill";

const glyphMap = {
  check: 59906,
  "check-fill": 59907,
  heart: 59909,
  "heart-fill": 59908,
  write: 59911,
  "write-fill": 59910,
  "share-ios": 59912,
  "share-android": 59913,
  "thumb-up": 59915,
  "thumb-up-fill": 59914,
  "thumb-down": 59917,
  "thumb-down-fill": 59916,
  gamepad: 59919,
  "gamepad-fill": 59918,
  calendar: 59921,
  "calendar-fill": 59920,
  bag: 59922,
  "bag-fill": 59923,
  search: 59925,
  "search-fill": 59924,
};
const expoAssetId = require("@assets/fonts/nucleo.ttf");
export const Nucleo = createIconSet(glyphMap, "nucleo", expoAssetId);

export function getIconFromPlatform(icon: string, darkIcons: boolean = false) {
  switch (icon.toLowerCase()) {
    case "new nintendo 3ds":
    case "nintendo 3ds":
    case "nintendo eshop":
    case "wii u":
    case "wii":
    case "wiiu":
      return darkIcons
        ? require("@assets/platforms/nintendo-dark.png")
        : require("@assets/platforms/nintendo-light.png");
    case "nintendo switch":
      return darkIcons
        ? require("@assets/platforms/switch-dark.png")
        : require("@assets/platforms/switch-light.png");
    case "stadia":
    case "google stadia":
      return darkIcons
        ? require("@assets/platforms/stadia-dark.png")
        : require("@assets/platforms/stadia-light.png");
    case "steamvr":
    case "pc":
    case "pc (microsoft windows)":
    case "windows mixed Reality":
      return darkIcons
        ? require("@assets/platforms/pc-dark.png")
        : require("@assets/platforms/pc-light.png");
    case "mac":
    case "apple":
    case "ios":
      return darkIcons
        ? require("@assets/platforms/mac-dark.png")
        : require("@assets/platforms/mac-light.png");
    case "xbox 360":
    case "xbox one":
      return darkIcons
        ? require("@assets/platforms/xbox-dark.png")
        : require("@assets/platforms/xbox-light.png");
    case "xbox series x":
      return darkIcons
        ? require("@assets/platforms/xsx-dark.png")
        : require("@assets/platforms/xsx-light.png");
    case "playstation 3":
    case "playstation 2":
    case "playstation network":
      return darkIcons
        ? require("@assets/platforms/ps-dark.png")
        : require("@assets/platforms/ps-light.png");
    case "playstation vr":
    case "playstation 4":
      return darkIcons
        ? require("@assets/platforms/ps4-dark.png")
        : require("@assets/platforms/ps4-light.png");
    case "playstation 5":
      return darkIcons
        ? require("@assets/platforms/ps5-dark.png")
        : require("@assets/platforms/ps5-light.png");
    case "linux":
      return darkIcons
        ? require("@assets/platforms/linux-dark.png")
        : require("@assets/platforms/linux-light.png");
    case "android":
      return darkIcons
        ? require("@assets/platforms/android-dark.png")
        : require("@assets/platforms/android-light.png");
    default:
      break;
  }
}

export function getAffiliateLogo(source: string) {
  switch (source.toLowerCase()) {
    case "cdkeys":
      return require("@assets/affiliates/cdkeys.png");
    case "mmoga ltd. us":
    case "mmoga ltd. uk":
      return require("@assets/affiliates/mmoga.png");
    case "green man gaming":
    case "green man gaming us":
      return require("@assets/affiliates/greenmangaming.png");
    default:
      break;
  }
}

export function getDistributerLogo(source: string) {
  switch (source.toLowerCase()) {
    case "steam":
    case "steam vr":
    case "steamvr":
      return require("@assets/affiliates/distribution/steam.png");
    case "origin":
      return require("@assets/affiliates/distribution/origin.png");
    case "bethesda":
    case "bethesda.net":
      return require("@assets/affiliates/distribution/bethesda.png");
    case "epic":
    case "epic games":
      return require("@assets/affiliates/distribution/epic.png");
    case "eshop":
    case "e shop":
    case "nintendo eshop":
    case "nintendo e shop":
      return require("@assets/affiliates/distribution/eshop.png");
    case "gog":
    case "good old games":
    case "goodoldgames":
      return require("@assets/affiliates/distribution/gog.png");
    case "psn":
    case "playstation":
    case "playstation network":
      return require("@assets/affiliates/distribution/psn.png");
    case "rockstar":
    case "rockstar launcher":
    case "rockstar games":
    case "rockstar games launcher":
      return require("@assets/affiliates/distribution/rockstar.png");
    case "uplay":
    case "u play":
    case "u-play":
      return require("@assets/affiliates/distribution/uplay.png");
    case "xbox":
    case "xbox live":
    case "microsoft store":
      return require("@assets/affiliates/distribution/xbl.png");
    default:
      return require("@assets/affiliates/distribution/default.png");
  }
}
