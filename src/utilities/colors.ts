const colors = {
  // Primary
  blue050: "#E6F6FF",
  blue100: "#BAE3FF",
  blue200: "#7CC4FA",
  blue300: "#47A3F3",
  blue400: "#2186EB",
  blue500: "#0967D2",
  blue600: "#0552B5",
  blue700: "#03449E",
  blue800: "#01337D",
  blue900: "#002159",

  // Neutrals
  grey050: "#F5F7FA",
  grey100: "#E4E7EB",
  grey200: "#CBD2D9",
  grey300: "#9AA5B1",
  grey400: "#7B8794",
  grey500: "#616E7C",
  grey600: "#52606D",
  grey700: "#3E4C59",
  grey800: "#323F4B",
  grey900: "#1F2933",

  cyan050: "#E1FCF8",
  cyan100: "#C1FEF6",
  cyan200: "#92FDF2",
  cyan300: "#62F4EB",
  cyan400: "#3AE7E1",
  cyan500: "#1CD4D4",
  cyan600: "#0FB5BA",
  cyan700: "#099AA4",
  cyan800: "#07818F",
  cyan900: "#05606E",

  orange050: "#FFE8D9",
  orange100: "#FFD0B5",
  orange200: "#FFB088",
  orange300: "#FF9466",
  orange400: "#F9703E",
  orange500: "#F35627",
  orange600: "#DE3A11",
  orange700: "#C52707",
  orange800: "#AD1D07",
  orange900: "#841003",

  red050: "#FFE3E3",
  red100: "#FFBDBD",
  red200: "#FF9B9B",
  red300: "#F86A6A",
  red400: "#EF4E4E",
  red500: "#E12D39",
  red600: "#CF1124",
  red700: "#AB091E",
  red800: "#8A041A",
  red900: "#610316",

  yellow050: "#FFFBEA",
  yellow100: "#FFF3C4",
  yellow200: "#FCE588",
  yellow300: "#FADB5F",
  yellow400: "#F7C948",
  yellow500: "#F0B429",
  yellow600: "#DE911D",
  yellow900: "#8D2B0B",
  yellow700: "#CB6E17",
  yellow800: "#B44D12",

  green050: "#EFFCF6",
  green100: "#C6F7E2",
  green200: "#8EEDC7",
  green300: "#65D6AD",
  green400: "#3EBD93",
  green500: "#27AB83",
  green600: "#199473",
  green700: "#147D64",
  green800: "#0C6B58",
  green900: "#014D40",
};

export const LightTheme = {
  dark: false,
  colors: {
    primary: colors.blue600,
    background: colors.grey050,
    card: colors.grey100,
    text: colors.grey900,
    border: colors.grey200,
  },
};

export const DarkTheme = {
  dark: true,
  colors: {
    primary: colors.blue600,
    background: colors.grey900,
    card: colors.grey800,
    text: colors.grey050,
    border: colors.grey800,
  },
};

export default colors;
