import React from "react";
import {
  StyleSheet,
  View,
  Dimensions,
  TouchableOpacity,
  Platform,
  Share,
} from "react-native";
import { useSelector, useDispatch } from "react-redux";
import { useNavigation, useTheme } from "@react-navigation/native";
import * as Haptics from "expo-haptics";

import colors from "utilities/colors";
import { Game } from "utilities/types";
import { RootState } from "state/store";
import { Nucleo } from "utilities/icons";
import { addGameToList } from "state/actions";

interface ActionBarProps {
  id: number;
}

export default function ActionBar({ id }: ActionBarProps) {
  const theme = useTheme();
  const iconSize = 24;
  const buttonColor = theme.colors.text;

  const dispatch = useDispatch();
  const heartAction = (gameId: number) =>
    dispatch(addGameToList(gameId, "hearted"));
  const checkAction = (gameId: number) =>
    dispatch(addGameToList(gameId, "checked"));
  const [game] = useSelector((state: RootState) =>
    state.games.games.filter((g: Game) => {
      return g.id === id;
    })
  );

  const isHearted = useSelector(
    (state: RootState) => state.games.wishlist.indexOf(id) !== -1
  );

  const isChecked = useSelector(
    (state: RootState) => state.games.owned.indexOf(game.id) !== -1
  );

  const navigation = useNavigation();
  const styles = StyleSheet.create({
    container: {
      width: Dimensions.get("screen").width,
      paddingTop: 24,
      paddingHorizontal: 12,
      alignContent: "center",
    },
    buttonWrapper: {
      flex: 1,
      flexDirection: "row",
      justifyContent: "space-between",
      padding: 12,
      borderWidth: 1,
      borderRadius: 12,
      borderColor: theme.colors.border,
      backgroundColor: theme.colors.card,
    },
  });

  return (
    <View style={styles.container}>
      <View style={styles.buttonWrapper}>
        <TouchableOpacity
          onPress={() => {
            checkAction(id);
            Haptics.notificationAsync(Haptics.NotificationFeedbackType.Success);
          }}
        >
          <Nucleo
            name={isChecked ? "check-fill" : "check"}
            size={iconSize}
            color={isChecked ? colors.green400 : buttonColor}
          />
        </TouchableOpacity>

        <TouchableOpacity
          onPress={() => {
            heartAction(id);
            Haptics.notificationAsync(Haptics.NotificationFeedbackType.Success);
          }}
        >
          <Nucleo
            name={isHearted ? "heart-fill" : "heart"}
            size={iconSize}
            color={isHearted ? colors.red400 : buttonColor}
          />
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => {
            navigation.navigate("UserReview", { id });
            Haptics.notificationAsync(Haptics.NotificationFeedbackType.Success);
          }}
        >
          <Nucleo
            name={game.user_review === null ? "write" : "write-fill"}
            size={iconSize}
            color={game.user_review === null ? buttonColor : colors.blue400}
          />
        </TouchableOpacity>
        <TouchableOpacity
          onPress={async () => {
            Haptics.notificationAsync(Haptics.NotificationFeedbackType.Success);
            Share.share({
              url: "https://www.nugame.uk/share/" + game.id,
            });
          }}
        >
          <Nucleo
            name={Platform.OS === "ios" ? "share-ios" : "share-android"}
            size={iconSize}
            color={buttonColor}
          />
        </TouchableOpacity>
      </View>
    </View>
  );
}

export { ActionBar };
