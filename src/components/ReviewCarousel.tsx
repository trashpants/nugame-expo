import React from "react";
import { StyleSheet, FlatList, Dimensions, View } from "react-native";

import ReviewCard from "./ReviewCard";

import { Review } from "utilities/types";

interface ReviewCarouselProps {
  reviews: Review[];
}

export default function ReviewCarousel({ reviews }: ReviewCarouselProps) {
  const styles = StyleSheet.create({
    container: {
      width: Dimensions.get("screen").width,
    },
  });

  return (
    <View style={styles.container}>
      <FlatList
        horizontal
        showsHorizontalScrollIndicator={false}
        data={reviews.slice(0, 3)}
        keyExtractor={(review) => review.id.toString()}
        ListHeaderComponent={() => {
          return <View style={{ width: 12 }} />;
        }}
        ListFooterComponent={() => {
          return <View style={{ width: 24 }} />;
        }}
        renderItem={({ item }: { item: Review }) => (
          <ReviewCard review={item} largeMode={false} fullWidth={false} />
        )}
        snapToInterval={Dimensions.get("screen").width - 36}
      />
    </View>
  );
}

export { ReviewCarousel };
