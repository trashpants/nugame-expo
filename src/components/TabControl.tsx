/* eslint-disable react-native/no-unused-styles */
import React, { FunctionComponent, useState, useEffect } from "react";
import {
  View,
  Text,
  Animated,
  StyleSheet,
  Platform,
  TouchableWithoutFeedback,
  TouchableNativeFeedback,
  ViewStyle,
  TextStyle,
} from "react-native";
import * as Haptics from "expo-haptics";
import {
  PanGestureHandler,
  PanGestureHandlerGestureEvent,
} from "react-native-gesture-handler";
import { useTheme } from "@react-navigation/native";

import colors from "utilities/colors";

interface TabControlProps {
  values: string[];
  onChange: (value: string) => void;
}

interface SegmentProps {
  values: string[];
  selectedIndex: number;
  onIndexChange: (value: number) => void;
}

interface ContainerProps {
  numberValues: number;
  style: TabStyleProps;
  activeTabIndex: number;
  onIndexChange: (value: number) => void;
}

interface RenderLeftSeparatorProp {
  index: number;
  selectedIndex: number;
}

interface TabSpecificProps {
  style: ViewStyle[];
  onPress: () => void;
  renderLeftSeparator: Boolean;
  isActive: Boolean;
}

interface TabProps {
  label: string;
  onPress: () => void;
  isActive: Boolean;
  isFirst: Boolean;
  isLast: Boolean;
  renderLeftSeparator: Boolean;
}

interface TabStyleProps {
  tabStyle: ViewStyle;
  activeTabStyle: ViewStyle;
  tabsContainerStyle: ViewStyle;
  tabTextStyle: TextStyle;
  activeTabTextStyle: TextStyle;
  firstTabStyle: ViewStyle;
  lastTabStyle: ViewStyle;
}

const iosTabControlStyles = StyleSheet.create({
  tabsContainerStyle: {
    backgroundColor: colors.grey100,
    borderColor: colors.grey500,
    borderRadius: 6,
    paddingVertical: 2,
  },
  tabStyle: {
    flex: 1,
    marginVertical: 1,
    margin: 3,
    borderRadius: 6,
  },
  tabTextStyle: {
    color: colors.grey500,
    fontSize: 16,
    paddingVertical: 3,
    alignSelf: "center",
    textTransform: "capitalize",
  },
  activeTabStyle: {
    backgroundColor: colors.grey050,
  },
  activeTabTextStyle: {
    color: colors.grey900,
  },
  firstTabStyle: { marginLeft: 0 },
  lastTabStyle: { marginRight: 0 },
});
const androidTabControlStyles = StyleSheet.create({
  tabsContainerStyle: {
    backgroundColor: colors.grey100,
    height: 48,
    marginVertical: 12,
    borderRadius: 6,
  },
  tabStyle: {
    flex: 1,
    paddingVertical: 12,
    paddingHorizontal: 6,
  },
  tabTextStyle: { alignSelf: "center", textTransform: "capitalize" },
  activeTabStyle: {
    borderBottomWidth: 3,
    borderBottomColor: colors.blue400,
  },
  activeTabTextStyle: {},
  firstTabStyle: {},
  lastTabStyle: {},
});

const wrapperStyles = StyleSheet.create({
  outerGapStyle:
    Platform.OS === "ios"
      ? { padding: 3, paddingVertical: 12, marginHorizontal: 8 }
      : { padding: 0, marginHorizontal: 8 },
});

const tabControlStyles: TabStyleProps =
  Platform.OS === "ios" ? iosTabControlStyles : androidTabControlStyles;

const TabControl = ({ values, onChange }: TabControlProps) => {
  const [selectedIndex, setSelectedIndex] = useState(0);

  const handleIndexChange = (index: number) => {
    setSelectedIndex(index);
    onChange(values[index]);
  };

  return (
    <View style={wrapperStyles.outerGapStyle}>
      <SegmentedControl
        values={values}
        selectedIndex={selectedIndex}
        onIndexChange={handleIndexChange}
      />
    </View>
  );
};

function SegmentedControl({
  values,
  selectedIndex,
  onIndexChange,
}: SegmentProps) {
  return (
    <Container
      style={tabControlStyles}
      numberValues={values.length}
      activeTabIndex={selectedIndex}
      onIndexChange={onIndexChange}
    >
      {values.map((tabValue, index) => (
        <Tab
          label={tabValue}
          onPress={() => {
            onIndexChange(index);
            Haptics.notificationAsync(Haptics.NotificationFeedbackType.Success);
          }}
          isActive={selectedIndex === index}
          isFirst={index === 0}
          isLast={index === values.length - 1}
          renderLeftSeparator={shouldRenderLeftSeparator({
            index,
            selectedIndex,
          })}
          key={tabValue}
        />
      ))}
    </Container>
  );
}

const Container: FunctionComponent<ContainerProps> = ({
  children,
  numberValues,
  style,
  activeTabIndex,
  onIndexChange,
}) => {
  const theme = useTheme();
  const { tabStyle, activeTabStyle, tabsContainerStyle } = style;

  const margin = 6;

  const [moveAnimation] = useState(new Animated.Value(0));
  const [containerWidth, setContainerWidth] = useState(0);

  useEffect(() => {
    const leftVal = (containerWidth / numberValues) * activeTabIndex;
    Animated.timing(moveAnimation, {
      toValue: leftVal,
      duration: 250,
      useNativeDriver: false,
    }).start();
  }, [containerWidth, activeTabIndex, numberValues, moveAnimation]);

  const onGestureEvent = (evt: PanGestureHandlerGestureEvent) => {
    const tabWidth = containerWidth / numberValues;
    let index = Math.floor(evt.nativeEvent.x / tabWidth);
    if (index > numberValues - 1) index = numberValues - 1;
    else if (index < 0) index = 0;
    if (index !== activeTabIndex) {
      onIndexChange(index);
    }
  };

  return Platform.OS === "ios" ? (
    <PanGestureHandler onGestureEvent={onGestureEvent}>
      <View
        style={[
          {
            marginHorizontal: margin,
            flexDirection: "row",
            position: "relative",
          },
          tabsContainerStyle,
          { backgroundColor: theme.colors.card },
        ]}
        onLayout={(event) => {
          setContainerWidth(event.nativeEvent.layout.width);
        }}
      >
        <Animated.View
          style={{
            // works too
            width: containerWidth / numberValues - 6,
            left: moveAnimation,
            top: 2,
            bottom: 2,
            position: "absolute",
            ...tabStyle,
            ...activeTabStyle,
            backgroundColor: theme.dark ? colors.grey300 : colors.grey050,
          }}
        />
        {children}
      </View>
    </PanGestureHandler>
  ) : (
    <View
      style={[
        { marginHorizontal: margin, flexDirection: "row" },
        tabsContainerStyle,
        { backgroundColor: theme.colors.card },
      ]}
    >
      {children}
    </View>
  );
};

function shouldRenderLeftSeparator({
  index,
  selectedIndex,
}: RenderLeftSeparatorProp) {
  const isFirst = index === 0;
  const isSelected = index === selectedIndex;
  const isPrevSelected = index - 1 === selectedIndex;
  if (isFirst || isSelected || isPrevSelected) {
    return false;
  }
  return true;
}

const IosTab: FunctionComponent<TabSpecificProps> = ({
  children,
  style: tabControlStyle,
  onPress,
  renderLeftSeparator,
}) => (
  <View style={{ flex: 1, flexDirection: "row", alignItems: "center" }}>
    {renderLeftSeparator && (
      <View
        style={{
          height: "50%",
          width: 1,
          backgroundColor: colors.grey200,
        }}
      />
    )}
    <TouchableWithoutFeedback onPress={onPress}>
      <View style={tabControlStyle}>{children}</View>
    </TouchableWithoutFeedback>
  </View>
);

const AndroidTab: FunctionComponent<TabSpecificProps> = ({
  children,
  style: tabControlStyle,
  onPress,
}) => (
  <TouchableNativeFeedback
    onPress={onPress}
    background={TouchableNativeFeedback.Ripple(colors.grey300, true)}
  >
    <View style={tabControlStyle}>{children}</View>
  </TouchableNativeFeedback>
);

const OsSpecificTab: FunctionComponent<TabSpecificProps> = (props) => {
  return Platform.OS === "ios" ? (
    <IosTab {...props} />
  ) : (
    <AndroidTab {...props} />
  );
};

function Tab({
  label,
  onPress,
  isActive,
  isFirst,
  isLast,
  renderLeftSeparator,
}: TabProps) {
  const theme = useTheme();
  const {
    tabStyle,
    tabTextStyle,
    activeTabStyle,
    activeTabTextStyle,
    firstTabStyle,
    lastTabStyle,
  } = tabControlStyles;
  return (
    <OsSpecificTab
      isActive={isActive}
      onPress={onPress}
      style={[
        tabStyle,
        Platform.OS !== "ios" && isActive ? activeTabStyle : {},
        Platform.OS === "ios" && isActive
          ? {
              backgroundColor: theme.dark ? colors.grey300 : colors.grey050,
            }
          : {},
        isFirst && firstTabStyle,
        isLast && lastTabStyle,
      ]}
      renderLeftSeparator={renderLeftSeparator}
    >
      <Text
        style={[
          tabTextStyle,
          { color: theme.colors.text },
          isActive && activeTabTextStyle,
        ]}
      >
        {label}
      </Text>
    </OsSpecificTab>
  );
}

export default TabControl;
export { TabControl };
