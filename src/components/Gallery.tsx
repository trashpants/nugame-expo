import React from "react";
import { StyleSheet, View, Dimensions, Image } from "react-native";
import { ScrollView } from "react-native-gesture-handler";

interface GalleryProps {
  images: string[];
}

export default function Gallery({ images }: GalleryProps) {
  const styles = StyleSheet.create({
    container: {
      width: Dimensions.get("screen").width,
    },
    imageSmall: {
      height: Dimensions.get("screen").width * 0.4,
      width: Dimensions.get("screen").width * 0.7,
      marginLeft: 16,
      marginVertical: 16,
      borderRadius: 6,
    },
  });

  return (
    <View style={styles.container}>
      <ScrollView horizontal showsHorizontalScrollIndicator={false}>
        <>
          {images.map((img) => {
            return (
              <View key={img}>
                <Image style={styles.imageSmall} source={{ uri: img }} />
              </View>
            );
          })}
          <View style={{ marginLeft: 16 }} />
        </>
      </ScrollView>
    </View>
  );
}

export { Gallery };
