import React from "react";
import { StyleSheet, Text, TouchableOpacity } from "react-native";

import colors from "utilities/colors";

interface TextButtonProps {
  onPress: () => void;
  label: string;
}

export default function TextButton({ onPress, label }: TextButtonProps) {
  const styles = StyleSheet.create({
    container: {
      alignContent: "center",
    },
    button: {
      fontSize: 16,
      fontWeight: "bold",
      color: colors.blue400,
    },
  });

  return (
    <TouchableOpacity style={styles.container} onPress={onPress}>
      <Text style={styles.button} numberOfLines={1}>
        {label}
      </Text>
    </TouchableOpacity>
  );
}

export { TextButton };
