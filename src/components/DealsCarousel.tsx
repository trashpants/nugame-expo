import React from "react";
import {
  StyleSheet,
  FlatList,
  Dimensions,
  View,
  Text,
  Image,
  TouchableWithoutFeedback,
} from "react-native";
import { useNavigation } from "@react-navigation/native";

import { Deal } from "utilities/types";
import { getAffiliateLogo } from "utilities/icons";
import colors from "utilities/colors";

interface GamesCarouselProps {
  deals: Deal[];
}

export default function DealsCarousel({ deals }: GamesCarouselProps) {
  const navigation = useNavigation();
  const styles = StyleSheet.create({
    container: {
      width: Dimensions.get("screen").width,
    },
    itemContainer: {
      width: 130,
      height: 180,
      marginHorizontal: 6,
      alignItems: "center",
      padding: 20,
      shadowColor: "#000",
      shadowOffset: {
        width: 0,
        height: 5,
      },
      shadowOpacity: 0.34,
      shadowRadius: 6.27,

      elevation: 10,
    },
    detailsContainer: {
      marginLeft: 6,
      width: 130,
      alignItems: "center",
      justifyContent: "center",
    },
    image: {
      height: 160,
      width: 120,
      borderRadius: 6,
    },
    sourceContainer: {
      height: 32,
      width: 32,
      flex: 1,
      alignItems: "center",
      justifyContent: "center",
      backgroundColor: colors.grey900,
      borderRadius: 16,
      position: "absolute",
      right: -6,
      top: 6,
    },
    sourceImage: {
      height: 36,
      width: 30,
    },
    title: {
      textAlign: "center",
    },
    price: {
      textAlign: "center",
      fontWeight: "bold",
      fontSize: 16,
      paddingTop: 3,
    },
  });

  return (
    <View style={styles.container}>
      <FlatList
        horizontal
        showsHorizontalScrollIndicator={false}
        data={deals}
        keyExtractor={(deal, index) => deal.url + index}
        initialNumToRender={6}
        ListHeaderComponent={() => {
          return <View style={{ width: 12 }} />;
        }}
        ListFooterComponent={() => {
          return <View style={{ width: 12 }} />;
        }}
        renderItem={({ item }: { item: Deal }) => (
          <TouchableWithoutFeedback
            onPress={() => navigation.navigate("DealDetail", { deal: item })}
          >
            <View>
              <View style={styles.itemContainer}>
                <Image
                  source={{ uri: item.image_url }}
                  style={styles.image}
                  resizeMode="cover"
                />
                <View style={styles.sourceContainer}>
                  <Image
                    source={getAffiliateLogo(item.source)}
                    style={styles.sourceImage}
                    resizeMode="contain"
                  />
                </View>
              </View>
              <View style={styles.detailsContainer}>
                <Text style={styles.title} numberOfLines={2}>
                  {item.title}
                </Text>
                <Text style={styles.price}>{item.sale_price}</Text>
              </View>
            </View>
          </TouchableWithoutFeedback>
        )}
      />
    </View>
  );
}

export { DealsCarousel };
