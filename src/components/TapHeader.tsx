import React from "react";
import { StyleSheet, Text, TouchableOpacity } from "react-native";
import { useNavigation, useTheme } from "@react-navigation/native";
import { SimpleLineIcons } from "@expo/vector-icons";
import * as Haptics from "expo-haptics";

interface TextButtonProps {
  onPress?: () => void;
  label: string;
}

export default function TapHeader({ onPress, label }: TextButtonProps) {
  const theme = useTheme();
  const navigation = useNavigation();
  const styles = StyleSheet.create({
    container: {
      flexDirection: "row",
      marginLeft: 4,
      marginRight: 24,
      paddingTop: 16,
    },
    button: {
      fontSize: 36,
      fontWeight: "bold",
      color: theme.colors.text,
    },
    arrow: {
      marginTop: 10,
      paddingRight: 4,
    },
  });

  return (
    <TouchableOpacity
      style={styles.container}
      onPress={() => {
        Haptics.notificationAsync(Haptics.NotificationFeedbackType.Success);
        if (onPress === undefined) {
          if (navigation.canGoBack()) {
            navigation.goBack();
          }
        } else {
          onPress();
        }
      }}
    >
      <SimpleLineIcons
        name="arrow-left"
        size={24}
        style={styles.arrow}
        color={theme.colors.text}
      />
      <Text style={styles.button}>{label}</Text>
    </TouchableOpacity>
  );
}

export { TapHeader };
