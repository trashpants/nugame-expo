import React, { useState, useEffect } from "react";
import { Animated, StyleSheet, Image, View, Dimensions } from "react-native";
import { Video } from "expo-av";

interface VideoGalleryProps {
  image: string | null;
  video: string | null;
  onVideoStarted: Function;
  onVideoEnded: Function;
}

export default function BannerBackground({ image, video }: VideoGalleryProps) {
  const bannerWidth = Dimensions.get("screen").width;
  const bannerHeight = Dimensions.get("screen").height / 2;

  const [playVideo, setPlayVideo] = useState(false);
  const [opacity] = useState(new Animated.Value(0));
  const [transitionOpacity] = useState(new Animated.Value(0));

  useEffect(() => {
    if (playVideo) {
      Animated.timing(transitionOpacity, {
        toValue: 1,
        duration: 1000,
        useNativeDriver: true,
      }).start(() => {
        Animated.timing(opacity, {
          toValue: 1,
          duration: 1000,
          useNativeDriver: true,
        }).start();
      });
    } else {
      Animated.timing(opacity, {
        toValue: 0,
        duration: 2000,
        useNativeDriver: true,
      }).start(() => {
        Animated.timing(transitionOpacity, {
          toValue: 0,
          duration: 1000,
          useNativeDriver: true,
        }).start();
      });
    }

    return () => {};
  }, [playVideo, opacity, transitionOpacity, setPlayVideo]);

  const styles = StyleSheet.create({
    container: {
      flex: 1,
    },
    bannerContainer: {
      height: bannerHeight,
      width: bannerWidth,
    },
    overlayContainer: {
      position: "absolute",
      top: 0,
    },
  });

  return (
    <View style={styles.container}>
      {image !== null && (
        <>
          <Image
            style={styles.bannerContainer}
            source={{ uri: image!, cache: "force-cache" }}
          />
          <Animated.View
            style={[
              styles.bannerContainer,
              styles.overlayContainer,
              { backgroundColor: "#000", opacity: transitionOpacity },
            ]}
          />
        </>
      )}
      {video !== null && (
        <Animated.View
          style={[styles.bannerContainer, styles.overlayContainer, { opacity }]}
        >
          <Video
            onLoad={() => {
              setTimeout(() => {
                setPlayVideo(true);
              }, 1000);
            }}
            source={{ uri: video! }}
            rate={1.0}
            volume={0}
            isMuted
            resizeMode="cover"
            shouldPlay
            isLooping={false}
            style={[styles.bannerContainer, { backgroundColor: "transparent" }]}
            onPlaybackStatusUpdate={(playbackStatus) => {
              if (playbackStatus.isLoaded) {
                if (
                  playbackStatus.isPlaying &&
                  playbackStatus.positionMillis + 3000 >=
                    playbackStatus.durationMillis!
                ) {
                  setPlayVideo(false);
                }
              }
            }}
          />
        </Animated.View>
      )}
    </View>
  );
}

export { BannerBackground };
