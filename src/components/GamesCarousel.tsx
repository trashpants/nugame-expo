import React from "react";
import { StyleSheet, FlatList, Dimensions, View, Text } from "react-native";
import { useNavigation, useTheme } from "@react-navigation/native";

import GameCard from "./GameCard";

import { Game, GamePlaceholder, placeholders } from "utilities/types";

interface GamesCarouselProps {
  games: Game[];
}

export default function GamesCarousel({ games }: GamesCarouselProps) {
  const theme = useTheme();
  const navigation = useNavigation();

  const styles = StyleSheet.create({
    container: {
      width: Dimensions.get("screen").width,
    },
    releaseDate: {
      paddingTop: 8,
      textAlign: "center",
      fontWeight: "bold",
      color: theme.colors.text,
    },
  });
  return (
    <View style={styles.container}>
      <FlatList
        horizontal
        showsHorizontalScrollIndicator={false}
        data={games.length > 0 ? games : placeholders}
        keyExtractor={(game) => game.id.toString()}
        initialNumToRender={6}
        ListHeaderComponent={() => {
          return <View style={{ width: 10 }} />;
        }}
        ListFooterComponent={() => {
          return <View style={{ width: 10 }} />;
        }}
        renderItem={({ item }: { item: Game | GamePlaceholder }) => (
          <View style={{ minWidth: 150, marginHorizontal: 6 }}>
            <GameCard
              cardWidth={150}
              game={item}
              onPress={(game) => {
                if (game.id > 0) {
                  navigation.navigate("Detail", {
                    screen: "GameDetail",
                    params: { id: game.id },
                  });
                }
              }}
            />
            <Text style={styles.releaseDate}>
              {item.id < 0
                ? "Loading"
                : item.initial_release === null
                ? "TBD"
                : new Date(item.initial_release).toLocaleDateString(undefined, {
                    year: "numeric",
                    month: "short",
                    day: "numeric",
                  })}
            </Text>
          </View>
        )}
      />
    </View>
  );
}

export { GamesCarousel };
