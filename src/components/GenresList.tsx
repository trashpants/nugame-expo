import React from "react";
import { StyleSheet, Text, View } from "react-native";

import colors from "utilities/colors";
interface GenreListProps {
  genres: string[];
}

export default function GenreList({ genres }: GenreListProps) {
  const styles = StyleSheet.create({
    container: {
      flexDirection: "row",
      flexWrap: "wrap",
      borderRadius: 6,
      padding: 16,
    },
    genreContainer: {
      backgroundColor: colors.grey100,
      paddingHorizontal: 12,
      paddingVertical: 6,
      borderRadius: 6,
      marginBottom: 12,
      marginRight: 12,
    },
    genreText: {
      fontSize: 16,
      fontWeight: "bold",
    },
  });

  return (
    <View style={styles.container}>
      {genres.map((item, i) => {
        return (
          <View key={i} style={styles.genreContainer}>
            <Text style={styles.genreText}>{item}</Text>
          </View>
        );
      })}
      <View style={{ padding: 12 }} />
    </View>
  );
}

export { GenreList };
