import React, { useState } from "react";
import {
  StyleSheet,
  View,
  Dimensions,
  Image,
  Text,
  TouchableWithoutFeedback,
} from "react-native";
import Animated, {
  Value,
  interpolate,
  Extrapolate,
  useCode,
  block,
  set,
  Clock,
  Easing,
  timing,
  cond,
  not,
  clockRunning,
  startClock,
  eq,
  stopClock,
  call,
  and,
} from "react-native-reanimated";
import { LinearGradient } from "expo-linear-gradient";
import { useMemoOne } from "use-memo-one";
import { useNavigation, useTheme } from "@react-navigation/native";

import colors from "utilities/colors";
import { PromotedGames } from "utilities/types";

interface HeaderCardProps {
  games: PromotedGames[];
}

interface LoaderSegmentProps {
  onComplete: () => void;
  index: number;
  active: boolean;
}

export default function HeaderCard({ games }: HeaderCardProps) {
  const theme = useTheme();
  const [activeIndex, setActiveIndex] = useState(0);
  const navigation = useNavigation();

  const styles = StyleSheet.create({
    container: {
      height: Dimensions.get("screen").height / 1.75,
      padding: 0,
      margin: 8,
      marginHorizontal: 16,
      shadowColor: "#000",
      shadowOffset: {
        width: 0,
        height: 0,
      },
      shadowOpacity: 0.3,
      shadowRadius: 4,
      borderRadius: 12,
      elevation: 5,
    },
    backgroundImage: {
      flex: 1,
      borderRadius: 12,
    },
    gradient: {
      position: "absolute",
      left: -2,
      right: -2,
      top: 0,
      bottom: -2,
      borderRadius: 11,
      borderWidth: 2,
      borderColor: theme.colors.border,
    },
    mainContent: {
      justifyContent: "flex-end",
      position: "absolute",
      top: 0,
      bottom: 0,
      left: 0,
      right: 0,
      paddingBottom: 48,
      padding: 12,
    },
    title: {
      fontSize: 36,
      fontWeight: "bold",
      color: theme.colors.text,
    },
    subTitle: {
      fontSize: 18,
      color: theme.colors.text,
    },
    segmentPanel: {
      flexDirection: "row",
      alignItems: "center",
      position: "absolute",
      left: 6,
      right: 6,
      bottom: 6,
      height: 24,
    },
  });

  return (
    <TouchableWithoutFeedback
      onPress={() => {
        navigation.navigate("PromotedGames");
      }}
    >
      <View style={styles.container}>
        {games[activeIndex] !== null && games[activeIndex] !== undefined && (
          <Image
            source={{
              uri:
                games[activeIndex].details.banner_img === null
                  ? games[activeIndex].details.cover
                  : games[activeIndex].details.banner_img,
              cache: "force-cache",
            }}
            style={styles.backgroundImage}
          />
        )}
        <LinearGradient
          colors={["transparent", theme.colors.background]}
          style={styles.gradient}
        />
        <View style={styles.mainContent}>
          <Text style={styles.title}>
            {games[activeIndex] !== null && games[activeIndex] !== undefined
              ? games[activeIndex].details.name
              : ""}
          </Text>
          <Text style={styles.subTitle}>
            {games[activeIndex] !== undefined ? games[activeIndex].title : ""}
          </Text>
        </View>
        <View style={styles.segmentPanel}>
          {games.map((item, i) => {
            return (
              <LoaderSegment
                key={i}
                index={i}
                onComplete={() => {
                  setActiveIndex((i + 1) % games.length);
                }}
                active={activeIndex === i}
              />
            );
          })}
        </View>
      </View>
    </TouchableWithoutFeedback>
  );
}

const runTiming = (
  clock: Animated.Clock,
  callback: () => void
): Animated.Node<number> => {
  const state: Animated.TimingState = {
    finished: new Value(0),
    position: new Value(0),
    frameTime: new Value(0),
    time: new Value(0),
  };
  const config = {
    toValue: new Value(1),
    duration: 10000,
    easing: Easing.linear,
  };
  return block([
    cond(
      not(clockRunning(clock)),
      set(state.time, 0),
      timing(clock, state, config)
    ),
    cond(eq(state.finished, 1), [
      set(state.finished, 0),
      set(state.position, 0),
      set(state.frameTime, 0),
      set(state.time, 0),
      stopClock(clock),
      call([state.position], () => {
        callback();
      }),
    ]),
    state.position,
  ]);
};

function LoaderSegment({ onComplete, index, active }: LoaderSegmentProps) {
  const theme = useTheme();
  const { clock, isActive, progress } = useMemoOne(
    () => ({
      clock: new Clock(),
      isActive: new Value(0) as Animated.Value<0 | 1>,
      progress: new Value(0),
    }),
    []
  );

  isActive.setValue(new Value(active ? 1 : 0));

  useCode(
    () =>
      block([
        cond(and(isActive, not(clockRunning(clock))), startClock(clock)),
        cond(and(not(isActive), clockRunning(clock)), stopClock(clock)),
        set(progress, runTiming(clock, onComplete)),
      ]),
    [clock, isActive, progress]
  );

  const delta = 1;

  const start = 0;
  const end = start + delta;
  const width = interpolate(progress, {
    inputRange: [start, end],
    outputRange: [Dimensions.get("screen").width / 6 - 20, 0],
    extrapolate: Extrapolate.CLAMP,
  });

  const left = interpolate(progress, {
    inputRange: [start, end],
    outputRange: [
      index * (Dimensions.get("screen").width / 6 - 8) + 2,
      (index + 1) * (Dimensions.get("screen").width / 6 - 8) - 10,
    ],
    extrapolate: Extrapolate.CLAMP,
  });

  const styles = StyleSheet.create({
    segment: {
      width: Dimensions.get("screen").width / 6 - 16,
      height: 8,
      backgroundColor: theme.colors.background,
      marginHorizontal: 4,
      borderRadius: 4,
      borderWidth: 2,
      borderColor: theme.colors.border,
    },
    segmentProgress: {
      width: Dimensions.get("screen").width / 6 - 18,
      height: 4,
      backgroundColor: theme.colors.background,
      marginHorizontal: 4,
      borderRadius: 4,
      borderWidth: 0,
      borderColor: theme.colors.text,
      position: "absolute",
    },
  });

  return (
    <>
      <LinearGradient
        colors={[colors.blue400, colors.cyan400]}
        start={[0, 0.5]}
        end={[1, 0.5]}
        style={styles.segment}
      />
      <Animated.View
        style={[
          styles.segmentProgress,
          {
            left,
            width,
          },
        ]}
      />
    </>
  );
}

export { HeaderCard };
