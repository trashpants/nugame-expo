import React from "react";
import { StyleSheet, TouchableWithoutFeedback } from "react-native";
import { useTheme } from "@react-navigation/native";

import colors from "utilities/colors";
import { Nucleo, NucleoIconNames } from "utilities/icons";

interface ToggleButtonProps {
  icon: NucleoIconNames;
  selectedIcon: NucleoIconNames;
  onColor: string;
  isSelected: boolean | null;
  onToggleOn: (isSet: boolean) => void;
}

export default function ToggleButton({
  icon,
  selectedIcon,
  isSelected,
  onColor,
  onToggleOn,
}: ToggleButtonProps) {
  const theme = useTheme();
  const actualButton = isSelected ? selectedIcon : icon;

  const styles = StyleSheet.create({
    container: {},
  });
  return (
    <TouchableWithoutFeedback
      style={styles.container}
      onPress={() => {
        if (isSelected) {
          return;
        }
        onToggleOn(true);
      }}
    >
      <Nucleo
        name={actualButton}
        size={28}
        color={
          isSelected ? onColor : theme.dark ? colors.grey400 : colors.grey600
        }
      />
    </TouchableWithoutFeedback>
  );
}

export { ToggleButton };
