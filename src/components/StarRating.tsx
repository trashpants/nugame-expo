import React from "react";
import { StyleSheet, View, TouchableWithoutFeedback } from "react-native";
import { FontAwesome } from "@expo/vector-icons";

import colors from "utilities/colors";

interface StarRatingProps {
  score: number;
  editable?: boolean;
  onScore?: (score: number) => void;
}

export default function StarRating({
  score,
  editable,
  onScore,
}: StarRatingProps) {
  const stars = [1, 2, 3, 4, 5];

  const styles = StyleSheet.create({
    container: {
      flexDirection: "row",
      borderRadius: 12,
    },
    starOutline: {
      paddingRight: 1,
    },
    star: {
      paddingRight: 1,
      position: "absolute",
      top: 3,
      left: 3,
    },
  });
  return (
    <View style={styles.container}>
      {stars.map((item, i) => {
        return (
          <TouchableWithoutFeedback
            key={i}
            onPress={() => {
              onScore!(i + 1);
            }}
          >
            <View>
              <FontAwesome
                color={
                  score >= i + 1 && score > 0
                    ? colors.yellow500
                    : colors.grey300
                }
                name="star-o"
                size={editable ? 36 : 24}
                style={styles.starOutline}
              />

              <FontAwesome
                color={
                  score >= i + 1 && score > 0 ? colors.yellow400 : "transparent"
                }
                name={score >= i + 1 && score > 0 ? "star" : "star-o"}
                size={editable ? 30 : 18}
                style={styles.star}
              />
            </View>
          </TouchableWithoutFeedback>
        );
      })}
    </View>
  );
}

export { StarRating };
