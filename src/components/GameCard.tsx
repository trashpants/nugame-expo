import React, { useState } from "react";
import {
  StyleSheet,
  View,
  Image,
  TouchableOpacity,
  ActivityIndicator,
} from "react-native";
import { useSelector } from "react-redux";
import { useTheme } from "@react-navigation/native";

import { Game, SimilarGame, GamePlaceholder } from "utilities/types";
import { RootState } from "state/store";
import { Nucleo } from "utilities/icons";
import colors from "utilities/colors";

interface GameCardProps {
  cardWidth: number;
  onPress: (game: { id: number; name: string; cover: string }) => void;
  game: Game | SimilarGame | GamePlaceholder;
}

export default function GameCard({ cardWidth, game, onPress }: GameCardProps) {
  const theme = useTheme();
  const [loading, setLoading] = useState(true);
  const isHearted = useSelector(
    (state: RootState) => state.games.wishlist.indexOf(game.id) !== -1
  );

  const isChecked = useSelector(
    (state: RootState) => state.games.owned.indexOf(game.id) !== -1
  );

  const cardHeight = cardWidth * 1.33;
  const borderRadius = cardWidth / 15;

  const styles = StyleSheet.create({
    card: {
      height: cardHeight,
      borderRadius: borderRadius,
      shadowColor: "#000",
      shadowOffset: {
        width: 0,
        height: 0,
      },
      shadowOpacity: 0.3,
      shadowRadius: 2,
      elevation: 5,
    },
    boxArt: {
      height: cardHeight,
      position: "absolute",
      top: 0,
      left: 0,
      right: 0,
      bottom: 0,
      borderRadius,
    },
    iconContainer: {
      position: "absolute",
      backgroundColor: theme.colors.background,
      padding: 3,
      borderRadius: 12,
      borderWidth: 2,
      borderColor: theme.colors.border,
      shadowColor: "#000",
      shadowOffset: {
        width: 0,
        height: 0,
      },
      shadowOpacity: 0.3,
      shadowRadius: 2,
      elevation: 5,
      right: 6,
    },
    loadingOverlay: {
      position: "absolute",
      top: 0,
      left: 0,
      right: 0,
      bottom: 0,
      backgroundColor: colors.grey200,
      borderRadius,
      justifyContent: "center",
      alignItems: "center",
    },
  });

  const isReviewed =
    game.user_review !== null && game.user_review !== undefined;

  var icons = [];
  icons.push(isChecked ? "check" : null);
  icons.push(isHearted ? "heart" : null);
  icons.push(isReviewed ? "reviewed" : null);
  icons = icons.filter((i) => {
    return i !== null;
  });
  return (
    <TouchableOpacity
      onPress={() => {
        onPress({ id: game.id, name: game.name, cover: game.cover });
      }}
    >
      <View style={styles.card}>
        <Image
          style={styles.boxArt}
          source={{ uri: game.cover, cache: "force-cache" }}
          onLoad={() => {
            setLoading(true);
          }}
          onLoadEnd={() => {
            setLoading(false);
          }}
        />
        {icons.map((item, i) => {
          return (
            <View key={i}>
              {item !== null && (
                <View
                  key={i}
                  style={[styles.iconContainer, { top: i * 26 + 6 }]}
                >
                  {item === "check" && (
                    <Nucleo
                      name="check-fill"
                      size={12}
                      color={colors.green400}
                    />
                  )}
                  {item === "heart" && (
                    <Nucleo name="heart-fill" size={12} color={colors.red400} />
                  )}
                  {item === "reviewed" && (
                    <Nucleo
                      name="write-fill"
                      size={12}
                      color={colors.blue400}
                    />
                  )}
                </View>
              )}
            </View>
          );
        })}
        {loading && (
          <View style={styles.loadingOverlay}>
            <ActivityIndicator color={colors.grey900} animating={loading} />
          </View>
        )}
      </View>
    </TouchableOpacity>
  );
}

export { GameCard };
