import React from "react";
import { StyleSheet, Image, Text, Dimensions, View } from "react-native";
import { useDispatch, useSelector } from "react-redux";
import stripHtml from "string-strip-html";
import { useNavigation, useTheme } from "@react-navigation/native";
import * as Haptics from "expo-haptics";

import StarRating from "./StarRating";
import TextButton from "./TextButton";
import ToggleButton from "./ToggleButton";
import HTMLRender from "./HTMLRender";

import { markReviewHelpful } from "state/actions";
import { Review } from "utilities/types";
import colors from "utilities/colors";
import { RootState } from "state/store";

interface ReviewCardProps {
  review: Review;
  largeMode: boolean;
  fullWidth: boolean;
}

export default function ReviewCard({
  review,
  largeMode,
  fullWidth,
}: ReviewCardProps) {
  const theme = useTheme();
  const navigation = useNavigation();
  const dispatch = useDispatch();
  const reviewThought = (isHelpful: boolean) =>
    dispatch(markReviewHelpful(review.id, review.igdb_index, isHelpful));

  const [game] = useSelector((state: RootState) =>
    state.games.games.filter((i) => {
      return i.id === review.igdb_index;
    })
  );

  const [r] = game.reviews.filter((rv) => {
    return rv.id === review.id;
  });

  const styles = StyleSheet.create({
    container: {
      width:
        Dimensions.get("screen").width - (largeMode || fullWidth ? 32 : 48),
      borderColor: theme.colors.border,
      backgroundColor: theme.colors.card,
      padding: 16,
      marginVertical: 16,
      marginLeft: largeMode || fullWidth ? 16 : 12,
      borderWidth: 2,
      borderRadius: 12,
    },
    headerContainer: {
      flexDirection: "row",
    },
    avatarImg: {
      height: 64,
      width: 64,
      borderRadius: 32,
      borderWidth: 2,
      borderColor: theme.colors.border,
      backgroundColor: theme.colors.background,
    },
    headerDetailsVertical: {
      marginVertical: 6,
      marginHorizontal: 12,
    },
    user: {
      textTransform: "capitalize",
      fontWeight: "bold",
      fontSize: 18,
      color: theme.colors.text,
      marginRight: 48,
    },
    sourceImage: {
      position: "absolute",
      top: 42,
      left: 42,
      height: 24,
      width: 24,
      borderRadius: 12,
    },
    title: {
      marginTop: 16,
      fontWeight: "bold",
      fontSize: 20,
      color: theme.colors.text,
    },
    reviewText: {
      marginVertical: 8,
      fontSize: 16,
      color: theme.colors.text,
    },
    helpfulContainer: {
      flex: 1,
      flexDirection: "row",
      justifyContent: "space-between",
      alignItems: "center",
      marginTop: 12,
    },
    helpfulText: {
      fontSize: 16,
      color: theme.colors.text,
    },
    actionButtons: {
      flexDirection: "row",
      alignItems: "center",
    },
    reviewCount: {
      fontSize: 12,
      alignSelf: "center",
      flex: 1,
      paddingTop: 3,
      color: theme.colors.text,
    },
  });

  return (
    <View style={styles.container}>
      <View style={styles.headerContainer}>
        <Image source={{ uri: r.user_avatar }} style={styles.avatarImg} />
        <Image source={getSource(r.source)} style={styles.sourceImage} />
        <View style={styles.headerDetailsVertical}>
          <Text style={styles.user} numberOfLines={1} ellipsizeMode="tail">
            {r.user}
          </Text>
          <StarRating score={r.score} />
        </View>
      </View>

      <Text style={styles.title}>{r.title}</Text>

      {largeMode ? (
        <HTMLRender html={r.body} imagePadding={64} />
      ) : (
        <View>
          <Text style={styles.reviewText} numberOfLines={3}>
            {stripHtml(r.body)}
          </Text>
          <TextButton
            label="Read more"
            onPress={() => {
              navigation.navigate("DetailedReview", { review: review });
            }}
          />
        </View>
      )}

      <View style={styles.helpfulContainer}>
        <Text style={styles.helpfulText}>Was this review helpful?</Text>

        <View style={styles.actionButtons}>
          <View>
            <ToggleButton
              icon="thumb-up"
              selectedIcon="thumb-up-fill"
              onColor={colors.blue400}
              isSelected={r.user_found_helpful === true ? true : false}
              onToggleOn={() => {
                Haptics.notificationAsync(
                  Haptics.NotificationFeedbackType.Success
                );
                reviewThought(true);
              }}
            />
            <Text style={styles.reviewCount}>{r.helpful_count}</Text>
          </View>
          <View style={{ paddingHorizontal: 12 }} />
          <View>
            <ToggleButton
              icon="thumb-down"
              selectedIcon="thumb-down-fill"
              onColor={colors.red400}
              isSelected={r.user_found_helpful === false ? true : false}
              onToggleOn={() => {
                Haptics.notificationAsync(
                  Haptics.NotificationFeedbackType.Success
                );
                reviewThought(false);
              }}
            />
            <Text style={styles.reviewCount}>{r.unhelpful_count}</Text>
          </View>
        </View>
      </View>
    </View>
  );
}

function getSource(source: string) {
  switch (source) {
    case "giantbomb":
      return require("@assets/sources/giantbomb.jpg");
    case "gamespot":
      return require("@assets/sources/gamespot.png");
    default:
      break;
  }
}

export { ReviewCard };
