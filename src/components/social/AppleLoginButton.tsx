import * as React from "react";
import { Alert, StyleSheet, View } from "react-native";
import * as AppleAuthentication from "expo-apple-authentication";

type SocialButtonProps = {
  disabled: boolean;
  onComplete: (values: {
    username: string;
    email: string;
    token: string;
    source: string;
  }) => void;
};

const styles = StyleSheet.create({
  container: {
    height: 44,
    paddingHorizontal: 12,
    flexDirection: "row",
    borderRadius: 6,
    marginBottom: 10,
  },
});

export default function AppleLoginButton({
  onComplete,
  disabled,
}: SocialButtonProps) {
  return (
    <View style={styles.container}>
      <AppleAuthentication.AppleAuthenticationButton
        buttonType={AppleAuthentication.AppleAuthenticationButtonType.SIGN_IN}
        buttonStyle={AppleAuthentication.AppleAuthenticationButtonStyle.BLACK}
        cornerRadius={6}
        style={{ flex: 1 }}
        onPress={async () => {
          if (disabled) {
            return;
          }
          try {
            const credential = await AppleAuthentication.signInAsync({
              requestedScopes: [
                AppleAuthentication.AppleAuthenticationScope.FULL_NAME,
                AppleAuthentication.AppleAuthenticationScope.EMAIL,
              ],
            });
            // signed in
            onComplete({
              username: `${credential.fullName!
                .givenName!} ${credential.fullName!.familyName!}`,
              email: credential.email!,
              token: credential.identityToken!,
              source: "Apple",
            });
          } catch (e) {
            if (e.code === "ERR_CANCELED") {
              // handle that the user canceled the sign-in flow
            } else {
              Alert.alert("Login Error");
            }
          }
        }}
      />
    </View>
  );
}

export { AppleLoginButton };
