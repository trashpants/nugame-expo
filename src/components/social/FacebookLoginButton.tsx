import * as React from "react";
import {
  Alert,
  Image,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from "react-native";
import * as Facebook from "expo-facebook";

import Config from "../../../config.json";

type SocialButtonProps = {
  disabled: boolean;
  onComplete: (values: {
    username: string;
    email: string;
    token: string;
    source: string;
    avatar: string;
  }) => void;
};

const styles = StyleSheet.create({
  container: {
    height: 44,
    marginHorizontal: 12,
    flexDirection: "row",
    borderRadius: 6,
    marginBottom: 10,
    backgroundColor: "#1778F2",
  },
  text: {
    color: "#ffffff",
    fontSize: 18,
    fontWeight: "600",
  },
  logo: {
    height: 16,
    width: 16,
    marginRight: 5,
  },
});

export default function FacebookLoginButton({
  onComplete,
  disabled,
}: SocialButtonProps) {
  const facebookLogIn = async () => {
    // try {
    await Facebook.initializeAsync(Config.FACEBOOK_KEY);
    await Facebook.logInWithReadPermissionsAsync({
      permissions: ["public_profile"],
    })
      .then((response) => {
        if (response.type === "success") {
          fetch(
            `https://graph.facebook.com/me?access_token=${response.token}&fields=id,name,email,picture.height(500)`
          )
            .then((res) => res.json())
            .then((user) => {
              onComplete({
                username: user.name,
                email: user.email,
                token: response.token,
                source: "Facebook",
                avatar: user.picture.data.url,
              });
            })
            .catch((e) => Alert.alert("Facebook Login Error", e));
        }
      })
      .catch((e) => {
        Alert.alert("Facebook Login Error", e);
      });
  };

  return (
    <View style={styles.container}>
      <TouchableOpacity
        style={{
          flex: 1,
          flexDirection: "row",
          alignItems: "center",
          justifyContent: "center",
        }}
        onPress={() => {
          if (disabled) {
            return;
          }
          facebookLogIn();
        }}
      >
        <Image
          style={styles.logo}
          source={require("@assets/social/facebook.png")}
        />
        <Text style={styles.text}>Log in with Facebook</Text>
      </TouchableOpacity>
    </View>
  );
}

export { FacebookLoginButton };
