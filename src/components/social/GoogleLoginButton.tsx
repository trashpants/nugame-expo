import * as React from "react";
import { Image, StyleSheet, Text, TouchableOpacity, View } from "react-native";
import * as Google from "expo-google-app-auth";

import Config from "../../../config.json";

import colors from "utilities/colors";

type SocialButtonProps = {
  disabled: boolean;
  onComplete: (values: {
    username: string;
    email: string;
    token: string;
    source: string;
    avatar: string;
  }) => void;
};

const styles = StyleSheet.create({
  container: {
    height: 44,
    marginHorizontal: 12,
    flexDirection: "row",
    borderRadius: 6,
    marginBottom: 10,
    borderColor: "#dedede",
    borderWidth: 1,
    backgroundColor: colors.grey050,
  },
  text: {
    color: "#7b7b7b",
    fontSize: 18,
    fontWeight: "600",
  },
  logo: {
    height: 16,
    width: 16,
    marginRight: 5,
  },
});

export default function GoogleLoginButton({
  onComplete,
  disabled,
}: SocialButtonProps) {
  const googleLogIn = async () => {
    await Google.logInAsync({
      iosClientId: Config.GOOGLE_EXPO_KEY_IOS,
      androidClientId: Config.GOOGLE_EXPO_KEY_ANDROID,
      iosStandaloneAppClientId: Config.GOOGLE_KEY_IOS,
      androidStandaloneAppClientId: Config.GOOGLE_KEY_ANDROID,
      scopes: ["profile", "email"],
    })
      .then((response) => {
        if (response.type === "success") {
          onComplete({
            username: response.user.name!,
            email: response.user.email!,
            token: response.accessToken!,
            source: "Google",
            avatar: response.user.photoUrl!,
          });
        }
      })
      .catch(() => {});
  };

  return (
    <View style={styles.container}>
      <TouchableOpacity
        style={{
          flex: 1,
          flexDirection: "row",
          alignItems: "center",
          justifyContent: "center",
        }}
        onPress={() => {
          if (disabled) {
            return;
          }
          googleLogIn();
        }}
      >
        <Image
          style={styles.logo}
          source={require("@assets/social/google.png")}
        />
        <Text style={styles.text}>Sign in with Google</Text>
      </TouchableOpacity>
    </View>
  );
}

export { GoogleLoginButton };
