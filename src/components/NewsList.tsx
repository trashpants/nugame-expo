import React from "react";
import {
  StyleSheet,
  View,
  Dimensions,
  Image,
  Text,
  TouchableOpacity,
} from "react-native";
import { SimpleLineIcons } from "@expo/vector-icons";
import { useTheme } from "@react-navigation/native";

import colors from "utilities/colors";
import { Article } from "utilities/types";

interface NewsListProps {
  limited: boolean;
  articles: Article[];
  onPress: (article: Article) => void;
}

export default function NewsList({
  articles,
  limited,
  onPress,
}: NewsListProps) {
  const theme = useTheme();
  const articlesToDisplay = limited ? articles.slice(0, 3) : articles;

  const styles = StyleSheet.create({
    container: {
      maxWidth: Dimensions.get("screen").width,
      paddingVertical: 12,
    },
    articleContainer: {
      flex: 1,
      alignItems: "center",
      flexDirection: "row",
      marginVertical: 8,
      paddingHorizontal: 16,
    },
    imageThumbnail: {
      height: 80,
      width: 80,
      borderRadius: 6,
      borderWidth: 1,
      borderColor: theme.colors.border,
    },
    detailsContainer: {
      flex: 1,
      justifyContent: "space-between",
      paddingHorizontal: 12,
      paddingVertical: 6,
    },
    title: {
      fontSize: 16,
      fontWeight: "bold",
      color: theme.colors.text,
    },
    date: {
      fontSize: 14,
      color: colors.grey500,
    },
    lineSeparator: {
      borderBottomWidth: 1,
      borderColor: theme.colors.border,
      marginHorizontal: 32,
      marginVertical: 6,
    },
  });

  return (
    <View style={styles.container}>
      {articlesToDisplay.map((article, i) => {
        const date = new Date(article.publish_date).toLocaleDateString(
          undefined,
          {
            year: "numeric",
            month: "short",
            day: "numeric",
          }
        );
        return (
          <TouchableOpacity
            key={i}
            onPress={() => {
              onPress(article);
            }}
          >
            <View style={styles.articleContainer}>
              <Image
                source={{ uri: article.image.square_small }}
                style={styles.imageThumbnail}
              />
              <View style={styles.detailsContainer}>
                <Text style={styles.title} numberOfLines={2}>
                  {article.title}
                </Text>
                <Text style={styles.date}>{date}</Text>
              </View>
              <SimpleLineIcons
                name="arrow-right"
                size={20}
                color={theme.colors.border}
              />
            </View>
            {i + 1 < articlesToDisplay.length ? (
              <TouchableOpacity style={styles.lineSeparator} />
            ) : null}
          </TouchableOpacity>
        );
      })}
    </View>
  );
}

export { NewsList };
