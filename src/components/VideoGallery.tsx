import React, { useState, useEffect } from "react";
import {
  StyleSheet,
  View,
  Dimensions,
  Image,
  TouchableOpacity,
  FlatList,
} from "react-native";
import YoutubePlayer from "react-native-youtube-iframe";

import colors from "utilities/colors";

interface VideoGalleryProps {
  videos: string[];
}

export default function VideoGallery({ videos }: VideoGalleryProps) {
  const [currentVideoIndex, setCurrentVideoIndex] = useState(0);
  const [isLoaded, setIsLoaded] = useState(false);
  const videoWidth = Dimensions.get("screen").width - 32;
  const videoHeight = (videoWidth / 16) * 9;

  const styles = StyleSheet.create({
    container: {
      flex: 1,
      paddingTop: 12,
    },
    videoContainer: {
      marginHorizontal: 16,
      marginBottom: 16,
      borderRadius: 6,
      height: videoHeight,
      width: videoWidth,
      backgroundColor: colors.grey900,
    },
    videoCarousel: {},
    imagesHeaderPadding: {
      marginLeft: 8,
      shadowColor: "#000",
      shadowOpacity: 1,
      shadowRadius: 12,
      elevation: 12,
    },
    image: {
      height: 75,
      width: (75 / 9) * 16,
      borderRadius: 6,
      marginLeft: 8,
      borderWidth: 2,
      borderColor: colors.grey200,
    },
    selectedVideoBorder: {
      borderWidth: 2,
      borderColor: colors.blue400,
    },
  });

  useEffect(() => {
    setTimeout(() => {
      setIsLoaded(true);
    }, 100);
  }, []);

  return (
    <View style={styles.container}>
      <View style={styles.videoContainer}>
        {isLoaded && (
          <YoutubePlayer
            videoId={
              videos[currentVideoIndex]
                .split("https://www.youtube.com/watch?v=")
                .pop()!
            }
            height={videoHeight}
            width={videoWidth}
            initialPlayerParams={{ modestbranding: true }}
            webViewStyle={{ borderRadius: 6 }}
          />
        )}
      </View>

      <FlatList
        horizontal
        showsHorizontalScrollIndicator={false}
        data={videos}
        renderItem={({ item }) => (
          <TouchableOpacity
            style={styles.videoCarousel}
            onPress={() => {
              setCurrentVideoIndex(videos.indexOf(item));
            }}
          >
            <Image
              source={{
                uri:
                  "https://i.ytimg.com/vi/" +
                  item.split("https://www.youtube.com/watch?v=").pop()! +
                  "/hqdefault.jpg",
              }}
              style={[
                styles.image,
                videos.indexOf(item) === currentVideoIndex &&
                  styles.selectedVideoBorder,
              ]}
            />
          </TouchableOpacity>
        )}
        keyExtractor={(item) => item}
        ListHeaderComponent={<View style={styles.imagesHeaderPadding} />}
        ListFooterComponent={<View style={{ marginLeft: 16 }} />}
      />
    </View>
  );
}

export { VideoGallery };
