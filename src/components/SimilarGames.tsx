import React from "react";
import {
  StyleSheet,
  View,
  Dimensions,
  Image,
  Text,
  TouchableOpacity,
  ScrollView,
} from "react-native";
import { useTheme } from "@react-navigation/native";

import { SimilarGame } from "utilities/types";

interface SimilarGamesProps {
  limited: boolean;
  games: SimilarGame[];
  onPress: (game: SimilarGame) => void;
}

export default function SimilarGames({
  games,
  limited,
  onPress,
}: SimilarGamesProps) {
  const theme = useTheme();
  const gamesToDisplay = limited ? games.slice(0, 5) : games;

  const styles = StyleSheet.create({
    container: {
      flex: 1,
      flexDirection: "row",
      maxWidth: Dimensions.get("screen").width,
      paddingVertical: 12,
    },
    gameContainer: {
      alignItems: "center",
      marginVertical: 8,
      paddingHorizontal: 16,
      maxWidth: Dimensions.get("screen").width / 4,
      marginHorizontal: 4,
    },
    imageThumbnail: {
      height: (Dimensions.get("screen").width / 4) * 1.4,
      width: Dimensions.get("screen").width / 4,
      borderRadius: 6,
      borderWidth: 1,
      borderColor: theme.colors.border,
    },
    gameTitle: {
      width: Dimensions.get("screen").width / 4,
      fontSize: 16,
      fontWeight: "bold",
      textAlign: "center",
      color: theme.colors.text,
    },
    date: {
      color: theme.colors.text,
    },
  });

  return (
    <View style={styles.container}>
      <ScrollView horizontal showsHorizontalScrollIndicator={false}>
        <View style={{ width: 12 }} />
        {gamesToDisplay.map((game, i) => {
          const date =
            game.initial_release === null
              ? "TBD"
              : new Date(game.initial_release).getFullYear();
          return (
            <TouchableOpacity
              key={i}
              onPress={() => {
                onPress(game);
              }}
            >
              <View style={styles.gameContainer}>
                <Image
                  source={{ uri: game.cover }}
                  style={styles.imageThumbnail}
                />
                <Text style={styles.gameTitle} numberOfLines={2}>
                  {game.name}
                </Text>
                <Text style={styles.date}>{date}</Text>
              </View>
            </TouchableOpacity>
          );
        })}
        <View style={{ width: 12 }} />
      </ScrollView>
    </View>
  );
}

export { SimilarGames };
