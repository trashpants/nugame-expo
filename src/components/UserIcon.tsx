import React from "react";
import { StyleSheet, View, TouchableOpacity, Image } from "react-native";
import { useSelector } from "react-redux";
import { SimpleLineIcons } from "@expo/vector-icons";
import { useTheme } from "@react-navigation/native";

import colors from "utilities/colors";
import { RootState } from "state/store";

interface UserIconProps {
  size: number;
  onPress?: () => void;
  edit?: boolean;
  tempImage?: string;
}

export default function UserIcon({
  size,
  onPress,
  edit,
  tempImage,
}: UserIconProps) {
  const theme = useTheme();
  const userDetails = useSelector((state: RootState) => state.auth);
  const styles = StyleSheet.create({
    container: {
      flexDirection: "row",
      flexWrap: "wrap",
      borderRadius: 6,
    },
    avatarImage: {
      height: size,
      width: size,
      borderRadius: size / 2,
      borderWidth: 2,
      borderColor: theme.colors.border,
      backgroundColor: theme.colors.background,
    },
    editIcon: {
      position: "absolute",
      bottom: 0,
      right: 0,
      backgroundColor: colors.blue400,
      padding: 12,
      borderRadius: 32,
      borderWidth: 2,
      borderColor: theme.colors.border,
      shadowOffset: {
        width: 0,
        height: 0,
      },
      shadowColor: "#000",
      shadowOpacity: 0.3,
      shadowRadius: 2,
      elevation: 5,
    },
  });

  return (
    <>
      {onPress ? (
        <TouchableOpacity style={styles.container} onPress={onPress}>
          <View>
            <Image
              source={{
                uri:
                  tempImage !== null && tempImage !== undefined
                    ? tempImage
                    : userDetails.avatar,
              }}
              style={styles.avatarImage}
            />
          </View>
          {edit && (
            <View style={styles.editIcon}>
              <SimpleLineIcons name="pencil" size={24} color={colors.grey050} />
            </View>
          )}
        </TouchableOpacity>
      ) : (
        <View style={styles.container}>
          <Image
            source={{ uri: userDetails.avatar }}
            style={styles.avatarImage}
          />
        </View>
      )}
    </>
  );
}

export { UserIcon };
