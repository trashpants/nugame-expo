import React from "react";
import { StyleSheet, Image, TouchableOpacity, Text } from "react-native";
import { useNavigation, useTheme } from "@react-navigation/native";
import { SimpleLineIcons } from "@expo/vector-icons";

import { getIconFromPlatform } from "utilities/icons";

interface StarRatingProps {
  gameId: number;
  platforms: string[];
}

export default function PlatformIcons({ gameId, platforms }: StarRatingProps) {
  const theme = useTheme();
  const navigation = useNavigation();

  const styles = StyleSheet.create({
    container: {
      flexDirection: "row",
      alignItems: "center",
      borderRadius: 12,
    },
    icon: {
      height: 24,
      width: 24,
      resizeMode: "contain",
      margin: 6,
    },
  });

  return (
    <TouchableOpacity
      style={styles.container}
      onPress={() => {
        navigation.navigate("Releases", { id: gameId });
      }}
    >
      {platforms.slice(0, 4).map((item, i) => {
        return (
          <Image
            key={i}
            source={getIconFromPlatform(item, theme.dark)}
            style={styles.icon}
          />
        );
      })}
      {platforms.length > 4 && <Text>...</Text>}
      <SimpleLineIcons
        name="arrow-right"
        size={20}
        color={theme.colors.border}
      />
    </TouchableOpacity>
  );
}

export { PlatformIcons };
