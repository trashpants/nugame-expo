import React from "react";
import { StyleSheet, TouchableOpacity, View } from "react-native";
import { useNavigation } from "@react-navigation/native";
import { AntDesign, SimpleLineIcons } from "@expo/vector-icons";
import * as Haptics from "expo-haptics";

import colors from "utilities/colors";

interface CloseButtonProps {
  onPress?: () => void;
  icon?: "back" | "close";
}

export default function CloseButton({ onPress, icon }: CloseButtonProps) {
  const navigation = useNavigation();
  const styles = StyleSheet.create({
    container: {
      alignContent: "center",
      position: "absolute",
      borderRadius: 18,
      top: 16,
      left: 16,
      padding: 6,
      shadowColor: "#000",
      shadowOffset: {
        width: 0,
        height: 2,
      },
      shadowOpacity: 0.34,
      shadowRadius: 6.27,

      elevation: 10,
    },
    background: {
      position: "absolute",
      opacity: 0.75,
      backgroundColor: colors.grey050,
      height: 36,
      width: 36,
      borderRadius: 18,
      flex: 1,
      justifyContent: "center",
      alignContent: "center",
    },
  });

  const isBack = icon === undefined || icon === "close";

  return (
    <TouchableOpacity
      style={styles.container}
      onPress={() => {
        Haptics.notificationAsync(Haptics.NotificationFeedbackType.Success);
        if (onPress === undefined) {
          if (navigation.canGoBack()) {
            navigation.goBack();
          }
        } else {
          onPress();
        }
      }}
    >
      <View style={styles.background} />
      {isBack ? (
        <AntDesign name="close" size={24} />
      ) : (
        <SimpleLineIcons
          name="arrow-left"
          size={20}
          style={{ paddingTop: 1 }}
        />
      )}
    </TouchableOpacity>
  );
}

export { CloseButton };
