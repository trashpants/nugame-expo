import React from "react";
import { Dimensions, StyleSheet, View, Image } from "react-native";
import HTML from "react-native-render-html";
import { useTheme } from "@react-navigation/native";

interface RenderHtmlProps {
  html: string;
  imagePadding: number;
}

/*
const videoArticle = `
https://gamespot1.cbsistatic.com/uploads/original/1574/15746725/3667657-insidexbox_acvalhallawithinterview_site.jpg
https://static-gamespotvideo.cbsistatic.com/vr/2020/05/07/Breakout_ACVTrailerInterview_4000.mp4
<h2 dir="ltr"><strong>Assassin's Creed Valhalla</strong></h2>
<div data-embed-type="video" data-ref-id="2300-6452827" data-src="/videos/assassins-creed-valhalla-full-presentation-inside-/2300-6452827/" data-width="100%" data-height="100%" data-video-start="0">
    <iframe src="/videos/embed/6452827/" width="100%" height="100%" scrolling="no" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowfullscreen></iframe>
</div>
`;
*/

export default function HTMLRender({ html, imagePadding }: RenderHtmlProps) {
  const theme = useTheme();
  const styles = StyleSheet.create({
    inlineImage: {
      height: 200,
      width: Dimensions.get("window").width - imagePadding,
      borderRadius: 6,
    },
  });

  return (
    <HTML
      html={"<body>" + html + "</body>"}
      imagesMaxWidth={Dimensions.get("window").width - imagePadding}
      staticContentMaxWidth={Dimensions.get("window").width - imagePadding}
      alterNode={(node: HTML.HTMLNode) => {
        const { name, parent } = node;
        if (
          name === "iframe" &&
          parent.attribs.hasOwnProperty("data-embed-type") &&
          parent.attribs["data-embed-type"] === "video"
        ) {
          node.attribs = {
            ...node.attribs,
            src: !node.attribs.src.startsWith("https://")
              ? "https://gamespot.com" + node.attribs.src
              : node.attribs.src,
          };
        }
      }}
      baseFontStyle={{ fontSize: 16, color: theme.colors.text }}
      renderers={{
        figure: (htmlAttribs: HTML.HtmlAttributesDictionary) => {
          if (htmlAttribs["data-embed-type"] === "gallery") {
            return <View />;
          } else if (htmlAttribs["data-embed-type"] === "image") {
            return (
              <Image
                style={styles.inlineImage}
                source={{ uri: htmlAttribs["data-img-src"] }}
              />
            );
          }
        },
      }}
    />
  );
}
export { HTMLRender };
