import React, { FunctionComponent, useRef } from "react";
import { Animated, View, Dimensions } from "react-native";

interface ParallaxScrollProps {
  renderHeader: () => {};
  headerHeight: number;
  backgroundColor: string;
}

const ParallaxScrollView: FunctionComponent<ParallaxScrollProps> = ({
  renderHeader,
  headerHeight,
  backgroundColor,
  children,
}) => {
  const animatedScrollYValue = useRef(new Animated.Value(0)).current;

  const renderParallaxHeader = () => {
    const scaleValue = 2;
    const scale = animatedScrollYValue.interpolate({
      inputRange: [-headerHeight, 0],
      outputRange: [scaleValue * 1.5, 1],
      extrapolate: "clamp",
    });
    let animationStyle = {
      transform: [
        {
          scale: scale,
        },
      ],
    };
    return (
      <>
        <Animated.View
          style={[
            animationStyle,
            {
              position: "absolute",
              top: 0,
              height: headerHeight,
            },
          ]}
        >
          {renderHeader()}
        </Animated.View>
        <View
          style={{
            flex: 1,
            width: Dimensions.get("screen").width,
            height: Dimensions.get("screen").height - headerHeight,
            position: "absolute",
            top: headerHeight,
            backgroundColor,
          }}
        />
      </>
    );
  };

  return (
    <View style={{ flex: 1 }}>
      <Animated.ScrollView
        stickyHeaderIndices={[2]}
        onScroll={Animated.event(
          [{ nativeEvent: { contentOffset: { y: animatedScrollYValue } } }],
          {
            useNativeDriver: true,
          }
        )}
      >
        {renderParallaxHeader()}
        {children}
      </Animated.ScrollView>
    </View>
  );
};
export default ParallaxScrollView;
export { ParallaxScrollView };
