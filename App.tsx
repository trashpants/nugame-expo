import React from "react";
import { StatusBar, View } from "react-native";
import { PersistGate } from "redux-persist/integration/react";
import { Provider } from "react-redux";
import { useFonts } from "@use-expo/font";
import RootNavigator from "@navigators/RootNavigator";
import * as Sentry from "sentry-expo";
import Constants from "expo-constants";

import { persistor, store } from "./src/state/store";

Sentry.init({
  dsn:
    "https://8de4157bebee463287cae8035d388ea4@o310572.ingest.sentry.io/5357224",
  enableInExpoDevelopment: true,
  debug: true,
});
if (Constants.manifest.revisionId) {
  Sentry.setRelease(Constants.manifest.revisionId);
}

export default function App() {
  const [fontsLoaded] = useFonts({
    nucleo: require("@assets/fonts/nucleo.ttf"),
  });

  return (
    <Provider store={store}>
      <PersistGate persistor={persistor} loading={null}>
        <StatusBar
          barStyle="dark-content"
          backgroundColor="#00000033"
          translucent
        />
        {fontsLoaded ? <RootNavigator /> : <View />}
      </PersistGate>
    </Provider>
  );
}
