#!/bin/bash
echo "What kind of build would you like to make? [(e)xpo / (a)ndroid / (i)os] ?"
read build

rm -rf .expo

if [ $build == "expo" ] || [ $build == 'e' ] ;
then
    echo You selected expo
    expo publish
    exit 1
fi

if [ $build == "android" ] || [ $build == 'a' ] ;
then
    echo This will build a live channel android app
    expo build:android -t app-bundle --release-channel live
    exit 1
fi

if [ $build == "ios" ] || [ $build == 'i' ] ;
then
    echo This will build a live channel iOS app
    expo build:ios --release-channel live
    exit 1
fi

echo $build is not a valid build type... exiting now.
